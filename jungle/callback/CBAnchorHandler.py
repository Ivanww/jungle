import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import math

from .CallbackBase import CallbackBase

DEFAULT_YOLO_ANCHORS = [
    [(116,90),  (156,198),  (373,326)],
    [(30,61),  (62,45),  (59,119)],
    [(10,13),  (16,30),  (33,23)]
    ]
DEFAULT_YOLO_STRIDES = [13, 26, 52]
DEFAULT_YOLO_SCALES = [32, 16, 8]

class YOLOAnchorHandler(nn.Module):
    def __init__(self, anchors=DEFAULT_YOLO_ANCHORS, scales=DEFAULT_YOLO_SCALES, emb_sizes = []):
        super(YOLOAnchorHandler, self).__init__()
        self.scales = scales
        self.base_anchors = anchors
        self.anchors = {}
        for s in emb_sizes:
            self.base_anchors[s] = self._calc_anchors(s)

    def _calc_anchors(self, emb_size):
        strides = [torch.ceil(emb_size/s) for s in self.scales]
        return [a.view(-1, 2) / s for a, s in zip(self.base_anchors, strides)]

    def forward(self, emb_size):
        if emb_size not in self.anchors:
            self.anchors[emb_size] = self._calc_anchors(emb_size)
        return self.anchors[emb_size]

DEFAULT_STRIDES = (4, 8, 16, 32, 64)
DEFAULT_ANCHOR_SCALES = (32, 64, 128, 256, 512)
DEFAULT_ASPECT_RATIOS = (0.5, 1., 2.)
class RCNNAnchorHandler(nn.Module):
    def __init__(self, strides, anchor_scales, aspect_ratios):
        super(RCNNAnchorHandler, self).__init__()
        if not isinstance(anchor_scales[0], (list, tuple)):
            anchor_scales = tuple((s,) for s in anchor_scales)
        if not isinstance(aspect_ratios[0], (list, tuple)):
            aspect_ratios = (aspect_ratios,) * len(anchor_scales)

        # backbone scale = input feature size // feature embedding size
        self.strides = strides
        if not isinstance(self.strides, (list, tuple)):
            self.strides = [self.strides] # need check
        # ratios of base anchors
        self.anchor_scale = anchor_scales
        self.aspect_ratios = aspect_ratios

        base_anchors = self.build_base_anchors()
        for i, anchor in enumerate(base_anchors):
            self.register_buffer('base_anchor_{}'.format(i), anchor)

        self.num_level = len(base_anchors)

        # TODO: No anchor cache
        self.tiled_anchors = None
        self.cached_feature_size = None
        # self.anchors = None

    def _base_anchor(self):
        return [getattr(self, 'base_anchor_{}'.format(i)) for i in range(self.num_level)]

    def build_base_anchors(self):
        base_anchors = []
        for s, r in zip(self.anchor_scale, self.aspect_ratios):
            scales = torch.as_tensor(s).float()
            aspect_ratios = torch.as_tensor(r).float()
            h_ratios = torch.sqrt(aspect_ratios)
            w_ratios = 1 / h_ratios

            ws = (w_ratios[:, None] * scales[None, :]).view(-1)
            hs = (h_ratios[:, None] * scales[None, :]).view(-1)

            base_anchors.append((torch.stack([-ws, -hs, ws, hs], dim=1) / 2).round())

        return base_anchors

    def tile_base_anchor(self, strides, cellsizes):
        # tile base anchors for each scale
        self.tiled_anchors = []
        base_anchors = self._base_anchor()
        for stride, cellsize, base_anchor in zip(strides, cellsizes, base_anchors):
            shifts_x = torch.arange(0, cellsize[1], device=base_anchor.device).float() * int(stride[1])
            shifts_y = torch.arange(0, cellsize[0], device=base_anchor.device).float() * int(stride[0])
            shift_y, shift_x = torch.meshgrid(shifts_y, shifts_x)
            shift_x = shift_x.reshape(-1)
            shift_y = shift_y.reshape(-1)
            shifts = torch.stack((shift_x, shift_y, shift_x, shift_y), dim=1)
            self.tiled_anchors.append(
                (shifts.view(-1, 1, 4) + base_anchor.view(1, -1, 4)).reshape(-1, 4)
            )

    def forward(self, emb_size):
        # some checks
        if len(self.strides) != len(self.anchor_scale):
            raise RuntimeError('number of backbone scales and number of anchor scales not match')
        # image_size = map_size * stride
        if self.tiled_anchors is not None:
            if self.cached_feature_size[0] == emb_size[0] and self.cached_feature_size[1] == emb_size[1]:
                return self.tiled_anchors

        self.cached_feature_size = emb_size
        cell_sizes = [(math.ceil(emb_size[0] / s), math.ceil(emb_size[1] / s)) for s in self.strides]
        # calibrate strides
        strides = [(emb_size[0] / c[0], emb_size[1] / c[1]) for c in cell_sizes]
        self.tile_base_anchor(strides, cell_sizes)

        return self.tiled_anchors

FASTER_RCNN_ANCHOR_CONFIG = {
    'strides': DEFAULT_STRIDES,
    'anchor_scales': DEFAULT_ANCHOR_SCALES,
    'aspect_ratios': DEFAULT_ASPECT_RATIOS
}
YOLO_ANCHOR_CONFIG = {
    'anchors': DEFAULT_YOLO_ANCHORS,
    'scales': DEFAULT_ANCHOR_SCALES,
    'feas_dims': []
}

class CBAnchorHandler(CallbackBase):
    def __init__(self, tag='AnchorHandler', t_anchor='rcnn', config=None):
        super(CBAnchorHandler, self).__init__(tag)
        self.callback_t = CallbackBase.DATA_PROCESS

        self.t_anchor = t_anchor
        if t_anchor == 'rcnn':
            if config is None:
                config = FASTER_RCNN_ANCHOR_CONFIG
            self.anchor_handler = RCNNAnchorHandler(**config)
        elif t_anchor == 'yolo':
            if config is None:
                config = YOLO_ANCHOR_CONFIG
            self.anchor_handler = YOLOAnchorHandler(**config)
        else:
            raise TypeError('unknown anchor type ' + str(t_anchor))

    def on_initializing(self, eng):
        self._cuda(eng.memo['use_cuda'])
        if self.cuda:
            self.anchor_handler = self.anchor_handler.cuda()

    def on_batch_begin(self, eng, ctx):
        feature_size = ctx['features'].shape[-2:]
        # [tensor, ...] one for each scale
        tiled_anchors = self.anchor_handler(feature_size)

        if self.t_anchor == 'rcnn':
            eng.model.anchors = tiled_anchors
        ctx['targets']['anchors'] = tiled_anchors

        return None
