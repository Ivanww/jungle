import math
import torch
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torchvision

from .CallbackBase import CallbackBase
from ..model import FasterRCNN

class RCNNPreProcess(nn.Module):
    def __init__(self, min_size=800, max_size=1333, **kwargs):
        super(RCNNPreProcess, self).__init__()
        self.min_size = min_size
        self.max_size = max_size

    def _resize(self, img, box=None):
        # TODO: multiscale support
        img_size = torch.tensor(img.shape[-2:], device=img.device).long()
        scale = self.min_size / float(torch.min(img_size))
        if float(torch.max(img_size)) * scale > self.max_size:
            scale = self.max_size / float(torch.max(img_size))
        img = F.interpolate(img[None], scale_factor=scale, mode='bilinear', align_corners=False)[0]
        if box is not None:
            resized_box = torch.zeros_like(box, device=img.device).float()
            # W
            resized_box[:, 0::2] = box[:, 0::2] * scale
            # H
            resized_box[:, 1::2] = box[:, 1::2] * scale
        else:
            resized_box = None

        return img, resized_box

    def _make_batch(self, images, strides=32):
        img_sizes = torch.stack([torch.tensor(i.shape[-2:]) for i in images])
        img_channel = images[0].shape[0]
        max_size, _ = img_sizes.max(dim=0)
        batch_size = torch.ceil(max_size / float(strides)).long() * strides
        batch = torch.zeros(len(images), img_channel, batch_size[0], batch_size[1],
            dtype=torch.float32, device=images[0].device)
        for i in range(len(images)):
            batch[i, :, :img_sizes[i][0], :img_sizes[i][1]].copy_(images[i])

        return batch, img_sizes

    def forward(self, images, boxes=None):
        if boxes is None:
            boxes = [None for _ in images]
        assert len(images) == len(boxes)

        resized_images, resized_boxes = list(zip(*[self._resize(i, b) for i, b in zip(images, boxes)]))
        batch_images, img_sizes = self._make_batch(resized_images)

        return batch_images, resized_boxes, img_sizes

class RCNNPostProcess(nn.Module):
    def __init__(self, conf_thres, nms_thres, coord_weights, num_class, **kwargs):
        super(RCNNPostProcess, self).__init__()
        self.conf_thres = conf_thres
        self.nms_thres = nms_thres
        self.num_detections = 100
        self.num_class = num_class
        self.bbox_xform_clip = math.log(1000/16)
        coord_weights = torch.tensor(coord_weights).view(1, 1, 4)
        self.register_buffer('coord_weights', coord_weights)

    def decode_box(self, reg, ref, img_size):
        # reg [O, num_classes * 4]
        # ref [O, 4]
        refw = ref[:, 2] - ref[:, 0] # [O,]
        refh = ref[:, 3] - ref[:, 1]
        refcx = ref[:, 0] + 0.5 * refw
        refcy = ref[:, 1] + 0.5 * refh

        # need expend dim here
        N = reg.size(0)
        reg = reg.view(N, -1, 4) / self.coord_weights
        reg[..., 2:] = torch.clamp(reg[..., 2:], max=self.bbox_xform_clip)

        pred_cx = reg[..., 0] * refw.unsqueeze(1) + refcx.unsqueeze(1)
        pred_cy = reg[..., 1] * refh.unsqueeze(1) + refcy.unsqueeze(1)
        pred_w = torch.exp(reg[..., 2]) * refw.unsqueeze(1)
        pred_h = torch.exp(reg[..., 3]) * refh.unsqueeze(1)

        pred_boxes = torch.stack([pred_cx, pred_cy, pred_w, pred_h], dim=-1)
        pred_boxes[..., :2] = pred_boxes[..., :2] - 0.5 * pred_boxes[..., 2:]
        pred_boxes[..., 2:] = pred_boxes[..., 2:] + pred_boxes[..., 0:2]

        # clip into images
        pred_boxes[..., 0::2] = torch.clamp(pred_boxes[..., 0::2], 0, img_size[1])
        pred_boxes[..., 1::2] = torch.clamp(pred_boxes[..., 1::2], 0, img_size[0])

        return pred_boxes

    def forward(self, x, encap_sizes):
        proposals, cls_logits, bbox_reg = x[:3]
        cls_logits = cls_logits.detach()
        bbox_reg = bbox_reg.detach()
        num_prop_per_image = [p.size(0) for p in proposals]
        bbox_reg = torch.split(bbox_reg, num_prop_per_image, 0)

        cls_pred = F.softmax(cls_logits, dim=1)
        cls_labels = torch.arange(cls_pred.shape[1]).view(1, -1).expand_as(cls_pred)
        cls_pred = torch.split(cls_pred, num_prop_per_image, 0)
        cls_labels = torch.split(cls_labels, num_prop_per_image, 0)

        decoded = [self.decode_box(b, p, s) for b, p, s in zip(bbox_reg, proposals, encap_sizes)]
        results = []
        # scores, labels, [num_prop, num_classes]; boxes [num_prop, num_classes, 4]
        for scores, labels, boxes in zip(cls_pred, cls_labels, decoded):
            # eliminate BG
            scores = scores[:, 1:].view(-1)
            labels = labels[:, 1:].view(-1)
            boxes = boxes[:, 1:, :] .view(-1, 4)
            # eliminate small confidence boxes
            conf_mask = scores > self.conf_thres
            if (conf_mask.sum() == 0):
                results.append(None)
                continue

            scores = scores[conf_mask]
            labels = labels[conf_mask]
            boxes = boxes[conf_mask, :]

            size_mask = (boxes[:, 2]-boxes[:, 0] > 1e-2) & (boxes[:, 3]-boxes[:,1] > 1e-2)
            scores = scores[size_mask]
            labels = labels[size_mask]
            boxes = boxes[size_mask]

            # NMS
            nms_mask = torch.zeros_like(scores)
            for i in range(1, self.num_class+1):
                cls_ind = torch.nonzero(labels==i, as_tuple=True)[0]
                keep = torchvision.ops.nms(boxes[cls_ind], scores[cls_ind], self.nms_thres)
                nms_mask[cls_ind[keep]] = 1
            nms_ind = torch.nonzero(nms_mask, as_tuple=True)[0]
            scores = scores[nms_ind]
            labels = labels[nms_ind]
            boxes = boxes[nms_ind]

            scores, ind = torch.sort(scores, dim=0, descending=True)
            scores = scores[:self.num_detections]
            labels = labels[ind[:self.num_detections]]
            boxes = boxes[ind[:self.num_detections]]

            results.append([labels, scores, boxes])

        return results

class CBImageWrapper(CallbackBase):
    def __init__(self, **kwargs):
        super(CBImageWrapper, self).__init__(tag='ImageWrapper')
        self.callback_t = CallbackBase.DATA_PROCESS
        self.encap = RCNNPreProcess(**kwargs)
        self.decap = RCNNPostProcess(**kwargs)
        self.encap_sizes = None
        self.ori_sizes = None

    def on_initializing(self, eng):
        self._cuda(eng.memo['use_cuda'])
        if self.cuda:
            self.encap = self.encap.cuda()
            self.decap = self.decap.cuda()

    def on_batch_begin(self, eng, ctx):
        raw_images = ctx['features']
        raw_targets = ctx.get('targets', {})
        # expect raw_boxes to be [tensors, ...], XYXY fmt
        raw_boxes = raw_targets.get('boxes', None)
        raw_labels = raw_targets.get('labels', None)

        if raw_boxes is not None and not isinstance(raw_boxes[0], torch.Tensor):
            raw_boxes = [torch.from_numpy(b).float() for b in raw_boxes]
        if raw_labels is not None and not isinstance(raw_labels[0], torch.Tensor):
            raw_labels = [torch.from_numpy(l).long() for l in raw_labels]

        self.ori_sizes = [r.shape[-2:] for r in raw_images]
        if self.cuda:
            raw_images = [i.cuda() for i in raw_images]
            if raw_boxes is not None:
                raw_boxes = [b.cuda() for b in raw_boxes]
            if raw_labels is not None:
                raw_labels = [l.cuda() for l in raw_labels]

        batch, boxes, encap_sizes  = self.encap(raw_images, raw_boxes)

        if self.train is not None and self.train:
            batch.requires_grad_(True)
        else:
            batch.requires_grad_(False)

        # update context
        ctx['features'] = batch
        if boxes is not None:
            ctx['targets']['boxes'] = boxes
            ctx['targets']['encap_sizes'] = encap_sizes
            ctx['targets']['labels'] = raw_labels
            ctx['batch_size'] = len(raw_images)

        if isinstance(self.encap, RCNNPreProcess):
            eng.model.image_sizes = encap_sizes
            eng.model.t_boxes = boxes
        # update self memo
        self.encap_sizes = encap_sizes

        return None

    def on_batch_end(self, eng, ctx):
        if self.decap is not None:
            detections = self.decap(ctx['outputs'], self.encap_sizes)
            for i in range(len(detections)):
                if detections[i] is None:
                    continue
                ori_s = self.ori_sizes[i]
                encap_s = self.encap_sizes[i]
                h_scale = float(encap_s[0]) / float(ori_s[0])
                w_scale = float(encap_s[1]) / float(ori_s[1])
                detections[i][2][:, 0::2] /= w_scale
                detections[i][2][:, 1::2] /= h_scale
                if ctx['targets']['boxes'] is not None:
                    ctx['targets']['boxes'][i][:, 0::2] /= w_scale
                    ctx['targets']['boxes'][i][:, 1::2] /= h_scale
            ctx['outputs'] = detections

        return None

if __name__ == '__main__':
    cb = CBImageWrapper(min_size=800, max_size=1333)
    x = [torch.randn(3, 400, 600), torch.randn(3, 1000, 1000)]
    ctx = {'features': x, 'labels': {}}
    cb.on_batch_begin(None, ctx)
    print([out.shape for out in ctx['features'] ])
