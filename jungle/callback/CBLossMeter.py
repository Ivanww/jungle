import torch
import os, pickle
from tensorboardX import SummaryWriter
from collections.abc import Iterable
from .CallbackBase import CallbackBase

# This is for running metric
# Consider using metric meter bundle to evaluate model
class CBLossMeter(CallbackBase):
    def __init__(self, tag='', batch=None, epoch=None, as_criterion=True):
        super(CBLossMeter, self).__init__('loss')
        self.callback_t = CallbackBase.METRIC_METER

        self.interval_batch = batch
        self.interval_epoch = epoch

        self.hist = {'loss_train': [], 'loss_val': []}
        self.cached_data = [0., 0.]

        self.as_criterion = as_criterion

    def _reset(self):
        self.cached_data = [0., 0.]

    def on_initializing(self, eng):
        memo = eng.memo

        ts = memo.get('timestamp', None)
        if ts is None:
            self.workdir = memo['workspace']
        else:
            self.workdir = os.path.join(memo['workspace'], ts)

        self.cached_data = [0., 0.]

    def on_batch_end(self, eng, ctx):
        batch_size = ctx.get('batch_size', None)
        if batch_size is None:
            batch_size = ctx['outputs'].size(0)
        loss_val = ctx['loss'].cpu().numpy()
        self.cached_data[0] += (loss_val * batch_size)
        self.cached_data[1] += batch_size

        return self._handle_return(ctx['progress'][0])

    def _evaluate(self):
        if self.cached_data[1] > 0:
            loss_eval = self.cached_data[0] / self.cached_data[1]
        else:
            loss_eval = 0.
        return loss_eval

    def on_epoch_begin(self, eng, ctx):
        self._reset()

    def _handle_return(self, cur_iter, is_batch=True):
        if is_batch:
            if self.interval_batch is None:
                return None
            if cur_iter % self.interval_batch == 0:
                return self._evaluate()
            else:
                return None
        else:
            if self.interval_epoch is None:
                return None
            if cur_iter % self.interval_epoch == 0:
                return self._evaluate()
            else:
                return None

    def on_epoch_end(self, eng, ctx):
        loss_val = self._evaluate()
        if self.train:
            self.hist['loss_train'].append(loss_val)
        else:
            self.hist['loss_val'].append(loss_val)

        if self.as_criterion:
            ctx['criterion'] = loss_val
        return loss_val

    def on_terminating(self):
        if self.workdir is not None:
            with open(os.path.join(self.workdir, self.tag+'_hist.pkl'), 'wb') as f:
                pickle.dump(self.hist, f)
        return None


