import torch
import os, pickle
import copy
import numpy as np
import pandas as pd
import sklearn.metrics as M
from collections.abc import Iterable
from collections import OrderedDict
from .CallbackBase import CallbackBase

from ..metric import MetricMeter
from ..metric import AccuracyMeter, LambdaMetricMeter
from ..metric import AveragePrecisionMeter

class CBMetricManager(CallbackBase):
    metrics = {
        'accuracy': AccuracyMeter
    }
    def __init__(self, tag='MetricManager', metric_meters=[],save_hist=True):
        super(CBMetricManager, self).__init__(tag)

        self.metric_meters = metric_meters
        self.save_hist = save_hist

        self.workdir = None
        self.as_criterion = False

        self.train = True

    def on_initializing(self, eng):
        metric_fn = OrderedDict()
        for m in self.metric_meters:
            if isinstance(m, MetricMeter):
                metric_fn[m.name] = m
            else:
                raise RuntimeError('Cannot initialize metric')

        self.metric_meters = metric_fn

        memo = eng.memo
        for m in self.metric_meters:
            if m == memo['criterion']:
                self.metric_meters[m].set_criterion()

        ts = memo.get('timestamp', None)
        if ts is None:
            self.workdir = memo['workspace']
        else:
            self.workdir = os.path.join(memo['workspace'], ts)

        for m in self.metric_meters:
            self.metric_meters[m].initialize()

        return None

    def eval(self, train=False):
        if train is None or self.train != train:
            for m in self.metric_meters:
                self.metric_meters[m].clear()
                self.metric_meters[m].train(train)
        super(CBMetricManager, self).eval(train)

    def on_batch_end(self, eng, ctx):
        outputs = ctx['outputs']
        targets = ctx['targets']
        if isinstance(targets, dict):
            targets = targets['label']
        for k in self.metric_meters:
            self.metric_meters[k].accumulate(outputs, targets)
        return None

    def on_epoch_end(self, eng, ctx:dict):
        # if self.train is None:
            # no actions when testing
        #     return None

        metric_values = {}
        # compute metric, save history and return curren metric
        for k in self.metric_meters:
            metric_values[k] = self.metric_meters[k].update()

        for k in self.metric_meters:
            if self.metric_meters[k].is_criterion():
                ctx['criterion'] = metric_values[k]

        for k in self.metric_meters:
            self.metric_meters[k].clear()

        return metric_values

    def on_terminating(self):
        if self.save_hist and self.workdir is not None:
            all_hist = {}
            for k in self.metric_meters:
                hist = self.metric_meters[k].history()
                for m in hist:
                    if len(hist[m]) != 0:
                        all_hist.update({m: hist[m]})
            hist_df = pd.DataFrame.from_dict(all_hist)
            hist_df.to_pickle(os.path.join(self.workdir, 'metric_hist.pkl'))
            return 'Metric History Saved'
        else:
            return None

