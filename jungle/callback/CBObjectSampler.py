import torch
import numpy as np
import torch.nn as nn
import torch.nn.functional as F

from .CallbackBase import CallbackBase

def bbox_iou(box1, box2):
    # use XYXY fmt
    b1_x1, b1_y1, b1_x2, b1_y2 = box1[..., 0], box1[..., 1], box1[..., 2], box1[..., 3]
    b2_x1, b2_y1, b2_x2, b2_y2 = box2[..., 0], box2[..., 1], box2[..., 2], box2[..., 3]

    inter_rect_x1 = torch.max(b1_x1, b2_x1)
    inter_rect_y1 = torch.max(b1_y1, b2_y1)
    inter_rect_x2 = torch.min(b1_x2, b2_x2)
    inter_rect_y2 = torch.min(b1_y2, b2_y2)
 

    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1 + 1, min=0) * torch.clamp(
        inter_rect_y2 - inter_rect_y1 + 1, min=0
    )

    b1_area = (b1_x2 - b1_x1 + 1) * (b1_y2 - b1_y1 + 1)
    b2_area = (b2_x2 - b2_x1 + 1) * (b2_y2 - b2_y1 + 1)

    iou = inter_area / (b1_area + b2_area - inter_area + 1e-16)

    return iou

class ObjectSampler(nn.Module):
    BACKGROUND = -1
    LOWQUALITY = -2
    IGNORED = -3
    def __init__(self, conf_lb, conf_ub, batch_size, pos_ratio):
        super(ObjectSampler, self).__init__()

        self.conf_lb = conf_lb
        self.conf_ub = conf_ub
        self.batch_size = batch_size
        self.pos_rato = pos_ratio
        
    def forward(self, objects, anchors, labels):
        labels = torch.tensor(labels, device=objects.device).long().view(-1)
        # [O, 1, 4] * [1, A, 4] A is the total number of anchors of each scale
        ious = bbox_iou(objects.unsqueeze(1), anchors.unsqueeze(0))
        # [O, A]
        max_obj_iou, matched_obj_idx = ious.max(dim=0)

        # infomation of each anchor, but need an indicator to interpret
        matched_obj_labels = torch.index_select(labels, 0, matched_obj_idx)
        matched_obj_box = objects[matched_obj_idx, :] # [A, 4]

        matched_obj_idx[max_obj_iou < self.conf_lb] = ObjectSampler.BACKGROUND
        matched_obj_idx[(max_obj_iou > self.conf_lb) & (max_obj_iou < self.conf_ub)] = ObjectSampler.LOWQUALITY

        pos = torch.nonzero(matched_obj_idx >= 0)
        neg = torch.nonzero(matched_obj_idx < 0)

        num_pos = min(int(self.batch_size*self.pos_rato), pos.numel())
        num_neg = self.batch_size - num_pos

        pos_idx = torch.randperm(pos.numel(), device=objects.device)[:num_pos]
        neg_idx = torch.randperm(neg.numel(), device=objects.device)[:num_neg]

        obj_indicator = torch.full_like(matched_obj_idx, ObjectSampler.IGNORED).long()
        obj_indicator[pos[pos_idx]] = matched_obj_labels[pos[pos_idx]]
        obj_indicator[neg[neg_idx]] = ObjectSampler.BACKGROUND

        return matched_obj_box, obj_indicator


class CBObjectSampler(CallbackBase):
    def __init__(self, conf_lb=0.3, conf_ub=0.7, batch_size=256, pos_ratio=0.5):
        super(CBObjectSampler, self).__init__('ObjectSampler')
        self.callback_t = CallbackBase.DATA_PROCESS

        self.object_sampler = ObjectSampler(conf_lb, conf_ub, batch_size, pos_ratio)
    
    def on_initializing(self, eng):
        pass

    def on_batch_begin(self, eng, ctx):
        targets = ctx.get('targets', {})

        bboxes = targets.get('boxes', None)
        labels = targets.get('labels', None)
        if bboxes is None:
            return None

        anchors = labels.get('anchors', None)
        if anchors is None:
            raise RuntimeError('No anchors found. Use AnchorHandler to generate anchors')

        anchors = torch.cat(anchors, dim=0)
        obj_boxes = [self.object_sampler(box, anchors) for box in bboxes]

        ctx['labels']['sampled_obj_boxes'] = obj_boxes

        return None
        
if __name__ == "__main__":
    x = torch.randn(5, 4)
    y = torch.randn(10, 4)
    iou = bbox_iou(x.view(5, 1, 4), y.view(1, 10, 4))
    print(iou.shape)