import numpy as np
import pandas as pd
import os
from .CallbackBase import CallbackBase

def to_pickle(result, save_path):
    df = pd.DataFrame.from_dict(result, orient='index')
    df.to_pickle(save_path)

def default_rasult_transform(out):
    out = out.detach().cpu().numpy()
    return {'result': np.split(out, out.shape[0], axis=0)}

class CBPredWritter(CallbackBase):
    def __init__(self, seralize_fn=to_pickle, result_transform=None):
        super(CBPredWritter, self).__init__('PredWritter')
        self.callback_t = CallbackBase.IO_HANDLER

        self.results = None
        self.seralize_fn = seralize_fn
        if result_transform is not None:
            if callable(result_transform):
                self.result_transform = result_transform
            else:
                raise TypeError('Result Transformer Must Be a Callable Object')
        else:
            self.result_transform=default_rasult_transform

    def on_initializing(self, eng):
        memo = eng.memo
        ts = memo.get('timestamp', '')
        if ts is None:
            ts = ''
        if 'prediction' in memo:
            self.save_path = os.path.join(
                memo['workspace'], ts, memo['prediction']
            )
        else:
            self.save_path = os.path.join(
                memo['workspace'], ts, 'result.pkl'
            )

    def on_batch_end(self, eng, ctx):
        if 'predictions' in ctx:
            outputs = ctx['predictions']
        else:
            outputs = ctx['outputs']

        if self.result_transform is not None:
            outputs = self.result_transform(outputs)

        ids = ctx['targets'].get('id', None)
        results = {'ID': ids}
        results.update(outputs)
        batch_result = pd.DataFrame.from_dict(results)
        if self.results is None:
            self.results = batch_result
        else:
            self.results = self.results.append(batch_result, ignore_index=True, sort=False)

        return None

    def on_terminating(self):
        # HOTFIX
        print(self.results.head())
        self.results.to_pickle(self.save_path)
        return 'Result Saved!'
