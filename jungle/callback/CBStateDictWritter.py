import torch
import shutil, datetime, os

from .CallbackBase import CallbackBase

class CBStateDictWritter(CallbackBase):
    def __init__(self, milestones=[], keep_all=False, min_best=True, save_env=True):
        super(CBStateDictWritter, self).__init__('StateDictWritter')

        self.callback_t = CallbackBase.IO_HANDLER

        self.save_env = save_env
        self.best = None
        self.min_best = min_best
        self.keep_all = keep_all
        if self.keep_all:
            self.milestones = []
        else:
            self.milestones = milestones

    def _save_statedict(self, n_epoch, criterion, model, optimizer):
        status = None
        state_dict = {
            'epoch': n_epoch,
            # 'state_dict': m.module.state_dict() if self.use_DP else m.state_dict(),
            'state_dict': model.state_dict(),
            'optimizer': optimizer.state_dict(),
            'best_criterion': criterion
        }
        torch.save(state_dict, self.temp_name)
        if self.best is None:
            self.best = criterion
            shutil.copy(self.temp_name, self.best_name)
            status = 'Save Model (None -> {:.4f})'.format(self.best)
        else:
            if self.min_best:
                if criterion < self.best:
                    shutil.copy(self.temp_name, self.best_name)
                    status = 'Save Model ({:.4f} -> {:.4f})'.format(self.best, criterion)
                    self.best = criterion
            else:
                if criterion > self.best:
                    shutil.copy(self.temp_name, self.best_name)
                    status = 'Save Model ({:.4f} -> {:.4f})'.format(self.best, criterion)
                    self.best = criterion

        if self.keep_all:
            save_name = os.path.join(self.workspace, 'state_dict_at_epoch_{}.pth.tar'.format(n_epoch))
            shutil.copy(self.temp_name, save_name)

        if n_epoch in self.milestones:
            save_name = os.path.join(self.workspace, 'state_dict_at_epoch_{}.pth.tar'.format(n_epoch))
            shutil.copy(self.temp_name, save_name)

        return status


    def on_initializing(self, eng):
        memo = eng.memo
        ckpt = memo.get('ckpt', 'best_model.pth.tar')
        self.use_cuda = memo.get('use_cuda', False)

        self.workspace = os.path.join(
            memo['workspace'], memo['timestamp']
        )

        self.temp_name = os.path.join(memo['workspace'], memo['timestamp'], 'latest_model.pth.tar')
        self.best_name = os.path.join(memo['workspace'], memo['timestamp'], ckpt)

    def on_epoch_end(self, eng, ctx):
        criterion = ctx['criterion']
        if criterion is None:
            raise RuntimeError('Cannot Find Criterion')
        model = eng.model
        optimizer = eng.optimizer
        n_epoch = ctx['epoch']

        if self.train is None:
            # this is an error..
            return None

        if not self.train:
            # if isinstance(model, list):
            #     ret = self.__save_statedict(n_epoch, criterion, model, optimizer)
            # else:
            ret = self._save_statedict(n_epoch, criterion, model, optimizer)

            return ret


