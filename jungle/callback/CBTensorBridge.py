import torch
import numpy as np
from .CallbackBase import CallbackBase

class CBTensorBridge(CallbackBase):
    def __init__(self, how=None):
        super(CBTensorBridge, self).__init__('TensorBridge')
        self.callback_t = CallbackBase.DATA_PROCESS
        self.use_cuda = True
        self.msg = ''
        self.show_msg = True
        self.move_fn = how

    def on_initializing(self, eng):
        self.use_cuda = eng.memo['use_cuda']

    def on_batch_begin(self, eng, ctx):
        # ndarray -> tensor
        if self.move_fn is None:
            if isinstance(ctx['features'], np.ndarray):
                ctx['features'] = torch.from_numpy(ctx['features'])
            elif isinstance(ctx['features'], list):
                ctx['features'] = [torch.from_numpy(t) for t in ctx['features']]

            if isinstance(ctx['targets'], np.ndarray):
                ctx['targets'] = torch.from_numpy(ctx['targets'])
            elif isinstance(ctx['targets'], list):
                ctx['targets'] = [torch.from_numpy(t) for t in ctx['targets']]

            # tensor -> CUDA tensor
            if self.use_cuda:
                if isinstance(ctx['features'], torch.Tensor):
                    ctx['features'] = ctx['features'].cuda()
                    ctx['features'].requires_grad_(True if self.train else False)
                elif isinstance(ctx['features'], list):
                    ctx['features'] = [t.cuda() for t in ctx['features']]
                    for t in ctx['features']:
                        t.requires_grad_(True if self.train else False)
                else:
                    self.msg += 'feature data are not tensor or list of tensors'

                if isinstance(ctx['targets'], torch.Tensor):
                    ctx['targets'] = ctx['targets'].cuda()
                elif isinstance(ctx['targets'], list):
                    ctx['targets'] = [t.cuda() for t in ctx['targets']]
                else:
                    self.msg += 'label data are not tensor'
        else:
            self.msg += self.move_fn(ctx, self.use_cuda)

        if self.show_msg and len(self.msg) > 0:
            self.show_msg = False
            return self.msg
        else:
            return None
