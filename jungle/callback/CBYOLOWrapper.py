import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.transforms import ToTensor
import numpy as np
from PIL.Image import Image
from skimage.transform import resize

from .CallbackBase import CallbackBase

def coord_fmt_transform(coord, from_fmt, to_fmt):
    if to_fmt == 'CXCY':
        if from_fmt == 'XYWH':
            coord[:, 0:2] = coord[:, 0:2] + coord[:, 2:4] / 2
        elif from_fmt == 'XYXY':
            coord[:, 2:4] = coord[:, 2:4] - coord[:, 0:2]
            coord[:, 0:2] = coord[:, 0:2] + coord[:, 2:4] / 2
        else:
            raise ValueError('Unknown from_fmt {}'.format(to_fmt))
    elif to_fmt == 'XYWH':
        if from_fmt == 'CXCY':
            coord[:, :2] -= coord[:, 2:4] / 2
        elif from_fmt == 'XYXY':
            coord[:, 2:4] -= coord[:, :2]
        else:
            raise ValueError('Unknown from_fmt {}'.format(to_fmt))
    elif to_fmt == 'XYXY':
        if from_fmt == 'CXCY':
            coord[:, :2] -= coord[:, 2:4] / 2
            coord[:, 2:4] += coord[:, :2]
        elif from_fmt == 'XYWH':
            coord[:, 2:4] += coord[:, :2]
        else:
            raise ValueError('Unknown from_fmt {}'.format(to_fmt))
    else:
        raise ValueError('Unknown from_fmt {}'.format(to_fmt))

    return coord

def coord_geo_transform(coord, pad, scale):
    scaled = coord.copy()

    offx, offy = pad[1][0], pad[0][0]
    scaled[:, 0] += offx
    scaled[:, 1] += offy
    scaled *= scale

    return scaled

def coord_inv_geo_transform(coords, pad, scale):
    coords = coord_fmt_transform(coords, 'XYXY', 'CXCY')
    coords /= scale

    offx, offy = pad[1][0], pad[0][0]
    coords[:, 0] -= offx
    coords[:, 1] -= offy

    return coords


def bbox_iou_torch(box1, box2):
    b1_x1, b1_y1, b1_x2, b1_y2 = box1[:, 0], box1[:, 1], box1[:, 2], box1[:, 3]
    b2_x1, b2_y1, b2_x2, b2_y2 = box2[:, 0], box2[:, 1], box2[:, 2], box2[:, 3]

    inter_rect_x1 = torch.max(b1_x1, b2_x1)
    inter_rect_y1 = torch.max(b1_y1, b2_y1)
    inter_rect_x2 = torch.min(b1_x2, b2_x2)
    inter_rect_y2 = torch.min(b1_y2, b2_y2)

    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1, min=0) * torch.clamp(
        inter_rect_y2 - inter_rect_y1, min=0
    )

    b1_area = (b1_x2 - b1_x1) * (b1_y2 - b1_y1)
    b2_area = (b2_x2 - b2_x1) * (b2_y2 - b2_y1)

    iou = inter_area / (b1_area + b2_area - inter_area)

    return iou

def non_maximum_suppression(preds, img_size, obj_conf_thres, nms_thres, num_class):
    # preds = torch.cat(preds, dim=0).view(, -1)
    # move out this function
    coords = preds[:, :4]
    coords = coord_fmt_transform(coords, 'CXCY', 'XYXY')
    coords = torch.clamp(coords, min=0., max=img_size)

    obj_conf_scores = preds[:, 4]
    cls_conf_scores = preds[:, 5:]

    coords = coords[obj_conf_scores > obj_conf_thres]
    cls_conf_scores = cls_conf_scores[obj_conf_scores > obj_conf_thres]

    if coords.size(0) == 0:
        # no object detected
        return None
    cls_score, class_preds = cls_conf_scores.max(1)
    cls_score = cls_score * obj_conf_scores[obj_conf_scores > obj_conf_thres]
    coords = coords[torch.argsort(cls_score, descending=True)]

    keep_box = []
    for cls_label in range(num_class):
        cls_mask = class_preds == cls_label
        if cls_mask.sum() == 0:
            continue
        detections = torch.cat([coords[cls_mask, :],
            cls_score[cls_mask].float().unsqueeze(-1),
            class_preds[cls_mask].float().unsqueeze(-1)], dim=1)
        while detections.size(0) > 0:
            keep_box.append(detections[0])
            ious = bbox_iou_torch(detections[0:1, :4], detections[:, :4])
            detections = detections[ious.view(-1) < nms_thres]
    if len(keep_box) == 0:
        return None
    else:
        return torch.stack(keep_box)

def bbox_iou(bboxes, anchors):
    boxwh = np.expand_dims(bboxes[:, 2:4], 1)
    anchorwh = np.expand_dims(anchors, 0)

    wh = np.minimum(boxwh, anchorwh)
    intersec = wh[..., 0] * wh[..., 1]
    union = (boxwh[..., 0] * boxwh[..., 1]) + (anchorwh[..., 0] * anchorwh[..., 1]) - intersec

    return intersec / np.clip(union, a_min=1e-16, a_max=None)

def coord_loss_func_transform(coord, scale, anchors):
    tcoord = coord / scale

    offset = (coord[:, 0:2] // scale).astype('int')
    tcoord[:, 0:2] -= offset

    ious = bbox_iou(coord, anchors)
    best_anchor_idx = ious.argmax(axis=1)
    best_anchors = anchors[best_anchor_idx, :]
    tcoord[:, 2:4] = np.log(tcoord[:, 2:4] / best_anchors)

    tcoord = torch.from_numpy(tcoord).float()
    offset = torch.from_numpy(offset).float()
    best_anchor_idx = torch.from_numpy(best_anchor_idx).float()

    return tcoord, offset, best_anchor_idx

def coord_inv_bbox_transform(output, scale, stride, anchors):
    output[..., 0:2] = torch.sigmoid(output[..., 0:2])
    output[..., 2:4] = torch.exp(output[..., 2:4])
    output[..., 4] = torch.sigmoid(output[..., 4])
    # Classification
    output[..., 5:] = torch.sigmoid(output[..., 5:])

    offset = torch.arange(stride).repeat(stride, 1)
    output[..., 0] += offset.view(1, -1, 1)
    output[..., 1] += offset.t().contiguous().view(1, -1, 1)

    anchor_t = torch.from_numpy(anchors).to(torch.float).view(
        1, 1, -1, 2)
    output[...,2:4] *= anchor_t

    output[..., :4] *= scale
    return output

# YOLO requires box coordinates to be 'CXCY'
class YOLOPreProcess(nn.Module):
    def __init__(self, feas_dims=608):
        super(YOLOPreProcess, self).__init__()

        self.p_value= 114./255.
        if not isinstance(feas_dims, list):
            self.feas_dims = torch.tensor([feas_dims]).long()
        else:
            self.feas_dims = torch.tensor(feas_dims).long()

    def _to_square(self, img):
        h, w = img.shape[-2:]
        dim_diff = np.abs(h - w)
        pad1, pad2 = dim_diff // 2, dim_diff - dim_diff //2
        # (left, right, top, bottom)
        pad = (pad1, pad2, 0, 0) if h >= w else (0, 0, pad1, pad2)
        img = torch.nn.functional.pad(img, pad, mode='constant', value=self.p_value)

        return img, pad

    def _resize(self, square_img, feas_dim):
        scale = float(feas_dim) / square_img.shape[-2]
        # need extra 'batch' dimenson
        # use align corner = False to be consistency with openCV
        image = F.interpolate(square_img.unsqueeze(0), feas_dim, mode='bilinear', align_corners=False)

        return image, scale

    def _box_coord_transform(self, box, pad, ratio):
        # need XYXY fmt
        scaled = box.clone()

        offx, offy = pad[0], pad[2]
        scaled[:, 0::2] += offx
        scaled[:, 1::2] += offy
        scaled *= ratio

        return scaled

    def forward(self, images, boxes=None):
        # choose dim for this batch
        feas_dim = self.feas_dims[torch.randperm(self.feas_dims.size(0))[0]]

        squares = [self._to_square(i) for i in images]
        resized = [self._resize(i[0], feas_dim) for i in squares]
        batch = torch.cat([r[0] for r in resized], dim=0)
        scales = [r[1] for r in resized]
        pads = [i[1] for i in squares]
        encap_sizes = feas_dim

        if boxes is not None:
            boxes = [self._box_coord_transform(b, p[1], s)
                for b, p, s in zip(boxes, squares, scales)]

        return batch, boxes, encap_sizes, scales, pads

class YOLOPostProcess(nn.Module):
    def __init__(self, strides, anchors, obj_thres, nms_thres, num_class):
        super(YOLOPostProcess, self).__init__()
        self.anchors = anchors
        self.num_anchor = len(self.anchors[0])
        self.strides = strides # scales
        self.obj_thres = obj_thres
        self.nms_thres = nms_thres
        self.num_class = num_class

    def _inv_bbox_regression(self, bbox_delta, stride, cell_size, anchors):
        bbox_delta[..., 0:2] = torch.sigmoid(bbox_delta[..., 0:2])
        bbox_delta[..., 2:4] = torch.exp(bbox_delta[..., 2:4])
        bbox_delta[..., 4] = torch.sigmoid(bbox_delta[..., 4])
        # Classification
        bbox_delta[..., 5:] = torch.sigmoid(bbox_delta[..., 5:])

        offset = torch.arange(cell_size).repeat(cell_size, 1)
        bbox_delta[..., 0] += offset.view(1, -1, 1)
        bbox_delta[..., 1] += offset.t().contiguous().view(1, -1, 1)

        anchors = anchors.view(1, 1, -1, 2)
        bbox_delta[...,2:4] *= anchors

        bbox_delta[..., :4] *= stride
        return bbox_delta

    def forward(self, out, img_size):
        # image_size = scale(strides) * cell_size
        batch_size = out[0].size(0)
        cell_sizes = [img_size // s for s in self.strides]
        out = [t.detach().view(batch_size, self.num_anchor, -1 , S*S)
            for t, S in zip(out, cell_sizes)]
        outputs = [t.permute(0, 3, 1, 2).contiguous() for t in out]

        # recover bbox coordinate to feature size
        outputs = [self._inv_bbox_regression(t, stride, cell_size, anchors)
            for t, stride, cell_size, anchors in zip(outputs, self.strides, cell_sizes, self.anchors)]

        preds = []
        for i in range(batch_size):
            # outputs for each images
            out = [o[i] for o in outputs]
            out = torch.cat(out, dim=0).view(-1, 5+self.num_class)
            preds.append(non_maximum_suppression(out, img_size,
                self.obj_thres, self.nms_thres, self.num_class))

        return preds

DEFAULT_YOLO_ANCHORS = [
    [(116,90),  (156,198),  (373,326)],
    [(30,61),  (62,45),  (59,119)],
    [(10,13),  (16,30),  (33,23)]
    ]
DEFAULT_YOLO_STRIDES = [32, 16, 8]
class CBYOLOWrapper(CallbackBase):
    def __init__(self, feas_dims=608, strides=DEFAULT_YOLO_STRIDES, anchors=DEFAULT_YOLO_ANCHORS,
        obj_thres=0.8, nms_thres=0.5, num_class=80, pred_thres=0.5):
        super(CBYOLOWrapper, self).__init__(tag='YOLOWrapper')
        self.callback_t = CallbackBase.DATA_PROCESS

        self.encap = YOLOPreProcess(feas_dims)
        self.decap = YOLOPostProcess(strides, anchors, obj_thres, nms_thres, num_class)

        self.raw_images = None
        self.raw_labels = None

        self.encap_size = None
        self.batch_scale_ratios = None
        self.batch_pad = None

    def on_initializing(self, eng, ctx):
        self._cuda(eng.memo['use_cuda'])
        if self.cuda:
            self.encap = self.encap.cuda()
            self.decap = self.decap.cuda()

    def on_batch_begin(self, ctx):
        raw_images = ctx['features']
        raw_targets = ctx['targets']

        raw_boxes = raw_targets.get['boxes', None]

        if self.cuda:
            raw_images = [i.cuda() for i in raw_images]
            if raw_boxes is not None:
                raw_boxes = [b.cuda() for b in raw_boxes]
            # if raw_labels is not None:
                # raw_labels = [l.cuda() for l in raw_labels]

        batch, boxes, encap_sizes, scale_ratios, pads = self.encap(raw_images, raw_boxes)

        if self.train is not None and self.train:
            batch.requires_grad_(True)
        else:
            batch.requires_grad_(False)

        ctx['features'] = batch
        if boxes is not None:
            ctx['targets']['boxes'] = boxes
            ctx['targets']['encap_sizes'] = encap_sizes

        self.encap_size = encap_sizes
        self.batch_scale_ratios = scale_ratios
        self.batch_pad = pads

        return None

    def on_batch_end(self, ctx):
        outputs = ctx['outputs']
        preds = self.decap(outputs, self.encap_size)

        detections = []
        for i, pred in enumerate(preds):
            if pred is None:
                detections.append(None)
            else:
                coord = pred[:, :4]
                coord /= self.batch_scale_ratios[i]
                padx, pady = self.batch_pad[i][0], self.batch_pad[i][2]
                coord[:, 0::2] -= padx
                coord[:, 1::2] -= pady
                coord = torch.clamp(coord, min=0, max=self.encap_size).numpy()
                cls_scores = pred[:, 4].numpy()
                cls_label = pred[i][:, 5].long().numpy()
                detections.append((cls_label, cls_scores, coord))
        ctx['outputs'] = detections
        return None