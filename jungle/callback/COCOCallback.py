import cv2
import torch
import numpy as np
from PIL.Image import Image

from .CallbackBase import CallbackBase
from .LetterBox import LetterBox

COCO_CATE_2017 = [None,
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10, None, 11, 12, 13, 14, 15, 16, 17, 18,
    19, 20, 21, 22, 23, None, 24, 25, None, None,
    26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
    36, 37, 38, 39, None, 40, 41, 42, 43, 44,
    45, 46, 47, 48, 49, 50, 51, 52, 53, 54,
    55, 56, 57, 58, 59, None, 60, None, None, 61,
    None, 62, 63, 64, 65, 66, 67, 68, 69, 70,
    71, 72, None, 73, 74, 75, 76, 77, 78, 79, None]

def _coco_extract_box(annotation, fmt):
    bboxes = np.array([t['bbox'] for t in annotation], dtype=np.float).reshape((-1, 4))
    labels = np.array(
        [COCO_CATE_2017[int(t['category_id'])] for t in annotation], dtype=np.int
        ).reshape((-1, 1))
    
    # convert XYWH to XYXY
    if fmt == 'XYWH':
        bboxes[:, 2:4] += bboxes[:, 0:2] / 2
    elif fmt == 'XYXY':
        bboxes[:, 2:4] -= bboxes[:, 0:2] # WH
        bboxes[:, 0:2] += bboxes[:, 2:4] / 2 # CXCY
    annotation = np.concatenate([labels, bboxes], axis=-1)
    return annotation

class COCOCallback(LetterBox):
    def __init__(self, feas_dim=416, bbox_format='XYWH'): 
        super(COCOCallback, self).__init__('COCO Data Processing', feas_dim,
            label_transform=lambda x: _coco_extract_box(x, bbox_format))

        self.bbox_format = bbox_format
        fmts = ['XYWH', 'XYXY']
        if not self.bbox_format in fmts:
            raise ValueError('BBOX format can only be {}'.format(fmts))