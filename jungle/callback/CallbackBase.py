class CallbackBase(object):
    METRIC_METER = 1
    DATA_PROCESS = 2
    IO_HANDLER = 3

    def __init__(self, tag = '', **kwargs):
        self.tag = tag
        self.train = True
        self.callback_t = None
        self.cuda = False

    def eval(self, train=False):
        # True -> train, False -> validation, None -> Test
        self.train = train

    def _cuda(self, use_cuda=False):
        self.cuda = use_cuda
        return None

    def on_initializing(self, eng):
        return None

    def on_epoch_end(self, eng, ctx={}):
        return None

    def on_epoch_begin(self, eng, ctx={}):
        return None

    def on_batch_begin(self, eng, ctx={}):
        return None

    def on_batch_end(self, eng, ctx={}):
        return None

    def on_terminating(self, ctx=None):
        return None

