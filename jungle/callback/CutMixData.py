import torch 
import numpy as np

from .CallbackBase import CallbackBase

# https://www.kaggle.com/c/bengaliai-cv19/discussion/126504
# https://github.com/facebookresearch/mixup-cifar10
class CutMixData(CallbackBase):
    def __init__(self, tag='CutMixData', alpha=1.0, cutmix_ratio=0.5, asis_ratio=None):
        super(CutMixData, self).__init__(tag)
        self.callback_t = CallbackBase.DATA_PROCESS
        self.alpha = 1.0
        self.cutmix_ratio = 0.5
        self.asis_ratio = asis_ratio
        self.raw_features = None
        self.raw_labels = None

    def __mixup(self):
        if self.alpha > 0:
            lam = np.random.beta(self.alpha, self.alpha)
        else:
            lam = 1.

        batch_size = self.raw_features.size(0)
        index = torch.randperm(batch_size)

        mixed_features = lam * self.raw_features + (1-lam) * self.raw_features[index]
        mixed_labels = torch.stack([self.raw_labels, self.raw_labels[index]], dim=0)

        return mixed_features, mixed_labels, lam
    
    def __cutmix(self):
        # shuffle data
        indices = torch.randperm(self.raw_features.size(0))
        mixed_features = self.raw_features.clone()

        if self.alpha > 0:
            lam = np.random.beta(self.alpha, self.alpha)
        else:
            lam = 1.

        # random bbox
        H = self.raw_features.size(2)
        W = self.raw_features.size(3)

        cut_rat = np.sqrt(1. - lam)
        cut_w = np.int(W * cut_rat)
        cut_h = np.int(H * cut_rat)

        cx = np.random.randint(W)
        cy = np.random.randint(H)

        bbx1 = np.clip(cx - cut_w // 2, 0, W)
        bby1 = np.clip(cy - cut_h // 2, 0, H)
        bbx2 = np.clip(cx + cut_w // 2, 0, W)
        bby2 = np.clip(cy + cut_h // 2, 0, H)

        mixed_features[:, :, bbx1:bbx2, bby1:bby2] = mixed_features[indices, :, bbx1:bbx2, bby1:bby2]
        # adjust lambda to exactly match pixel ratio
        lam = 1 - ((bbx2 - bbx1) * (bby2 - bby1) / (H * W))

        mixed_labels = torch.stack([self.raw_labels, self.raw_labels[indices]], dim=0)

        return mixed_features, mixed_labels, lam

    def on_batch_begin(self, ctx):
        self.raw_features = ctx['features']
        self.raw_labels = ctx['labels']

        if self.train is None or not self.train:
            # only mix up during training
            ctx['extra_bundle'].update({'lam': None})
            return None
        
        if self.asis_ratio is not None and np.random.uniform() < self.asis_ratio:
            # leavt it as is
            ctx['extra_bundle'].update({'lam': None})
            return None

        if np.random.uniform() < self.cutmix_ratio:
            mixed_features, mixed_labels, lam = self.__cutmix()
        else:
            mixed_features, mixed_labels, lam = self.__mixup()

        ctx['features'] = mixed_features
        ctx['labels'] = mixed_labels
        ctx['extra_bundle'].update({'lam': lam})

        return None

    def on_batch_end(self, ctx):
        # recover labels
        ctx['labels'] = self.raw_labels

        return None

    