import torch
import os
import numpy as np
import matplotlib.pyplot as plt
import cv2
from .CallbackBase import CallbackBase

# Reference: https://github.com/yizt/Grad-CAM.pytorch
class GradientHook(object):
    def __init__(self, model):
        super(GradientHook, self).__init__()
        self.hook = model.register_backward_hook(self.hook_fn)

    def hook_fn(self, module, input_grad, output_grad):
        self.gradient = output_grad[0]

    def remove(self):
        self.features = None
        self.hook.remove()

class FeatureMapHook(object):
    def __init__(self, model):
        super(FeatureMapHook, self).__init__()
        self.hook = model.register_forward_hook(self.hook_fn)

    def hook_fn(self, model, input, output):
        self.features = output
    
    def remove(self):
        self.features = None
        self.hook.remove()

class GradientCam(CallbackBase):
    def __init__(self, last_conv, save_dir=None):
        super(GradientCam, self).__init__('Gradient CAM')
        self.callback_t = CallbackBase.DATA_PROCESS

        self.last_conv = last_conv
        self.hooks = []
        self.save_dir = save_dir
        self.raw_images = None
        self.feature_hook = None
        self.gradient_hook = None
    
    def __init_cam_dir(self, sketch):
        if sketch['OTG']:
            return './GradCAM'
        else:
            return os.path.join(sketch['workspace'], sketch['timestamp'], 'GradCAM')

    def on_initializing(self, ctx):
        # add hook
        model = ctx['model']
        self.gradient_hook = GradientHook(dict(model.named_modules())[self.last_conv])
        self.feature_hook = FeatureMapHook(dict(model.named_modules())[self.last_conv])
        # save dir
        if self.save_dir is None:
            self.save_dir = self.__init_cam_dir(ctx['sketch'])
        
        if not os.path.isdir(self.save_dir):
            os.mkdir(self.save_dir)
        
        return None
    
    def __get_CAM(self, feature, idx):
        _, nc, h, w = feature.shape
        cam = self.linear_w[idx].dot(self.hook.features.reshape((nc, h*w)))
        cam = cam.reshape(h, w)
        cam = cam - np.min(cam)
        cam_image = cam / np.max(cam)
        return [cam_image]

    def on_batch_begin(self, ctx):
        # store original images
        batch_size = ctx['features'].shape[0]
        if batch_size != 1:
            raise RuntimeError('Batchsize should be 1 when using Gradient CAM')
        else:
            self.raw_shape = tuple(ctx['features'].shape[2:])
            raw_images = ctx['features'].permute(0, 2, 3, 1)
            self.raw_images = raw_images[0].numpy()

            ctx['features'].requires_grad_(True)
        return None

    def on_batch_end(self, ctx):
        # outputs
        outputs = ctx['outputs']
        pred_cls = torch.argmax(outputs.detach().cpu(), dim=1)[0] # model prediction
        
        # Calculate Gradient
        target = outputs[0][pred_cls]
        target.backward()

        gradients = self.gradient_hook.gradient[0].detach().cpu().numpy()
        gradients_w = np.mean(gradients, axis=(1, 2))
        
        features = self.feature_hook.features[0].detach().cpu().numpy()

        cam = features * gradients_w[:, np.newaxis, np.newaxis]
        cam = np.sum(cam, axis=0)
        cam = np.maximum(cam, 0)

        # Normalize
        cam -= np.min(cam)
        cam /= np.max(cam)
        cam = cv2.resize(cam, self.raw_shape)
        # Heatmap
        heatmap = cv2.applyColorMap(np.uint8(255 * cam), cv2.COLORMAP_JET)
        heatmap = np.float32(heatmap) / 255
        # heatmap = heatmap[..., ::-1]
        # Combine
        cam = heatmap + np.float32(self.raw_images)
        cam -= np.max(np.min(cam), 0)
        cam /= np.max(cam)
        cam *= 255.
        cam = np.uint8(cam)
        # Save images
        save_name = os.path.join(self.save_dir, ctx['labels'][0]+'.png')
        cv2.imwrite(save_name, cam)
        
        # cleanup model grad for next batch
        ctx['model'].zero_grad()
        return None
        
        
    def on_terminating(self):
        if self.gradient_hook is not None:
            self.gradient_hook.remove()
        if self.feature_hook is not None:
            self.feature_hook.remove()