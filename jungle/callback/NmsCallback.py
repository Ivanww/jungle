import torch
import torch.nn.functional as F
from .CallbackBase import CallbackBase

# This callback performs Non-Maxmial Supression on output data in the following way
# 1. Filter the bbox using a 'conf_thres'
# 2. For bboxes in the same image and for the same categorical object, filter 
#    them uing 'nms_thres'.
#    2.1 Keep the bbox with highest confidence
#    2.2 Remove those that has high IOU with the first bbox

def bbox_iou(box1, box2, use_XYXY=True):
    if not use_XYXY:
        # Transform from center and width to exact coordinates
        b1_x1, b1_x2 = box1[:, 0] - box1[:, 2] / 2, box1[:, 0] + box1[:, 2] / 2
        b1_y1, b1_y2 = box1[:, 1] - box1[:, 3] / 2, box1[:, 1] + box1[:, 3] / 2
        b2_x1, b2_x2 = box2[:, 0] - box2[:, 2] / 2, box2[:, 0] + box2[:, 2] / 2
        b2_y1, b2_y2 = box2[:, 1] - box2[:, 3] / 2, box2[:, 1] + box2[:, 3] / 2
    else:
        # Get the coordinates of bounding boxes
        b1_x1, b1_y1, b1_x2, b1_y2 = box1[:, 0], box1[:, 1], box1[:, 2], box1[:, 3]
        b2_x1, b2_y1, b2_x2, b2_y2 = box2[:, 0], box2[:, 1], box2[:, 2], box2[:, 3]

    # get the corrdinates of the intersection rectangle
    inter_rect_x1 = torch.max(b1_x1, b2_x1)
    inter_rect_y1 = torch.max(b1_y1, b2_y1)
    inter_rect_x2 = torch.min(b1_x2, b2_x2)
    inter_rect_y2 = torch.min(b1_y2, b2_y2)
    # Intersection area
    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1 + 1, min=0) * torch.clamp(
        inter_rect_y2 - inter_rect_y1 + 1, min=0
    )
    # Union Area
    b1_area = (b1_x2 - b1_x1 + 1) * (b1_y2 - b1_y1 + 1)
    b2_area = (b2_x2 - b2_x1 + 1) * (b2_y2 - b2_y1 + 1)

    iou = inter_area / (b1_area + b2_area - inter_area + 1e-16)

    return iou

class NmsCallback(CallbackBase):
    def __init__(self, anchors, scale, feas_dim, conf_thres, nms_thres, num_class, is_anchor_scaled=False):
        # anchors, feas_dim, scale, conf_thres, nms_thres, num_class):
        super(NmsCallback, self).__init__('Non-Maximal Supression')
        self.callback_type = CallbackBase.DATA_PROCESS

        self.num_classes = num_class
        self.anchors = anchors
        self.scale = scale
        if isinstance(self.scale, list):
            self.multiscale = True
        else:
            self.multiscale = False
        self.feas_dim = feas_dim
        self.conf_thres = conf_thres
        self.nms_thres = nms_thres
        self.is_anchor_scaled = is_anchor_scaled

    
    def on_initializing(self, ctx):
        if self.multiscale:
            if len(self.feas_dim) != len(self.scale):
                raise ValueError('Number of feature dimennsion and number of scales mismatch')
            if len(self.anchors) != len(self.scale):
                raise ValueError('Number of anchors and number of scales mismatch')

            self.xoffset = []
            self.yoffset = []
            for fs in self.feas_dim:
                yy, xx = torch.meshgrid(torch.arange(fs), torch.arange(fs))
                self.xoffset.append(xx.view(1, 1, fs, fs))
                self.yoffset.append(yy.view(1, 1, fs, fs))

            self.num_anchors = [len(x) for x in self.anchors]
            self.anchor_w = []
            self.anchor_h = []
            for na, branch_anchor, scale in zip(self.num_anchors, self.anchors, self.scale):
                if self.is_anchor_scaled:
                    self.anchor_w.append(torch.tensor([a[0] for a in branch_anchor]).view(1, na, 1, 1))
                    self.anchor_h.append(torch.tensor([a[1] for a in branch_anchor]).view(1, na, 1, 1))
                else:
                    self.anchor_w.append(torch.tensor([a[0]/scale for a in branch_anchor]).view(1, na, 1, 1))
                    self.anchor_h.append(torch.tensor([a[1]/scale for a in branch_anchor]).view(1, na, 1, 1))
        else:
            yy, xx = torch.meshgrid(torch.arange(self.feas_dim), torch.arange(self.feas_dim))
            self.xoffset = xx.view(1, 1, self.feas_dim, self.feas_dim)
            self.yoffset = yy.view(1, 1, self.feas_dim, self.feas_dim)
            self.num_anchors = len(self.anchors)
            if self.is_anchor_scaled:
                self.anchor_w = torch.tensor([a[0] for a in self.anchors]).view(1, len(self.anchors), 1, 1)
                self.anchor_h = torch.tensor([a[1] for a in self.anchors]).view(1, len(self.anchors), 1, 1)
            else:
                self.anchor_w = torch.tensor([a[0]/self.scale for a in self.anchors]).view(1, len(self.anchors), 1, 1)
                self.anchor_h = torch.tensor([a[1]/self.scale for a in self.anchors]).view(1, len(self.anchors), 1, 1)

    def _processing_yolo_result(self, data, num_anchors, feas_dim, scale, xoffset, yoffset, anchor_w, anchor_h):
        batch_size = data.size(0)
        out = data.view(batch_size, num_anchors, self.num_classes+5, feas_dim,
            feas_dim).permute(0, 1, 3, 4, 2).contiguous()
            
        out[..., 0] = torch.sigmoid(out[..., 0]) + xoffset
        out[..., 1] = torch.sigmoid(out[..., 1]) + yoffset
        out[..., 2] = torch.exp(out[..., 2]) * anchor_w
        out[..., 3] = torch.exp(out[..., 3]) * anchor_h
        out[..., :4] *= scale

        out[..., 4:] = torch.sigmoid(out[..., 4:])

        out = out.view(batch_size, -1, 5+self.num_classes)

        return out

    def on_batch_end(self, ctx):
        # reshape output, this is like the loss
        if self.multiscale:
            out = [o.detach() for o in ctx['outputs']]
            if len(out) != len(self.scale):
                raise RuntimeError('Data and scale mismatch')
            
            out = [self._processing_yolo_result(
                out[i], self.num_anchors[i], self.feas_dim[i], self.scale[i], self.xoffset[i], self.yoffset[i],
                self.anchor_w[i], self.anchor_h[i]
            ) for i in range(len(out))]
            out = torch.cat(out, dim=1)
            
        else:
            out = ctx['outputs'].detach()
            out = self._processing_yolo_result(
                out, self.num_anchors, self.feas_dim, self.scale, self.xoffset, self.yoffset, self.anchor_w, self.anchor_h
            )
        
        # convert cxcx XYXY
        out[..., :2] -= out[..., 2:4] / 2
        out[..., 2:4] += out[..., :2]

        bboxes = [None for _ in range(out.size(0))]
        for idx, preds in enumerate(out):
            preds = preds[preds[:, 4] > self.conf_thres]

            if preds.size(0) == 0:
                continue
            # sort prediction by class confidence
            class_score = preds[:, 4] * preds[:, 5:].max(1)[0]
            preds = preds[torch.argsort(class_score, descending=True)]
            class_confs, class_preds = preds[:, 5:].max(1, keepdim=True)
            detections = torch.cat([preds[:, :5], class_confs.float(), class_preds.float()], dim=1)

            keep_boxes = []
            while detections.size(0) != 0:
                overlaps = bbox_iou(detections[0:1, :4], detections[:, :4]) # [1, ...]
                overlaps_mask = overlaps > self.nms_thres

                same_cat_mask = detections[0, -1] == detections[:, -1]
                invalid_mask = overlaps_mask & same_cat_mask
                # https://github.com/eriklindernoren/PyTorch-YOLOv3/blob/master/utils/utils.py
                # Merge overlapping bboxes by order of confidence
                # weights = detections[invalid_mask, 4:5]
                # detections[0, :4] = (weights * detections[invalid_mask, :4]).sum(0) / weights.sum()
                # or we only keep the first bounding box
                keep_boxes += [detections[0]]
                detections = detections[~invalid_mask]
            
            if len(keep_boxes) > 0:
                bboxes[idx] = torch.stack(keep_boxes)
        
        ctx['outputs'] = bboxes

        return None # run this silently