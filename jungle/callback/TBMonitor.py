import torch
from tensorboardX import SummaryWriter

from .CallbackBase import CallbackBase

class ModelGraphMonitor(CallbackBase):
    def __init__(self, tag, batch, epoch, **kwargs):
        super(ModelGraphMonitor, self).__init__(tag=tag, batch=batch, epoch=epoch)
        self.writer = kwargs['writer']
    
    def on_initializing(self, ctx):
        if ctx['inputs'] is None:
            return 'Writing graph needs inputs!'
        inputs = ctx['inputs'].cuda()
        self.writer.add_graph(ctx['model'].module, inputs, True)
        return None

class ParamMonitor(CallbackBase):
    def __init__(self, tag, batch, epoch, writer:SummaryWriter):
        super(ParamMonitor, self).__init__(tag=tag, batch=batch, epoch=epoch)
        self.writer= writer 
    
    def _on_epoch_end(self, ctx):
        for name, param in ctx['model'].named_parameters():
            self.writer.add_histogram(name, param.detach().cpu().numpy(), ctx['epoch'])
        
        return None

class ParamGradMonitor(CallbackBase):
    def __init__(self, tag, writer:SummaryWriter, activate_interval=10):
        super(ParamGradMonitor, self).__init__(tag=tag)
        self.writer = self.writer
        self.activate_interval = 10
        self.activate = False

    def on_initializing(self, ctx):
        self.on_epoch_end(0, ctx)
        return None

    def on_batch_end(self, n_batch, ctx):
        if self.activate:
            pass
        return None

    # this monitor overrides on_epoch_end
    def on_epoch_end(self, n_epoch, ctx):
        if (n_epoch + 1) % self.activate_interval == 0:
            self.activate = True
        else:
            self.activate = False

        return None