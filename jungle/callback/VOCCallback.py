import cv2
import torch
import numpy as np
from PIL.Image import Image

from .CallbackBase import CallbackBase
from .LetterBox import LetterBox

# background
VOC_CLASSES = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair', 
                'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa',
                'train', 'tvmonitor']
VOC_CLASSES_DICT = {x:y for x, y in zip(VOC_CLASSES, range(len(VOC_CLASSES)))}

def _voc_extract_bbox(annotation):
    box = []
    cls_ = []
    if isinstance(annotation['annotation']['object'], list):
        for obj in annotation['annotation']['object']:
            box.append([int(obj['bndbox'][k]) for k in ['xmin', 'ymin', 'xmax', 'ymax']])
            cls_.append(VOC_CLASSES_DICT[obj['name']])
    elif isinstance(annotation['annotation']['object'], dict):
        obj = annotation['annotation']['object']
        box.append([int(obj['bndbox'][k]) for k in ['xmin', 'ymin', 'xmax', 'ymax']])
        cls_.append(VOC_CLASSES_DICT[obj['name']])
    else:
        raise ValueError('Cannot extract bbox information from annotation')

    box = np.array(box, dtype=np.float)
    box[:, 2:4] -= box[:, 0:2]
    box[:, 0:2] += box[:, 2:4] / 2
    cls_ = np.array(cls_, dtype=np.float).reshape((-1, 1))

    label = np.concatenate([cls_, box], axis=-1)

    return label

class VOCCallback(LetterBox):
    def __init__(self, feas_dim=416, pad_value=0.):
        super(VOCCallback, self).__init__('VOC DataProcessing', feas_dim, pad_value, label_transform=_voc_extract_bbox)