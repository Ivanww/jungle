from .CallbackBase import CallbackBase

class _WeightClipper(object):
    def __init__(self, min, max, adj=0.):
        if min is not None:
            self.min = min + adj
        else:
            self.min = None
        if max is not None:
            self.max = max - adj
        else:
            self.max = None

    def __call__(self, m):
        if hasattr(m, 'weight'):
            w = m.weight.data
            if self.min is not None:
                w.masked_fill_(w < self.min, 0)
            if self.max is not None:
                w.masked_fill_(w > self.max, 1)
            # w.clamp_(self.min, self.max)

class WeightClipper(CallbackBase):
    def __init__(self, freq=1, min=0., max=1., adj=0.):
        super(WeightClipper, self).__init__('WeightCliper', freq)
        self.clipper = _WeightClipper(min, max, adj)
        self.on = False
    # def on_epoch_end(self, n_epoch, ctx):
    #     if not self.train:
    #         return None

    #     if (n_epoch != 0 and n_epoch % self.n_epoch == 0):
    #         model = ctx['model']
    #         model.apply(self.clipper)
    #         return 'Weight Clipped to [{}, {}]'.format(self.clipper.min, self.clipper.max)
            
    #     return None
    def _on_epoch_end(self, ctx):
        self.on = ctx['epoch'] > 10

    def _on_batch_end(self, ctx):
        if not self.train:
            return None
        
        if not self.on:
            return None
        n_batch = ctx['progress'][0]
        if (n_batch != 0 and n_batch % self.interval_batch== 0):
            model = ctx['model']
            model.apply(self.clipper) # inplace clipping
            # return 'Weight Clipped to [{}, {}]'.format(self.clipper.min, self.clipper.max)
            
        return None