from .CallbackBase import CallbackBase
# from .COCOCallback import COCOCallback
# from .VOCCallback import VOCCallback
from .CutMixData import CutMixData
from .GradCam import GradientCam

from .CBStateDictWritter import CBStateDictWritter
from .CBMetricManager import CBMetricManager
from .CBLossMeter import CBLossMeter
from .CBPredWritter import CBPredWritter
from .CBAnchorHandler import CBAnchorHandler
from .CBImageWrapper import CBImageWrapper
from .CBTensorBridge import CBTensorBridge
# from .CBObjectSampler import CBObjectSampler

from .CBAnchorHandler import RCNNAnchorHandler
from .CBImageWrapper import RCNNPreProcess
from .CBImageWrapper import RCNNPostProcess
from .CBYOLOWrapper import YOLOPreProcess
from .CBYOLOWrapper import YOLOPostProcess

callback_alias = {
    'loss': CBLossMeter,
    'state_dict': CBStateDictWritter,
    'result_recorder': CBPredWritter,
    # 'tb_graph': ModelGraphMonitor,
    # 'tb_param': ParamMonitor,
    # 'coco_callback': COCOCallback,
    # 'voc_callback': VOCCallback,
    # 'letter_box': LetterBox,
    'metric_manager': CBMetricManager,
    # Complex Metric
    # 'AP_callback': APCallback,
    # 'mAP_callback': MeanAPCallback,
    'cutmix': CutMixData,
    'grad_cam': GradientCam,
    'image_wrapper': CBImageWrapper,
    'anchor_handler': CBAnchorHandler
}

def build_callback(name, **config):
    if name == 'metric':
        tag = config['tag']
        if tag in callback_alias.keys():
            return callback_alias[tag](**config)
        else:
            return callback_alias[name](**config)
    if name in callback_alias.keys():
        return callback_alias[name](**config)
    else:
        raise ValueError('Unknown Callback {}'.format(name))


