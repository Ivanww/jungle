from .torch_cifar10 import load_cifar_torch
from .torch_cifar100 import load_cifar100_torch
from .torch_coco import load_coco_torch
from .torch_imagenet import load_imagenet_torch
from .torch_mnist import load_mnist_torch
from .torch_voc2012 import load_voc2012_torch

dataset_alias = {
    'mnist': load_mnist_torch,
    'cifar10': load_cifar_torch,
    'cifar100': load_cifar100_torch,
    'image_net': load_imagenet_torch,
    'coco': load_coco_torch,
    'voc': load_voc2012_torch
}

def build_dataset(dataset, datadir, batch_size=16, num_worker=4, **kwargs):
    try:
        fn = dataset_alias[dataset]
    except KeyError:
        raise ValueError('Unknown Dataset {}'.format(dataset))

    return fn(datadir, batch_size, num_worker, **kwargs)