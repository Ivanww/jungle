import torch
import torchvision

# for mean and std variance, refer here
# https://github.com/kuangliu/pytorch-cifar/issues/19
def load_cifar_torch(datadir, batch_size=128, num_workers=4):
    train_data = torchvision.datasets.CIFAR10(datadir, 
        transform=torchvision.transforms.Compose([
            torchvision.transforms.RandomCrop(32, padding=4),
            torchvision.transforms.RandomHorizontalFlip(),
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize((0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261))
        ]), download=True)
    
    val_data = torchvision.datasets.CIFAR10(datadir, train=False,
        transform=torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize((0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261))
        ]), download=True)

    train_loader = torch.utils.data.DataLoader(train_data,
        batch_size=batch_size, shuffle=True, num_workers=num_workers, pin_memory=True)
    test_loader = torch.utils.data.DataLoader(val_data, 
        batch_size=batch_size, shuffle=False, num_workers=num_workers, pin_memory=True)

    return train_loader, test_loader