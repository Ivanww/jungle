import torch
import torchvision

# for mean and std variance, refer here
# https://gist.github.com/weiaicunzai/e623931921efefd4c331622c344d8151
def load_cifar100_torch(datadir, batch_size=128, num_workers=4):
    train_data = torchvision.datasets.CIFAR100(datadir, 
        transform=torchvision.transforms.Compose([
            torchvision.transforms.RandomCrop(32, padding=4),
            torchvision.transforms.RandomHorizontalFlip(),
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761))
        ]), download=True)
    
    val_data = torchvision.datasets.CIFAR100(datadir, train=False,
        transform=torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761))
        ]), download=True)

    train_loader = torch.utils.data.DataLoader(train_data,
        batch_size=batch_size, shuffle=True, num_workers=num_workers, pin_memory=True)
    test_loader = torch.utils.data.DataLoader(val_data, 
        batch_size=batch_size, shuffle=False, num_workers=num_workers, pin_memory=True)

    return train_loader, test_loader