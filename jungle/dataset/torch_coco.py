import torch
import torchvision
import numpy as np
import os
from PIL import Image

# https://tech.amikelive.com/node-718/what-object-categories-labels-are-in-coco-dataset/
COCO_NAMES_2017 = ['person', 'bicycle', 'car', 'motorcycle', 'aeroplane', 'bus', 'train', 'truck',
    'boat', 'traffic light', 'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
    'cat', 'dog', 'horse', 'sheep', 'cow','elephant','bear','zebra','giraffe',
    'backpack','umbrella','handbag','tie','suitcase','frisbee','skis','snowboard','sports ball',
    'kite','baseball bat','baseball glove','skateboard','surfboard','tennis racket','bottle','wine glass','cup','fork','knife','spoon',
    'bowl','banana','apple','sandwich','orange','broccoli','carrot','hot dog','pizza','donut','cake',
    'chair','sofa','pottedplant','bed','diningtable','toilet','tvmonitor','laptop','mouse','remote','keyboard','cell phone',
    'microwave','oven','toaster','sink','refrigerator','book','clock','vase','scissors','teddy bear','hair drier','toothbrush'
]

COCO_CATE_2017 = [None,
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10, None, 11, 12, 13, 14, 15, 16, 17, 18,
    19, 20, 21, 22, 23, None, 24, 25, None, None,
    26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
    36, 37, 38, 39, None, 40, 41, 42, 43, 44,
    45, 46, 47, 48, 49, 50, 51, 52, 53, 54,
    55, 56, 57, 58, 59, None, 60, None, None, 61,
    None, 62, 63, 64, 65, 66, 67, 68, 69, 70,
    71, 72, None, 73, 74, 75, 76, 77, 78, 79, None]

def coco_collect_fn(batch):
    images = [b[0] for b in batch]
    target = [b[1] for b in batch]
    return images, target


def load_coco_torch(datadir, batch_size = 1, num_workers=1):
    base = os.path.abspath(datadir)
    train_image_dir = os.path.join(base, 'train2017')
    train_anno_file = os.path.join(base, 'annotations', 'instances_train2017.json')
    val_image_dir = os.path.join(base, 'val2017')
    val_anno_file = os.path.join(base, 'annotations', 'instances_val2017.json')

    train_data = torchvision.datasets.CocoDetection(train_image_dir, train_anno_file)
    val_data = torchvision.datasets.CocoDetection(val_image_dir, val_anno_file)

    train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=False,
        num_workers=num_workers, collate_fn=coco_collect_fn
    )
    val_loader = torch.utils.data.DataLoader(val_data, batch_size=batch_size, shuffle=False,
        num_workers=num_workers, collate_fn=coco_collect_fn
    )

    return train_loader, val_loader