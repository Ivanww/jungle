import torch
import torchvision

def load_imagenet_torch(datadir, batch_size=128, num_workers=4):
    train_data = torchvision.datasets.ImageNet(datadir, download=False,
        transform=torchvision.transforms.Compose([
            torchvision.transforms.RandomSizedCrop(224),
            torchvision.transforms.RandomHorizontalFlip(),
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]))
    val_data = torchvision.datasets.ImageNet(datadir, split='val' , download=False,
        transform=torchvision.transforms.Compose([
            torchvision.transforms.Scale(256),
            torchvision.transforms.CenterCrop(224),
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]))
    train_loader = torch.utils.data.DataLoader(train_data,
        batch_size=batch_size, shuffle=True, num_workers=num_workers, pin_memory=True)
    test_loader = torch.utils.data.DataLoader(val_data,
        batch_size=batch_size, shuffle=False, num_workers=num_workers, pin_memory=True)

    return train_loader, test_loader
