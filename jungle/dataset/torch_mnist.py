import torch
import torchvision

from torch.utils.data import DataLoader
from torchvision.transforms import transforms

def load_mnist_torch(datadir='./data', batch_size=128, num_workers=1):
    train_data = torchvision.datasets.MNIST(datadir, train=True, download=True, 
        transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ]))
    test_data = torchvision.datasets.MNIST(datadir, train=False, download=True,
        transform=torchvision.transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ]))

    train_loader = DataLoader(train_data, batch_size=batch_size, num_workers=num_workers)
    test_loader = DataLoader(test_data, batch_size=batch_size, num_workers=num_workers)

    return train_loader, test_loader