import torch
import torchvision
import numpy as np

# def letterbox_image(pixels, dim):
#     img_w, img_h = pixels.shape[1], pixels.shape[0]
#     h, w = dim
#     new_w = int(img_w * min(w/img_w, h/img_h))
#     new_h = int(img_h * min(w/img_w, h/img_h))

#     resized = skimage.transform.resize(pixels, (new_h, new_w), order=3)

#     canvas = np.full((dim[0], dim[1], 3), 0.5)
#     canvas[(h-new_h)//2:(h-new_h)//2+new_h, (w-new_w)//2:(w-new_w)//2+new_w, :] = resized

#     return canvas

def voc_collect_fn(batch):
    tensors = [b[0] for b in batch]
    annotations = [b[1] for b in batch]
    return tensors, annotations

def load_voc2012_torch(datadir, batch_size=8, num_workers=1):
    voc2012_train = torchvision.datasets.VOCDetection(datadir, 
        image_set='train', download=False
    )
    voc2012_val = torchvision.datasets.VOCDetection(datadir, 
        image_set='val', download=False
    )

    train_loader = torch.utils.data.DataLoader(voc2012_train, batch_size=batch_size, 
        num_workers=num_workers, collate_fn=voc_collect_fn)
    val_loader = torch.utils.data.DataLoader(voc2012_val, batch_size=batch_size, 
        num_workers=num_workers, collate_fn=voc_collect_fn)

    return train_loader, val_loader

if __name__ == '__main__':
    train, _ = load_voc2012_torch('/home/ivan/workspace/data/VOC2012', batch_size=16)
    for img, labels in train:
        print(len(img))
        print(img[0].size)
        for l in labels:
            print(l)
        break
