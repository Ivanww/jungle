import torch
import torch.nn as nn
import os, pickle, datetime
import yaml
import copy
import numpy as np

from tqdm import tqdm
from collections import OrderedDict

from .. import model
from .EngineBase import EngineBase
from ..dataset import build_dataset
from ..callback import build_callback

class CoTeachEngine(EngineBase):
    co_teach_loss = {
        'cross_entropy': torch.nn.functional.cross_entropy
    }
    def __init__(self):
        super(CoTeachEngine, self).__init__()

        self.memo.update({
            'fr': 0.,
            'converge_epoch': None,
            'loss_weights': None
        })
        self.train, self.val = None, None
        self.start_epoch = 0
        self.best_loss = None
        self.best_val_loss = None

        self.criterion_k = self.memo['criterion']
        self.model_b = None

        # self.fr = 0. # Forget Ratio

    def set_model(self, model):
        if isinstance(model, list):
            # self.model, self.model_b = model[:2]
            self.model = model[:2]
            if not isinstance(self.model[0], nn.Module):
                raise TypeError('model must be an instance of nn.Module')
            if not isinstance(self.model[1], nn.Module):
                raise TypeError('model must be an instance of nn.Module')
        else:
            if isinstance(model, nn.Module):
                self.model = [model, copy.deepcopy(self.model)]
            else:
                raise TypeError('model must be an instance of nn.Module')
    def set_optimizer(self, optims):
        if isinstance(optims, list):
            self.optimizer = optims[:2]
            if not isinstance(self.optimizer[0], torch.optim.Optimizer):
                raise TypeError('optimizer must be torch.optim.Optimizer')
            if not isinstance(self.optimizer[1], torch.optim.Optimizer):
                raise TypeError('optimizer must be torch.optim.Optimizer')
        else:
            raise TypeError('use list of optimizer to initialize coteach engine')

    def set_dataset(self, data):
        super(CoTeachEngine, self).set_dataset(data)
        self.train = self.data[0]
        if len(self.data) > 1:
            self.val = self.data[1]
        return self

    def set_lr_scheduler(self, lr_sch, lr_sch_strategy='epoch'):
        if isinstance(lr_sch, torch.optim.lr_scheduler._LRScheduler):
            self.lr_scheduler = lr_sch
        else:
            raise TypeError('lr_scheduler must be one of torch lr_schedulers')
        self.memo['optim_lr_scheduler_strategy'] = lr_sch_strategy
        return self

    def _init_workspace(self):
        self.log.info('Initializing workspace...')
        if self.memo['workspace'] is None:
            raise ValueError('Workspace not Set')
        else:
            ws_root = self.memo['workspace']
            if not os.path.isdir(ws_root):
                os.mkdir(ws_root)
            self.memo['timestamp'] = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
            os.mkdir(os.path.join(ws_root, self.memo['timestamp']))

            # backup configfile
            with open(os.path.join(ws_root, self.memo['timestamp'], 'sketch.yml'), 'w') as f:
                yaml.dump(self.memo, f, default_flow_style=False)

        return self

    def initialize(self):
        self.is_initialized = True
        self._init_workspace()
        self._init_callback_pipeline()
        self._on_initializing()

        return self

    def run(self, n_epoch, start_epoch=0):
        if not self.is_initialized:
            raise RuntimeError('Please Initialize Engine before Run It')

        self.n_epoch = n_epoch

        for i in tqdm(range(start_epoch, n_epoch), disable=None):
            self.train_model(i)
            self.validate_model(i)
        return self._on_terminating()

    def train_model(self, epoch):
        self.model[0].train()
        self.model[1].train()
        for m in self.callbacks:
            self.callbacks[m].eval(train=True)

        tqdm.write('Training Epoch {}/{}'.format(epoch, self.n_epoch))
        ctx = self._on_refreshing_ctx(epoch=epoch)
        self._on_epoch_begin(ctx)
        for i, (features, targets) in enumerate(self.train):
            ctx = self._on_refreshing_ctx(ctx, batch=i, features=features,
                targets=targets)
            self._on_batch_begin(ctx)

            features = ctx['features']
            targets = ctx['targets']

            # selecting samples
            forward_a = self.model[0](features)
            forward_b = self.model[1](features)

            loss_a, loss_b = self.loss((forward_a, forward_b), targets, self.memo)
            # loss_no_grad_a = self.loss(forward_a.detach(), targets, None, 'none')
            # loss_no_grad_b = self.loss(forward_b.detach(), targets, None, 'none')
            # ind_a = torch.argsort(loss_no_grad_a)
            # ind_b = torch.argsort(loss_no_grad_b)

            # num_clean = int((1-self.memo['fr']) * features.shape[0])
            # ind_a_clean = ind_a[:num_clean]
            # ind_b_clean = ind_b[:num_clean]

            # # re-compute loss
            # loss_a = self.loss(forward_a[ind_b_clean], targets[ind_b_clean], self.memo['loss_weights'], 'mean')
            # loss_b = self.loss(forward_b[ind_a_clean], targets[ind_a_clean], self.memo['loss_weights'], 'mean')

            self.optimizer[0].zero_grad()
            self.optimizer[1].zero_grad()

            loss_a.backward()
            loss_b.backward()

            self.optimizer[0].step()
            self.optimizer[1].step()

            ctx = self._on_refreshing_ctx(ctx,
                progress = (i, len(self.train)),
                loss = [loss_a.detach().clone(), loss_b.detach().clone()],
                outputs = [forward_a, forward_b])

            self._on_batch_end(ctx)

        ctx = self._on_refreshing_ctx(epoch=epoch)
        self._on_epoch_end(ctx)


    def validate_model(self, epoch):
        self.model[0].eval()
        self.model[1].eval()
        for m in self.callbacks:
            self.callbacks[m].eval()

        tqdm.write('Validating Epoch {}/{}'.format(epoch, self.n_epoch))
        ctx = self._on_refreshing_ctx(epoch=epoch)
        self._on_epoch_begin(ctx)
        if self.val is not None:
            with torch.no_grad():
                for i, (features, targets) in enumerate(self.val):
                    ctx = self._on_refreshing_ctx(ctx, batch=i, features=features, targets=targets)
                    self._on_batch_begin(ctx)

                    labels = ctx['targets']
                    features = ctx['features']

                    outputs_a = self.model[0](features)
                    outputs_b = self.model[1](features)

                    loss_a, loss_b = self.loss((outputs_a, outputs_b), labels, self.memo)
                    # loss_a = self.loss(outputs_a, labels, None, 'mean')
                    # loss_b = self.loss(outputs_b, labels, None, 'mean')

                    # When validation, do not report anything
                    ctx = self._on_refreshing_ctx(ctx, progress=(i, len(self.val)),
                        loss=[loss_a.clone(), loss_b.clone()], outputs=[outputs_a, outputs_b])
                    self._on_batch_end(ctx)

        ctx = self._on_refreshing_ctx(epoch=epoch)
        self._on_epoch_end(ctx)
