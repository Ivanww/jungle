import torch
import torch.nn as nn
import numpy as np
from torch.utils.data import DataLoader
from tqdm import tqdm
from tabulate import tabulate
from collections import OrderedDict
import logging, sys

from .. import model
from ..callback import build_callback
from ..losses import RPNLoss, FasterRCNNCotrainLoss, FasterRCNNLoss
from ..callback import CallbackBase
# from ..losses import YOLOloss, MixUpLoss

def compose_running_log(headers, values):
    runing_log = []
    for header, value in zip(headers, values):
        if value is not None:
            if isinstance(value, dict):
                runing_log.extend([(k, value[k]) for k in value])
            else:
                runing_log.append((header, value))
    return runing_log


class EngineBase(object):
    def __init__(self):
        self.memo = {
            'callbacks': [],

            'workspace': None,
            'timestamp': None,

            # TODO: No interface
            'use_cuda': True,
            'use_DP': True,
            'use_DDP': False,
            'criterion': 'metric_loss'
        }

        self.model = None
        self.optimizer = None
        self.loss = None
        self.data = None

        self.callbacks = OrderedDict()

        self.is_built = False
        self.is_initialized = False

        self.log = logging.getLogger('Engine')
        self.log.handlers = []
        self.log.setLevel(logging.DEBUG)
        console = logging.StreamHandler(sys.stdout)
        console.setLevel(logging.DEBUG)
        console.setFormatter(logging.Formatter(
            '[%(asctime)s][%(name)s][%(levelname)s]%(message)s'))
        self.log.addHandler(console)

    def update_memo(self, config:dict):
        # TODO: control keys
        self.memo.update(config)

    def set_workspace(self, wsroot, timestamp=None):
        self.memo['workspace'] = wsroot
        self.memo['timestamp'] = timestamp

    def set_model(self, model):
        if isinstance(model, nn.Module):
            self.model = model
        else:
            raise TypeError('model must be an instance of nn.Module')

        return self

    def set_dataset(self, dataset):
        if isinstance(dataset, DataLoader):
            self.data = [dataset]
        elif isinstance(dataset, list):
            self.data = dataset[:2]
        else:
            raise TypeError('data must be an Dataloader or [Dataloader...]')

        return self

    def set_optimizer(self, optim):
        if isinstance(optim, torch.optim.Optimizer):
            self.optimizer = optim
        else:
            raise TypeError('optimizer must be torch.optim.Optimizer')

        return self

    def set_loss(self, loss_fn):
        if callable(loss_fn):
            self.loss = loss_fn
        else:
            raise TypeError('loss function must be callable')

        return self

    def add_callback(self, cb, priority=5):
        if isinstance(cb, CallbackBase):
            self.memo['callbacks'].append((priority, cb))

        return self

    def _init_callback_pipeline(self):
        # build each callback, sort them by priority, add them to ordered dict
        callbacks = sorted(self.memo['callbacks'], key=lambda x: x[0])
        self.log.info('Callback List [{}]'.format([c[1].tag for c in callbacks]))
        for _, cb in callbacks:
            self.callbacks[cb.tag] = cb

        return self

    # Callback functions
    # All return values of monitors  will be sent to log file (or terminal)

    def _on_initializing(self):
        headers = [self.callbacks[k].tag for k in self.callbacks.keys()]
        values = [m.on_initializing(self) for _, m in self.callbacks.items()]
        running_log = compose_running_log(headers, values)
        if len(running_log) > 0:
            tqdm.write(tabulate([[m[1] for m in running_log]], headers=[m[0] for m in running_log]))

    def _on_refreshing_ctx(self, ctx={}, **kwargs):
        ctx.update(kwargs)
        return ctx

    def _on_batch_begin(self, ctx):
        headers = [self.callbacks[k].tag for k in self.callbacks.keys()]
        values = [m.on_batch_begin(self, ctx) for _, m in self.callbacks.items()]
        running_log = compose_running_log(headers, values)
        if len(running_log) > 0:
            tqdm.write(tabulate([[m[1] for m in running_log]], headers=[m[0] for m in running_log]))

    def _on_epoch_begin(self, ctx):
        headers = [self.callbacks[k].tag for k in self.callbacks.keys()]
        values = [m.on_epoch_begin(self, ctx) for _, m in self.callbacks.items()]
        running_log = compose_running_log(headers, values)
        if len(running_log) > 0:
            tqdm.write(tabulate([[m[1] for m in running_log]], headers=[m[0] for m in running_log]))

    def _on_batch_end(self, ctx):
        headers = [self.callbacks[k].tag for k in self.callbacks.keys()]
        values = [m.on_batch_end(self, ctx) for _, m in self.callbacks.items()]
        running_log = compose_running_log(headers, values)
        if len(running_log) > 0:
            running_log.insert(0, ('Epoch', '{}'.format(ctx['epoch'])))
            running_log.insert(0, ('Batch', '{}/{}'.format(*ctx['progress'])))
            tqdm.write(tabulate([[m[1] for m in running_log]], headers=[m[0] for m in running_log]))

    def _on_epoch_end(self, ctx):
        headers = [self.callbacks[k].tag for k in self.callbacks.keys()]
        values = [m.on_epoch_end(self, ctx) for _, m in self.callbacks.items()]
        running_log = compose_running_log(headers, values)
        if len(running_log) > 0:
            running_log.insert(0, ('Epoch', '{}'.format(ctx['epoch'])))
            running_log.insert(0, ('Batch', '{}/{}'.format(*ctx['progress'])))
            tqdm.write(tabulate([[m[1] for m in running_log]], headers=[m[0] for m in running_log]))

    def _on_terminating(self):
        headers = [self.callbacks[k].tag for k in self.callbacks.keys()]
        values = [m.on_terminating() for _, m in self.callbacks.items()]
        running_log = compose_running_log(headers, values)
        if len(running_log) > 0:
            tqdm.write(tabulate([[m[1] for m in running_log]], headers=[m[0] for m in running_log]))

    def initialize(self):
        raise NotImplementedError('Initialize Method Not Implemented')

    def run(self, start_epoch=0, n_epoch=1):
        raise NotImplementedError('Run Method Not Implemented')
