import yaml

from .TrainEngine import TrainingEngine
from .TestEngine import TestEngine
from .ResumeEngine import ResumeEngine
from .ValidatingEngine import ValidatingEngine

class EngineFactory(object):
    def __init__(self):
        pass
    
    @staticmethod
    def create_from_file(config_file):
        with open(config_file, 'r') as f:
            config = yaml.safe_load(f)
        return EngineFactory.create_from_config(config)

    @staticmethod
    def create_from_config(config):
        # model configure
        task = config.get('task', None)
        if task is None:
            raise ValueError('Non Task Found')
        elif task == 'train':
            eng = TrainingEngine()
        elif task == 'test':
            eng = TestEngine()
        elif task == 'resume':
            eng = ResumeEngine()
        elif task == 'validate':
            eng = ValidatingEngine()
        else:
            raise ValueError('Unknown Task {}'.format(task))
        
        eng.set_workspace(
            config.get('workspace', None),
            config.get('timestamp', None)
        )

        eng.set_model(
            config.get('model_arch', None),
            **(config.get('model_config', {}))
        )

        eng.set_dataset(
            config.get('dataset_name', None),
            **(config.get('dataset_config', {}))
        )

        eng.set_loss(
            config.get('loss', None),
            **(config.get('loss_config', {}))
        )

        eng.set_optimizer(
            config.get('optim_name', None),
            **(config.get('optim_config', {}))
        )

        if task == 'train' or task == 'resume':
            eng.set_lr_scheduler(
                config.get('optim_lr_scheduler_name', None),
                config.get('optim_lr_scheduler_strategy', 'epoch'),
                **(config.get('optim_lr_scheduler_config', {}))
            )
        
        if task == 'resume':
            eng.update_sketch({'resume_ckpt': config['resume_ckpt']})

        callbacks = config.get('callbacks', [])
        for c in callbacks:
            eng.add_callback(c['name'], c.get('priority', 5), c.get('config', {}))
        
        for misc in ['use_cuda', 'use_DP', 'use_DDP', 'criterion', 'keep_all', 'ckpt']:
            if misc in config:
                eng.update_sketch({misc: config[misc]})
            
        return eng