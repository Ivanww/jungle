import torch
import os, datetime

from .TrainEngine import TrainingEngine

class ResumeEngine(TrainingEngine):
    def __init__(self):
        super(ResumeEngine, self).__init__()

        self.sketch.update({

            # TODO: No interface
            'resume_ckpt': None,
            'ckpt': None,
        })

        self.checkpoint = None
        self.resume_time = None

    # def _init_workspace(self):
    #     self.resume_time = self.sketch['timestamp']
    #     self.sketch['resume_from'] = self.resume_time

    #     super(ResumeEngine, self)._init_workspace()

    #     return self

    def _init_training(self):
        if self.checkpoint is not None:
            self.start_epoch = self.checkpoint['epoch']+1

        return self

    def _build_model(self):
        # This call is from EngineBase
        super(TrainingEngine, self)._build_model()
        self.log.info('Resume engine loading check points...')

        resume_time = self.sketch.get('timestamp', None)
        if resume_time is None:
            self.log.info('No resume time stamp found in config')
            base = self.sketch['workspace']
        else:
            self.sketch['resume_from'] = resume_time
            base = os.path.join(self.sketch['workspace'], self.resume_time)
        
        if self.sketch['resume_ckpt'] is None:
            raise ValueError('Cannot found resume checkpoint in config')
        else:
            ckpt = os.path.join(base, self.sketch['resume_ckpt'])

        try:
            self.checkpoint = torch.load(ckpt)
        except:
            self.log.warn('Cannot Load Checkpoint {}'.format(ckpt))
            self.log.warn('Loading weights should be handled by model itself')
            self.checkpoint = None

        if self.checkpoint is None:
            self.model.load_weights(ckpt)
        else:
            self.model.load_state_dict(self.checkpoint['state_dict'])

        if self.sketch['use_cuda']:
            if self.sketch['use_DP']:
                self.model = torch.nn.DataParallel(self.model).cuda()
            elif self.sketch['use_DDP']:
                raise NotImplementedError('')
            else:
                self.model = self.model.cuda()

        return self

    def _build_optimizer(self):
        super(ResumeEngine, self)._build_optimizer()

        if self.checkpoint is None:
            self.log.warn('Cannot find previous optimizer env...skipped')
        else:
            self.optimizer.load_state_dict(self.checkpoint['optimizer'])

        return self

    def initialize(self):
        super(ResumeEngine, self).initialize()

        return self._init_training()
