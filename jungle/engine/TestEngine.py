import torch
from tqdm import tqdm
from tabulate import tabulate
from collections import OrderedDict

from .EngineBase import EngineBase


class TestEngine(EngineBase):
    def __init__(self):
        super(TestEngine, self).__init__()
        self.memo.update({

            # TODO: No interface
            'OTG': False,
            'ckpt': None,
            'prediction': 'result.pkl'
        })
        self.checkpoint = None

        # TODO: Support TTA
        # self.tta_num = 1

    # TODO: support TTA
    # def set_tta(self, tta_num):
    #     self.tta_num = tta_num

    def set_dataset(self, data):
        super(TestEngine, self).set_dataset(data)
        self.test = self.data[0]

    def initialize(self):
        self.is_initialized = True
        self._init_callback_pipeline()
        self._on_initializing()
        return self

    def run(self):
        self.model.eval()
        for m in self.callbacks:
            self.callbacks[m].eval(None)

        ctx = self._on_refreshing_ctx(epoch=1)
        self._on_epoch_begin(ctx)
        # Discard this for Gradient CAM
        # with torch.no_grad():
        for i, (features, targets) in tqdm(enumerate(self.test), total=len(self.test), disable=None):
            ctx = self._on_refreshing_ctx(ctx, batch=i, features=features, targets=targets)
            self._on_batch_begin(ctx)

            features = ctx['features']

            outputs = self.model(features)
            # TODO: TTA support
            # outputs = outputs.view(outputs.size(0)//self.tta_num, self.tta_num, -1).mean(dim=1)

            ctx = self._on_refreshing_ctx(ctx, progress=(i, len(self.test)),
                outputs=outputs)

            self._on_batch_end(ctx)

        ctx = self._on_refreshing_ctx(epoch=1)
        self._on_epoch_end(ctx)

        self._on_terminating()
