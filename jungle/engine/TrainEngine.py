import torch
import os, pickle, datetime
import yaml

from tqdm import tqdm
from tensorboardX import SummaryWriter

from .EngineBase import EngineBase
from ..dataset import build_dataset

def PolynomialLR(opt, init_lr, power, max_iter):
    def lr(epoch):
        return init_lr * pow(1-epoch/max_iter, power)

    return torch.optim.lr_scheduler.LambdaLR(opt, lr)

class TrainingEngine(EngineBase):
    def __init__(self):
        super(TrainingEngine, self).__init__()

        self.memo.update({
            'optim_lr_scheduler_strategy': 'epoch',

            # TODO: NO interface
            'keep_all': False
        })

        self.train, self.val = None, None
        self.lr_scheduler = None
        self.start_epoch = 0

        self.criterion_k = self.memo['criterion']

    def update_memo(self, config):
        super(TrainingEngine, self).update_memo(config)
        self.criterion_k = self.memo['criterion']

        return self

    def set_dataset(self, data):
        super(TrainingEngine, self).set_dataset(data)
        self.train = self.data[0]
        if len(self.data) > 1:
            self.val = self.data[1]
        return self

    def set_lr_scheduler(self, lr_sch, lr_sch_strategy='epoch'):
        if isinstance(lr_sch, torch.optim.lr_scheduler._LRScheduler):
            self.lr_scheduler = lr_sch
        else:
            raise TypeError('lr_scheduler must be one of torch lr_schedulers')
        self.memo['optim_lr_scheduler_strategy'] = lr_sch_strategy
        return self

    def _init_workspace(self):
        self.log.info('Initializing workspace...')
        if self.memo['workspace'] is None:
            raise ValueError('Workspace not Set')
        else:
            ws_root = self.memo['workspace']
            if not os.path.isdir(ws_root):
                os.mkdir(ws_root)
            self.memo['timestamp'] = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
            os.mkdir(os.path.join(ws_root, self.memo['timestamp']))

            # backup configfile
            with open(os.path.join(ws_root, self.memo['timestamp'], 'memo.yml'), 'w') as f:
                yaml.dump(self.memo, f, default_flow_style=False)

        return self

    def initialize(self):
        self.is_initialized = True
        self._init_workspace()
        self._init_callback_pipeline()
        self._on_initializing()

        return self

    def run(self, n_epoch, start_epoch=0):
        if not self.is_initialized:
            raise RuntimeError('Please Initialize Engine before Run It')

        self.n_epoch = n_epoch
        for i in tqdm(range(start_epoch, n_epoch), disable=None):
            self.train_model(i)
            self.validate_model(i)
            if self.lr_scheduler is not None:
                if self.memo['optim_lr_scheduler_strategy']== 'batch':
                    self.lr_scheduler.step()
                    # FIXME: for plateau scheduler
        return self._on_terminating()

    def train_model(self, epoch):
        self.model.train()
        for m in self.callbacks:
            self.callbacks[m].eval(train=True)

        tqdm.write('Training Epoch {}/{}'.format(epoch, self.n_epoch))
        ctx = self._on_refreshing_ctx(epoch=epoch)
        self._on_epoch_begin(ctx)
        for i, (features, targets) in enumerate(self.train):
            ctx = self._on_refreshing_ctx(ctx, batch=i, features=features, targets=targets)
            self._on_batch_begin(ctx)

            # callbacks may change context
            features = ctx['features']
            targets = ctx['targets']

            # features.requires_grad_(True)
            self.optimizer.zero_grad()

            outputs = self.model(features)

            loss = self.loss(outputs, targets)
            # if len(ctx['extra_bundle']) == 0:
            #     loss = self.loss(outputs, targets)
            # else:
            #     loss = self.loss(outputs, targets, **ctx['extra_bundle'])

            loss.backward()
            self.optimizer.step()

            if self.lr_scheduler is not None:
                if self.memo['optim_lr_scheduler_strategy'] == 'batch':
                    self.lr_scheduler.step()

            ctx = self._on_refreshing_ctx(ctx,
                progress = (i, len(self.train)),
                loss = loss.detach(),
                outputs = outputs
            )
            self._on_batch_end(ctx)

        # new context, because all training data are gone
        ctx = self._on_refreshing_ctx(epoch=epoch)
        self._on_epoch_end(ctx)

    def validate_model(self, epoch):
        self.model.eval()
        for m in self.callbacks:
            self.callbacks[m].eval()

        tqdm.write('Validating Epoch {}/{}'.format(epoch, self.n_epoch))
        ctx = self._on_refreshing_ctx(epoch=epoch)
        self._on_epoch_begin(ctx)
        if self.val is not None:
            with torch.no_grad():
                for i, (features, targets) in enumerate(self.val):
                    ctx = self._on_refreshing_ctx(ctx, batch=i, features=features, targets=targets)
                    self._on_batch_begin(ctx)
                    labels = ctx['targets']
                    features = ctx['features']

                    outputs = self.model(features)
                    loss = self.loss(outputs, labels)
                    ctx = self._on_refreshing_ctx(ctx, progress=(i, len(self.val)),
                        loss=loss, outputs=outputs)
                    self._on_batch_end(ctx)

        ctx = self._on_refreshing_ctx(epoch=epoch)
        self._on_epoch_end(ctx)
