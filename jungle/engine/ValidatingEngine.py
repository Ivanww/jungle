import torch
from tqdm import tqdm
from tabulate import tabulate
from collections import OrderedDict

from .EngineBase import EngineBase

class ValidatingEngine(EngineBase):
    def __init__(self):
        super(ValidatingEngine, self).__init__()
        self.memo.update({

            # TODO: No interface
            'OTG': False,
            'ckpt': None,
            'predition': 'result.pkl'
        })
        self.val = None

        self.best_loss = None
        self.best_val_loss = None
    def set_dataset(self, data):
        super(ValidatingEngine, self).set_dataset(data)
        self.val = self.data[0]
        return self

    def initialize(self):
        self.is_initialized = True
        self._init_callback_pipeline()
        self._on_initializing()
        return self

    def run(self):
        self.model.eval()
        for m in self.callbacks:
            self.callbacks[m].eval()

        ctx = self._on_refreshing_ctx(epoch=1)
        self._on_epoch_begin(ctx)
        with torch.no_grad():
            for i, (features, targets) in tqdm(enumerate(self.val), total=len(self.val), disable=None):
                ctx = self._on_refreshing_ctx(ctx, features=features, targets=targets)
                self._on_batch_begin(ctx)

                features = ctx['features']
                targets = ctx['targets']

                outputs = self.model(features)
                # loss = self.loss(outputs, targets)
                ctx = self._on_refreshing_ctx(ctx, progress=(i, len(self.val)),
                    # loss=loss.detach(),
                    outputs=outputs)
                self._on_batch_end(ctx)

        ctx = self._on_refreshing_ctx(epoch=1)
        self._on_epoch_end(ctx)
        self._on_terminating()
        return None
