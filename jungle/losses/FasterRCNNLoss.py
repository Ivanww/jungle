import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision

# def smooth_l1_loss(input, target, beta: float = 1. / 9, size_average: bool = True):
#     n = torch.abs(input - target)
#     cond = n < beta
#     loss = torch.where(cond, 0.5 * n ** 2 / beta, n - 0.5 * beta)
#     if size_average:
#         return loss.mean()
#     return loss.sum()

def bbox_iou(box1, box2):
    # use XYXY fmt
    b1_x1, b1_y1, b1_x2, b1_y2 = box1[..., 0], box1[..., 1], box1[..., 2], box1[..., 3]
    b2_x1, b2_y1, b2_x2, b2_y2 = box2[..., 0], box2[..., 1], box2[..., 2], box2[..., 3]

    inter_rect_x1 = torch.max(b1_x1, b2_x1)
    inter_rect_y1 = torch.max(b1_y1, b2_y1)
    inter_rect_x2 = torch.min(b1_x2, b2_x2)
    inter_rect_y2 = torch.min(b1_y2, b2_y2)


    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1, min=0) * torch.clamp(
        inter_rect_y2 - inter_rect_y1, min=0
    )

    b1_area = (b1_x2 - b1_x1) * (b1_y2 - b1_y1)
    b2_area = (b2_x2 - b2_x1) * (b2_y2 - b2_y1)

    iou = inter_area / (b1_area + b2_area - inter_area)

    return iou

class ObjectSampler(nn.Module):
    BACKGROUND = -1
    LOWQUALITY = -2
    IGNORED = -3
    def __init__(self, conf_lb, conf_ub, batch_size, pos_ratio):
        super(ObjectSampler, self).__init__()

        self.conf_lb = conf_lb
        self.conf_ub = conf_ub
        self.batch_size = batch_size
        self.pos_rato = pos_ratio

    def forward(self, objects, anchors, labels=None):
        if labels is None:
            labels = torch.ones((objects.size(0),), device=objects.device).long().view(-1)
        # [O, 1, 4] * [1, A, 4] A is the total number of anchors of each scale
        ious = bbox_iou(objects.unsqueeze(1), anchors.unsqueeze(0))
        # [O, A]
        max_obj_iou, matched_obj_idx = ious.max(dim=0)

        # infomation of each anchor, but need an indicator to interpret
        matched_obj_labels = labels[matched_obj_idx] # matched_obj_labels is now [A, ]
        matched_obj_box = objects[matched_obj_idx, :] # [A, 4]

        matched_obj_labels[max_obj_iou < self.conf_lb] = ObjectSampler.BACKGROUND
        if self.conf_lb != self.conf_ub:
            matched_obj_labels[(max_obj_iou > self.conf_lb) & (max_obj_iou < self.conf_ub)] = ObjectSampler.LOWQUALITY

            # handle low quality match
            # assign object labels to the anchor with maximum iou
            max_anchor_iou, _ = ious.max(dim=1)
            for i, iou in enumerate(max_anchor_iou.view(-1)):
                max_anchor_iou_mask = torch.nonzero(ious[i, :] == iou, as_tuple=True)[0]
                matched_obj_labels[max_anchor_iou_mask] = labels[i]
            matched_obj_labels[matched_obj_labels==ObjectSampler.LOWQUALITY] = ObjectSampler.IGNORED
        pos = torch.nonzero(matched_obj_labels >= 0, as_tuple=True)[0]
        neg = torch.nonzero(matched_obj_labels == ObjectSampler.BACKGROUND, as_tuple=True)[0]
        num_pos = min(int(self.batch_size*self.pos_rato), pos.numel())
        num_neg = min(neg.numel(), self.batch_size - num_pos)

        pos_idx = torch.randperm(pos.numel(), device=objects.device)[:num_pos]
        neg_idx = torch.randperm(neg.numel(), device=objects.device)[:num_neg]

        obj_indicator = torch.full_like(matched_obj_labels, ObjectSampler.IGNORED).long()
        obj_indicator[pos[pos_idx]] = matched_obj_labels[pos[pos_idx]]
        obj_indicator[neg[neg_idx]] = ObjectSampler.BACKGROUND
        return matched_obj_box, obj_indicator


class BBoxRegressionLoss(nn.Module):
    def __init__(self, coord_weights = [1, 1, 1, 1], background = -1, smooth=True):
        super(BBoxRegressionLoss, self).__init__()
        self.background = background
        # TODO: add box coord weights
        coord_weights = torch.tensor(coord_weights)
        self.register_buffer('coord_weights', coord_weights)
        if smooth:
            self.loss_fn = F.smooth_l1_loss
        else:
            self.loss_fn = F.l1_loss


    def encode_box(self, boxes, anchors):
        box_w = boxes[:, 2] - boxes[:, 0]
        box_h = boxes[:, 3] - boxes[:, 1]
        box_cx = boxes[:, 0] + 0.5 * box_w
        box_cy = boxes[:, 1] + 0.5 * box_h

        anchor_w = anchors[:, 2] - anchors[:, 0]
        anchor_h = anchors[:, 3] - anchors[:, 1]
        anchor_cx = anchors[:, 0] + 0.5 * anchor_w
        anchor_cy = anchors[:, 1] + 0.5 * anchor_h

        dx = self.coord_weights[0] * (box_cx - anchor_cx) / anchor_w
        dy = self.coord_weights[1] * (box_cy - anchor_cy) / anchor_h
        dw = self.coord_weights[2] * torch.log(box_w / anchor_w)
        dh = self.coord_weights[3] * torch.log(box_h / anchor_h)

        encoded = torch.stack((dx, dy, dw, dh), dim=1)
        return encoded

    def forward(self, deltas, boxes, ref, labels):
        # encode boxes with ref
        if isinstance(ref, list):
            # use different ref boxes for each delta
            assert len(boxes) == len(ref)
            t_delta = torch.cat([self.encode_box(b, a) for b, a in zip(boxes, ref)], dim=0)
        else:
            # use the same ref box for all deltas
            t_delta = torch.cat([self.encode_box(b, ref) for b in boxes], dim=0)
        # case 1: background & foreground indicator
        # case 2: class labels
        t_mask = torch.nonzero(labels > self.background, as_tuple=True)[0]

        if deltas.size(1) != 4:
            pred_boxes = deltas.view(labels.size(0), -1, 4)
            # need extra class label to select correct pred box
            pred_boxes = pred_boxes[t_mask, labels[t_mask], :]
        else:
            pred_boxes = deltas.view(-1, 4)[t_mask]

        t_delta = t_delta.view(-1, 4)[t_mask]

        numel = (labels >= self.background).sum()
        reg_loss = self.loss_fn(pred_boxes, t_delta, reduction='sum')

        return reg_loss/numel

class ObjectConfidenceLoss(nn.Module):
    def __init__(self, background=-1):
        super(ObjectConfidenceLoss, self).__init__()
        self.background = background

    def forward(self, x, indicator):
        # create conf target
        t_conf = torch.zeros_like(indicator, device=x.device).float()
        # foreground
        t_conf[torch.nonzero(indicator > self.background, as_tuple=False)] = 1.
        # valid position
        conf_mask = torch.nonzero(indicator >= self.background, as_tuple=False)

        t_conf = t_conf[conf_mask]
        pred_conf = x.view(-1,)[conf_mask]
        conf_loss = F.binary_cross_entropy_with_logits(pred_conf, t_conf)

        return conf_loss

class ClassProbLoss(nn.Module):
    def __init__(self, background=-1):
        super(ClassProbLoss, self).__init__()
        self.background = background
        self.loss_fn = nn.CrossEntropyLoss()

    def forward(self, x, labels):
        labels = labels.view(-1)
        t_labels = labels.clone()
        # assign 0 as the label of BG
        t_labels[labels == self.background] = 0
        mask = torch.where(t_labels >= 0)[0]
        t_labels = t_labels[mask]
        pred_labels = x[mask, :]
        return self.loss_fn(pred_labels, t_labels.view(-1))

class RPNLoss(nn.Module):
    def __init__(self, conf_lb=0.3, conf_ub=0.7, num_prop=256, pos_ratio=0.5):
        super(RPNLoss, self).__init__()

        self.box_reg_loss = BBoxRegressionLoss(smooth=False)
        self.sampler = ObjectSampler(conf_lb, conf_ub, num_prop, pos_ratio)
        self.conf_loss = ObjectConfidenceLoss(self.sampler.BACKGROUND)

    def forward(self, x, targets):
        cls_tensor, reg_tensor = x

        cls_tensor = cls_tensor.view(-1)
        reg_tensor = reg_tensor.view(-1, 4)

        anchors = torch.cat(targets['anchors'], dim=0)

        obj_boxes = [self.sampler(b, anchors) for b in targets['boxes']]

        batch_labels = torch.cat([b[1] for b in obj_boxes], dim=0)
        conf_loss = self.conf_loss(cls_tensor, batch_labels)

        batch_boxes = [b[0] for b in obj_boxes]
        reg_loss = self.box_reg_loss(reg_tensor, batch_boxes, anchors, batch_labels)

        return reg_loss,  conf_loss

class FasterRCNNLoss(nn.Module):
    def __init__(self, coord_weights=[1, 1, 1, 1], conf_lb=0.5, conf_ub=0.5, num_prop=512, pos_ratio=0.25):
        super(FasterRCNNLoss, self).__init__()

        self.sampler = ObjectSampler(conf_lb, conf_ub, num_prop, pos_ratio)
        self.class_loss = ClassProbLoss(background=self.sampler.BACKGROUND)
        self.box_reg_loss = BBoxRegressionLoss(coord_weights=coord_weights, background=self.sampler.BACKGROUND, smooth=True)

    def forward(self, x, targets):
        proposals, cls_logits, bbox_reg = x[:3]
        # sample proposals according to the IOU to ground truth objects
        obj_boxes = [self.sampler(b, p, l)
            for b, p, l in zip(targets['boxes'], proposals, targets['labels'])]

        batch_boxes = [b[0] for b in obj_boxes] # sampled proposals for each images
        batch_labels = torch.cat([b[1] for b in obj_boxes], dim=0) # class labels assigned to each proposal
        cls_loss = self.class_loss(cls_logits, batch_labels)
        # == checked ==
        box_loss = self.box_reg_loss(bbox_reg, batch_boxes, proposals, batch_labels)

        return cls_loss,  box_loss

class FasterRCNNCotrainLoss(nn.Module):
    def __init__(self, rpn_loss_cfg={}, head_loss_cfg={}):
        super(FasterRCNNCotrainLoss, self).__init__()
        self.rpn_loss = RPNLoss(**rpn_loss_cfg)
        self.head_loss = FasterRCNNLoss(**head_loss_cfg)

    def forward(self, x, targets):
        roi_loss = self.head_loss(x[:3], targets)
        roi_loss = roi_loss[0] + roi_loss[1]
        if len(x) > 3:
            rpn_loss = self.rpn_loss(x[3:], targets)
            rpn_loss = rpn_loss[0] + rpn_loss[1]
            return rpn_loss + roi_loss
        else:
            return roi_loss
        # return self.rpn_loss(x[0], targets) + self.head_loss(x[1], targets)
