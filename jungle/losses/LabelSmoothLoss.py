import torch
import torch.nn as nn
from torch.nn.modules.loss import _WeightedLoss, _Loss

def smooth_one_hot(labels, num_class, smoothing=0.0):
    confidence = 1.0 - smoothing
    label_shape = torch.Size((labels.size(0), num_class))
    with torch.no_grad():
        label_dist = torch.empty(size=label_shape, device=labels.device)
        label_dist.fill_(smoothing / (num_class - 1)) # background distribution
        label_dist.scatter_(1, labels.data.unsqueeze(1), confidence)
    return label_dist

class LabelSmoothLoss(_WeightedLoss):
    def __init__(self, num_class, smoothing=0.0, weight=None, reduction='mean'):
        super(LabelSmoothLoss, self).__init__()
        self.num_class = num_class
        self.smoothing = smoothing
    
    def forward(self, x, target):
        smooth_label = smooth_one_hot(target, self.num_class, self.smoothing)
        return nn.functional.kl_div(x, smooth_label)