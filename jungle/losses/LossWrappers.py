import torch
from torch.nn.modules.loss import _WeightedLoss, _Loss
import logging, sys

class MixUpLoss(_Loss):
    losses_alias = {
        'cross_entropy': torch.nn.CrossEntropyLoss
    }
    def __init__(self, criterion, weight=None, size_average=None, reduce=None, reduction='mean'):
        super(MixUpLoss, self).__init__()

        self.log = logging.getLogger('loss.MixUpLoss')
        self.log.setLevel(logging.DEBUG)
        console = logging.StreamHandler(sys.stdout)
        console.setLevel(logging.DEBUG)
        console.setFormatter(logging.Formatter(
            '[%(asctime)s][%(name)s][%(levelname)s]%(message)s'))
        self.log.addHandler(console)
        self.log.info('You are using mixup loss wrapper')
        self.log.info('Make sure you have added mixup callback for data preprocessing!')

        if criterion in MixUpLoss.losses_alias:
            self.criterion = MixUpLoss.losses_alias[criterion](weight, 
                size_average, reduce, reduction)
        elif callable(criterion):
            self.log.info('Wrap up a callable object in mixup loss')
            self.criterion = criterion
        else:
            self.log.error('{} loss is not supported for mixing up')
            self.criterion = None

    def forward(self, inputs, targets, lam):
        if self.criterion is None:
            raise ValueError('criterion cannot be none')
        else:
            if lam is None:
                # no mixing performed, use original criterion
                return self.criterion(inputs, targets)
            else:
                return lam * self.criterion(inputs, targets[0]) + (1 - lam) * self.criterion(inputs, targets[1])