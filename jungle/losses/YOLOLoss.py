import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

import logging, sys

# COEFF_COORD = 5.
# COEFF_OBJ = 5.
# COEFF_NOOBJ = 0.5

# YOLOv2_LOSS_CONFIG = {
#     'scaled_anchor': True,
#     'anchors': [(1.3221, 1.73145),
#         (3.19275, 4.00944),
#         (5.05587, 8.09892),
#         (9.47112, 4.84053),
#         (11.2364, 10.0071)],
#     'in_dim': 416,
#     'feas_dim': 13,
#     'coeffs': {
#         'coord': 1.0,
#         'obj': 5.0,
#         'noobj': 0.5,
#         'cls': 1.
#     },
#     'iou_thres': 0.5,
#     'num_class': 80, # 80 for COCO
#     'debug': True
# }

def bbox_ious_wh(boxwh, anchorwh):
    boxes = torch.cat([-boxwh, boxwh], axis=1) / 2
    anchors = torch.cat([-anchorwh, anchorwh], axis=1) / 2

    return bbox_iou(boxes.unsqueeze(0), anchors.unsqueeze(1))

def bbox_iou(box1, box2):
    b1_x1, b1_y1, b1_x2, b1_y2 = box1[..., 0], box1[..., 1], box1[..., 2], box1[..., 3]
    b2_x1, b2_y1, b2_x2, b2_y2 = box2[..., 0], box2[..., 1], box2[..., 2], box2[..., 3]

    inter_rect_x1 = torch.max(b1_x1, b2_x1)
    inter_rect_y1 = torch.max(b1_y1, b2_y1)
    inter_rect_x2 = torch.min(b1_x2, b2_x2)
    inter_rect_y2 = torch.min(b1_y2, b2_y2)

    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1, min=0) * torch.clamp(
        inter_rect_y2 - inter_rect_y1, min=0
    )

    b1_area = (b1_x2 - b1_x1) * (b1_y2 - b1_y1)
    b2_area = (b2_x2 - b2_x1) * (b2_y2 - b2_y1)

    iou = inter_area / (b1_area + b2_area - inter_area)

    return iou

# build target for single branch
class YOLOTargetBuilder(nn.Module):
    def __init__(self, anchors, stride, num_class, low_quality_thres=0.5):
        super(YOLOTargetBuilder, self).__init__()
        self.stride = stride
        scaled_anchors = anchors / self.stride
        self.num_class = num_class
        self.low_quality_thres = low_quality_thres

        self.register_buffer('scaled_anchors', scaled_anchors)
        self.register_buffer('anchors', anchors)

    def _bbox_coord_transform(self, boxes):
        # XYXY -> CXCY
        boxes[:, 2:4] = boxes[:, 2:4] - boxes[:, 0:2]
        boxes[:, 0:2] = boxes[:, 0:2] + boxes[:, 2:4] / 2
        return boxes

    def _inv_box_regression(self, box_delta):
        # box_delta [N, num_anchor, S, S, 4]
        cell_size = box_delta.size(2)
        # do not use inplace computing here!
        box = torch.zeros_like(box_delta)
        box[..., 0:2] = torch.sigmoid(box_delta[..., 0:2])
        box[..., 2:4] = torch.exp(box_delta[..., 2:4])

        offset = torch.arange(cell_size, device=box_delta.device
            ).repeat(cell_size, 1)
        box[..., 0] += offset.view(1, 1, cell_size, cell_size)
        box[..., 1] += offset.t().contiguous().view(1, 1, cell_size, cell_size)
        box[..., 2:4] *= self.scaled_anchors.view(1, -1, 1, 1, 2) # CXCY

        # box[..., 0:2] -= box[..., 2:4] / 2
        # box[..., 2:4] += box[..., 0:2] # XYXY

        box *= self.stride

        return box

    def _assign_boxes(self, boxes, cls_labels, obj_masks, box_tensors, cls_tensors):
        for bid, box in enumerate(boxes):
            # for each image, use no scaled boxes here
            ious = bbox_ious_wh(box[:, 2:4], self.anchors)
            _, best_anchor_idx = ious.max(dim=0)
            labels = cls_labels[bid]

            best_anchors = self.scaled_anchors[best_anchor_idx, :]
            box = box / self.stride
            boxwh = torch.log(box[:, 2:4] / best_anchors)
            boxctr = box[:, 0:2] - torch.floor(box[:, 0:2])
            # assign obj masks
            box_offsets = torch.floor(box[:, 0:2]).long()
            obj_masks[bid, best_anchor_idx, box_offsets[:, 1], box_offsets[:, 0]] = 1
            box_tensors[bid, best_anchor_idx, box_offsets[:, 1], box_offsets[:, 0], :] = torch.cat([boxctr, boxwh], dim=1)
            cls_tensors[bid, best_anchor_idx, box_offsets[:, 1], box_offsets[:, 0], labels.long()] = 1.

        return obj_masks, box_tensors, cls_tensors

    def _init_target(self, pred_deltas):
        batch_size, num_anchor, _, cell_size, _ = pred_deltas.shape
        obj_mask = torch.zeros(batch_size, num_anchor, cell_size, cell_size,
            device=pred_deltas.device)
        box_tensor = torch.zeros(batch_size, num_anchor, cell_size, cell_size,
            4, device=pred_deltas.device)
        cls_tensor = torch.zeros(batch_size, num_anchor, cell_size, cell_size,
            self.num_class, device=pred_deltas.device)

        return obj_mask, box_tensor, cls_tensor

    def _build_neg_mask(self, obj_masks, pred_deltas, boxes):
        noobj_masks = 1-obj_masks

        pred_boxes = self._inv_box_regression(pred_deltas)
        for i, box in enumerate(boxes):
            # for each image
            ious = bbox_iou(pred_boxes[i].unsqueeze(-2), box.view(1, 1, 1, -1, 4))
            max_ious, _ = ious.max(dim=-1)
            if (max_ious > self.low_quality_thres).sum() > 0:
                obj = torch.nonzero(max_ious > self.low_quality_thres, as_tuple=True)
                noobj_masks[i, (*obj)] = 0.

        return noobj_masks

    def forward(self, boxes, labels, pred_box_deltas):
        # boxes = [b / self.stride for b in boxes]
        obj_mask, box_tensor, cls_tensor = self._init_target(pred_box_deltas)
        # t_tensors = [self._init_target(p) for p in pred_box_deltas]
        # obj_masks = [t[0] for t in t_tensors]
        # box_tensors = [t[1] for t in t_tensors]
        # cls_tensors = [t[2] for t in t_tensors]
        # boxes_ctr_fmt = [self._bbox_coord_transform(b) for b in boxes]

        obj_mask, box_tensor, cls_tensor = self._assign_boxes(boxes, labels, obj_mask, box_tensor, cls_tensor)
        noobj_mask = self._build_neg_mask(obj_mask, pred_box_deltas, boxes)

        return box_tensor, cls_tensor, obj_mask, noobj_mask


class YOLOLoss(nn.Module):
    def __init__(self, anchors, strides, num_class, coefficients):
        super(YOLOLoss, self).__init__()

        self.num_anchors = [a.shape[0] for a in anchors]
        self.num_class = num_class
        self.mse_loss = nn.MSELoss(reduction='mean')
        self.bce_loss = nn.BCEWithLogitsLoss(reduction='mean')
        self.ce_loss = nn.CrossEntropyLoss(reduction='mean')

        self.obj_scale = coefficients['obj_scale']
        self.noobj_scale = coefficients['noobj_scale']

        self.t_builders = [YOLOTargetBuilder(a, s, num_class) for a, s in zip(anchors, strides)]

    def flatten_features(self, x, num_anchor):
        batch_size = x.size(0)
        cell_size = x.size(-1)
        feat = x.view(batch_size, num_anchor, -1, cell_size, cell_size)
        feat = feat.permute(0, 1, 3, 4, 2).contiguous()

        box_reg = feat[..., :4]
        box_conf = feat[..., 4]
        box_cls = feat[..., 5:]

        return box_reg, box_conf, box_cls

    def forward(self, x, targets):
        # ASSUME target boxes is CXCY fmt
        flattened = [self.flatten_features(feat, na) for feat, na in zip(x, self.num_anchors)]
        box_reg_detached = [f[0].detach() for f in flattened]

        # return three masks
        t_tensors = [build(targets['boxes'], targets['labels'], box)
            for build, box in zip(self.t_builders, box_reg_detached)]

        t_deltas = [t[0] for t in t_tensors]
        t_cls = [t[1] for t in t_tensors]
        obj_mask = [t[2] for t in t_tensors]
        noobj_mask = [t[3] for t in t_tensors]

        # merge all levels
        valid_box_reg = torch.cat([feat[0][mask==1, :].view(-1, 4) for feat, mask in zip(flattened, obj_mask)], dim=0)
        t_box_reg = torch.cat([lvl_delta[mask==1, :].view(-1, 4) for lvl_delta, mask in zip(t_deltas, obj_mask)], dim=0)
        loss_coord = self.mse_loss(valid_box_reg, t_box_reg)
        print('Coord Loss ', loss_coord)

        valid_box_cls = torch.cat([feat[2][mask==1, :] for feat, mask in zip(flattened, obj_mask)], dim=0)
        t_cls = torch.cat([tc[mask==1, :].view(-1, self.num_class) for tc, mask in zip(t_cls, obj_mask)], dim=0)
        loss_cls = self.bce_loss(valid_box_cls, t_cls)
        print('Class Loss ', loss_cls)

        obj_conf = torch.cat([feat[1][mask==1].view(-1) for feat, mask in zip(flattened, obj_mask)], dim=0)

        t_obj_conf = torch.ones_like(obj_conf)
        loss_obj = self.bce_loss(obj_conf, t_obj_conf)#  * self.obj_scale
        noobj_conf = torch.cat([feat[1][mask==1].view(-1) for feat, mask in zip(flattened, noobj_mask)], dim=0)
        t_noobj_conf = torch.zeros_like(noobj_conf)
        loss_noobj = self.bce_loss(noobj_conf, t_noobj_conf) * self.noobj_scale
        print(f'Obj Conf: ', loss_obj)
        print(f'NoObj Conf: ', loss_noobj)
        loss_conf = loss_obj + loss_noobj
        return loss_coord + loss_cls + loss_conf
