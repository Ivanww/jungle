from .LossWrappers import MixUpLoss
# from .YOLOLoss import YOLOLossV3
from .FasterRCNNLoss import RPNLoss
from .FasterRCNNLoss import FasterRCNNLoss
from .FasterRCNNLoss import FasterRCNNCotrainLoss
from .YOLOLoss import YOLOLoss, YOLOTargetBuilder
from .LabelSmoothLoss import LabelSmoothLoss, smooth_one_hot