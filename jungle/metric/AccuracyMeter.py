import torch
import numpy as np
from .MetricMeter import MetricMeter

class AccuracyMeter(MetricMeter):
    def __init__(self, name='acc', as_criterion=False):
        super(AccuracyMeter, self).__init__(name, as_criterion)

    def initialize(self):
        super(AccuracyMeter, self).initialize()
        self.cached_data = [0., 0] # num_match, num_count

    def accumulate(self, outputs, targets):
        outputs = outputs.detach().cpu().numpy()
        targets = targets.detach().cpu().numpy()
        pred_labels = np.argmax(outputs, axis=-1)
        num_match = (pred_labels == targets).sum()
        num_count = len(pred_labels)
        self.cached_data[0] += num_match
        self.cached_data[1] += num_count

    def evaluate(self):
        if self.cached_data[1] == 0:
            metric = 0.
        else:
            metric = round(self.cached_data[0] / self.cached_data[1], 6)
        return metric

    def clear(self):
        self.cached_data = [0, 0]
