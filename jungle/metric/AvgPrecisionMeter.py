import torch
import numpy as np
from .MetricMeter import MetricMeter

def bbox_iou(box1, box2):
    # use XYXY fmt
    b1_x1, b1_y1, b1_x2, b1_y2 = box1[..., 0], box1[..., 1], box1[..., 2], box1[..., 3]
    b2_x1, b2_y1, b2_x2, b2_y2 = box2[..., 0], box2[..., 1], box2[..., 2], box2[..., 3]

    inter_rect_x1 = torch.max(b1_x1, b2_x1)
    inter_rect_y1 = torch.max(b1_y1, b2_y1)
    inter_rect_x2 = torch.min(b1_x2, b2_x2)
    inter_rect_y2 = torch.min(b1_y2, b2_y2)


    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1, min=0) * torch.clamp(
        inter_rect_y2 - inter_rect_y1, min=0
    )

    b1_area = (b1_x2 - b1_x1) * (b1_y2 - b1_y1)
    b2_area = (b2_x2 - b2_x1) * (b2_y2 - b2_y1)

    iou = inter_area / (b1_area + b2_area - inter_area)

    return iou

class AveragePrecisionMeter(MetricMeter):
    def __init__(self, num_classes, iou_thres=0.5, predict_bg=True, as_criterion=False, name='mAP'):
        super(AveragePrecisionMeter, self).__init__(name, as_criterion)
        self.num_classes = num_classes
        self.predict_bg = predict_bg
        self.iou_thres = iou_thres

    def initialize(self):
        super(AveragePrecisionMeter, self).initialize()
        self.cached_data = {'stats': [], 'obj_count':torch.zeros(self.num_classes)}

    def _compute_stats(self, boxes, labels, t_boxes, t_labels):
        # for each image
        is_TP = torch.zeros_like(labels, dtype=torch.float32)

        for l in torch.unique(t_labels):
            cls_boxes = boxes[labels==l, :]
            if cls_boxes.size(0) == 0:
                continue # no detection for this ground truth class

            t_cls_boxes = t_boxes[t_labels==l, :]
            self.cached_data['obj_count'][int(l)] += t_cls_boxes.size(0)

            # [T, P]
            mask = torch.zeros(cls_boxes.size(0))
            ious = bbox_iou(cls_boxes.unsqueeze(0), t_cls_boxes.unsqueeze(1))
            for i in range(t_cls_boxes.size(0)):
                tp_idx = torch.argmax(ious[i], dim=-1)
                if ious[i, tp_idx] > self.iou_thres and mask[tp_idx] == 0:
                    mask[tp_idx] = 1 # mark this prediction as TP
            is_TP[labels==l] = mask

        return is_TP

    def accumulate(self, outputs, targets):
        # labels, scores, boxes = outputs
        t_boxes = targets['boxes']
        t_labels = targets['labels']

        # compute stat for each image
        for out, tb, tl in zip(outputs, t_boxes, t_labels):
            l, s, b = out
            stats = torch.stack([s.cpu(), l.float().cpu(),
                self._compute_stats(b, l, tb, tl)], dim=-1)
            self.cached_data['stats'].append(stats)

    def _evaluate_map(self):
        stats = torch.cat(self.cached_data['stats'], dim=0)

        # convert to numpy array for stable sort
        sorted_ind = np.argsort(-stats[:, 0].numpy(), kind='stable')
        stats = stats.numpy()[sorted_ind, :]

        ap_cls = torch.zeros(self.num_classes)
        # calculate precision & recall for each class
        for l in range(1 if self.predict_bg else 0, self.num_classes):
            num_objects = self.cached_data['obj_count'][l]
            if num_objects == 0:
                continue

            cls_stats = stats[stats[:, 1] == l]

            num_tp = np.cumsum(cls_stats[:, 2], axis=0)
            num_detection = np.arange(cls_stats.shape[0])+1
            prec = num_tp / num_detection
            rec = num_tp / num_objects

            sorted_rec_ind = np.argsort(rec, kind='stable')
            sorted_rec = rec[sorted_rec_ind]
            sorted_prec = prec[sorted_rec_ind]

            cur_rec = 0.
            auc = 0.
            while cur_rec < sorted_rec.max():
                max_prec_ind = np.argsort(sorted_prec[sorted_rec > cur_rec], kind='stable')[-1]
                max_prec = sorted_prec[sorted_rec > cur_rec][max_prec_ind]
                if max_prec == 0:
                    break
                next_rec = sorted_rec[sorted_rec > cur_rec][max_prec_ind]
                accu = (max_prec * (next_rec - cur_rec))
                auc += accu
                cur_rec = next_rec
            ap_cls[l] = auc

        return ap_cls

    def evaluate(self):
        if len(self.cached_data['stats']) == 0:
            return 0.
        else:
            ap_cls = self._evaluate_map()
            if self.predict_bg:
                ap_cls = ap_cls[1:]
            return ap_cls.mean()

    def clear(self):
        self.cached_data = {'stats': [], 'obj_count':torch.zeros(self.num_classes)}
