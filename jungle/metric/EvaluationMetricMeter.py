import torch
import numpy as np
from sklearn import metrics

from .MetricMeter import MetricMeter

# extremely slow, consider using torch implementation
# this callback is not supposed to be a criterion
class EvaluationMetricMeter(MetricMeter):
    def __init__(self, metric_fns, name='sklearn-metircs'):
        super(EvaluationMetricMeter, self).__init__(name, False) # not for criterion
        self.metric_fns = self._init_metric_fns(metric_fns)

    def _init_metric_fns(self, metric_fns):
        if isinstance(metric_fns, list):
            return {'metric_{}'.format(i): metric_fns[i] for i in range(len(metric_fns))}
        elif isinstance(metric_fns, dict):
            return metric_fns
        elif callable(metric_fns):
            return {'sklearn-metric': metric_fns}

    def initialize(self):
        super(EvaluationMetricMeter, self).initialize()
        self.cached_data = {'pred': [], 't': []}

    def accumulate(self, outputs, targets):
        outputs = outputs.detach().cpu().numpy()
        targets = targets.detach().cpu().numpy()

        pred_labels = np.argmax(outputs, axis=-1)
        self.cached_data['pred'].append(pred_labels)
        self.cached_data['t'].append(targets)

    def evaluate(self):
        if len(self.cached_data['pred']) == 0:
            return 0.
        if len(self.cached_data['t']) == 0:
            return 0.

        pred = np.concatenate(self.cached_data['pred'])
        t = np.concatenate(self.cached_data['t'])

        metrics = [round(v(t, pred), 6) for _, v in self.metric_fns.items()]
        return metrics

    def clear(self):
        self.cached_data['pred'] = []
        self.cached_data['t'] = []
