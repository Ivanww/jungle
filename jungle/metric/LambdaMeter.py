import torch
import numpy as np
import sklearn.metrics as M
from collections.abc import Iterable
from collections import OrderedDict

from .MetricMeter import MetricMeter

def sklearn_metric(func):
    def metric(outputs, targets):
        outputs = outputs.cpu().numpy()
        targets = targets.cpu().numpy()

        return func(targets, outputs)

    return metric

@sklearn_metric
def multi_class_acc(y_true, probas):
    y_pred = np.argmax(probas, axis=1)
    return M.accuracy_score(y_true, y_pred)

@sklearn_metric
def multi_class_recall(y_true, probas):
    y_pred = np.argmax(probas, axis=1)
    return M.recall_score(y_true, y_pred, average=None)

@sklearn_metric
def multi_class_recall_macro(y_true, probas):
    y_pred = np.argmax(probas, axis=1)
    return M.recall_score(y_true, y_pred, average='macro')

@sklearn_metric
def multi_class_recall_micro(y_true, probas):
    y_pred = np.argmax(probas, axis=1)
    return M.recall_score(y_true, y_pred, average='micro')

@sklearn_metric
def multi_class_precision(y_true, probas):
    y_pred = np.argmax(probas, axis=1)
    return M.precision_score(y_true, y_pred, average=None)

@sklearn_metric
def multi_class_precision_macro(y_true, probas):
    y_pred = np.argmax(probas, axis=1)
    return M.precision_score(y_true, y_pred, average='macro')

@sklearn_metric
def multi_class_precision_micro(y_true, probas):
    y_pred = np.argmax(probas, axis=1)
    return M.precision_score(y_true, y_pred, average='micro')

@sklearn_metric
def multi_class_matthews_corrcoef(y_true, probas):
    y_pred = np.argmax(probas, axis=1)
    return M.matthews_corrcoef(y_true, y_pred)


class LambdaMetricMeter(MetricMeter):
    metrics = {
        'multi_class_acc': multi_class_acc,
        'multi_class_recall': multi_class_recall,
        'multi_class_recall_micro': multi_class_recall_micro,
        'multi_class_recall_macro': multi_class_recall_macro,
        'multi_class_precision': multi_class_precision,
        'multi_class_precision_macro': multi_class_precision_macro,
        'multi_class_precision_micro': multi_class_precision_micro,
        'multi_class_mcc': multi_class_matthews_corrcoef
    }
    def __init__(self, metric):
        super(LambdaMetricMeter, self).__init__()

        self.num_batch = 0.
        self.metric_val = 0.
        self.metric_fn = self._build_metric_fn(metric)
    
    def initialize(self, ks):
        super(LambdaMetricMeter, self).initialize(ks)
        for k in self.cached_data:
            self.cached_data[k] = []
    
    def _build_metric_fn(self, fn):
        if isinstance(fn, str):
            if fn in LambdaMetricMeter.metrics:
                self.name = fn
                return LambdaMetricMeter.metrics[fn]
            else:
                raise NotImplementedError('{} is not found in BatchMetricMeter')
        elif isinstance(fn, dict):
            if 'name' in fn and 'func' in fn:
                if isinstance(fn['name'], str) and callable(fn['func']):
                    self.name = fn['name']
                    return fn['func']
                else:
                    raise TypeError('Metric Name or Metric Func Type Error')
            else:
                raise ValueError('Use {\'name\':<name>, \'func\':<callable>} to set your metric in MetricBundle')
        elif isinstance(fn, list):
            if len(fn) < 2:
                raise ValueError('Use [<name>, <callable>] to set your metric in MetricBundle')

            if isinstance(fn[0], str) and callable(fn[1]):
                self.name = fn[0]
                return fn[1]
            else:
                raise TypeError('Metric Name or Metric Func Type Error')
        elif callable(fn):
            return fn
        else:
            raise ValueError('Metric is not accepted by BatchMetricMeter')
        return None

    def accumulate(self, outputs, targets, k=None):
        k = self.default_k if k is None else k
        self.cached_data[k].append(self.metric_fn(outputs, targets))
    
    def evaluate(self):
        # average over batches
        metric = {k:sum(self.cached_data[k]) / len(self.cached_data[k])
            for k in self.available_k}
        return metric
    
    def clear(self):
        super(LambdaMetricMeter, self).clear()
        for k in self.cached_data:
            self.cached_data[k] = []

