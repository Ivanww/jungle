import copy
class MetricMeter(object):
    def __init__(self, name='', as_criterion=False):
        super(MetricMeter, self).__init__()
        self.name = name
        self.cached_data = None
        self.hist = {}
        self.mode = '_train'
        self.as_criterion = as_criterion

    def set_criterion(self):
        self.as_criterion = True

    def is_criterion(self):
        return self.as_criterion

    def initialize(self):
        self.hist.update({self.name+'_train': []})
        self.hist.update({self.name+'_val': []})

    def train(self, train=True):
        if train:
            self.mode = '_train'
        else:
            self.mode = '_val'

    def accumulate(self, outputs, targets, k=None):
        raise NotImplementedError()

    def evaluate(self):
        raise NotImplementedError()

    def update(self):
        metric = self.evaluate()
        self.hist[self.name+self.mode].append(metric)
        return metric

    def clear(self):
        raise NotImplementedError()

    def history(self):
        return copy.deepcopy(self.hist)
