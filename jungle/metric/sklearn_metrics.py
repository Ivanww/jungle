import torch
import numpy as np
import sklearn.metrics as M

# Wrapper of sklearn metrics
# 1. convert torch tensor to numpy array
# 2. calculate metric
def sklearn_metric_wrapper(func):
    def metric(outputs, targets):
        if isinstance(outputs, torch.Tensor):
            outputs_arr = outputs.numpy()
        else:
            outputs_arr = outputs_arr

        if isinstance(targets, torch.Tensor):
            targets_arr = targets.numpy()
        else:
            targets_arr = targets_arr

        return func(targets_arr, outputs_arr)

    return metric

@sklearn_metric_wrapper
def accuracy(targets, outputs):
    if len(outputs.shape) == 1 or outputs.shape[1] > 1:
        pred = np.argmax(outputs, axis=-1)
    else:
        pred = (outputs > 0.5).astype(np.int)
    return M.accuracy_score(targets.reshape((-1, )), pred.reshape((-1,)))

@sklearn_metric_wrapper
def accuracy_top5(targets, outputs):
    pred = np.argsort(outputs, axis=-1)[..., -5:]
    pred = pred.reshape((-1, 5))
    matched = np.equal(pred, targets.reshape((-1, 1)))
    matched = np.sum(matched, axis=-1)
    return matched.sum() / matched.size


@sklearn_metric_wrapper
def macro_precision(targets, outputs):
    if len(outputs.shape) > 1 and outputs.shape[1] > 1:
        labels = list(range(outputs.shape[1]))
        pred = np.argmax(outputs, axis=-1)
    else:
        labels = [0, 1]
        pred = (outputs > 0.5).astype(np.int)
    return M.precision_score(targets.reshape((-1,)),
            pred.reshape((-1,)), labels=labels, average='macro')

@sklearn_metric_wrapper
def micro_precision(targets, outputs):
    if len(outputs.shape) > 1 and outputs.shape[1] > 1:
        labels = list(range(outputs.shape[1]))
        pred = np.argmax(outputs, axis=-1)
    else:
        labels = [0, 1]
        pred = (outputs > 0.5).astype(np.int)
    return M.precision_score(targets.reshape((-1,)),
            pred.reshape((-1,)), labels=labels, average='micro')

@sklearn_metric_wrapper
def macro_recall(targets, outputs):
    if len(outputs.shape) > 1 and outputs.shape[1] > 1:
        labels = list(range(outputs.shape[1]))
        pred = np.argmax(outputs, axis=-1)
    else:
        labels = [0, 1]
        pred = (outputs > 0.5).astype(np.int)
    return M.recall_score(targets.reshape((-1,)),
            pred.reshape((-1,)), labels=labels, average='macro')

@sklearn_metric_wrapper
def micro_recall(targets, outputs):
    if len(outputs.shape) > 1 or outputs.shape[1] > 1:
        labels = list(range(outputs.shape[1]))
        pred = np.argmax(outputs, axis=-1)
    else:
        labels = [0, 1]
        pred = (outputs > 0.5).astype(np.int)
    return M.recall_score(targets.reshape((-1,)),
            pred.reshape((-1,)), labels=labels, average='micro')

@sklearn_metric_wrapper
def log_loss(targets, outputs):
    return M.log_loss(targets.reshape((-1,)), outputs.reshape((-1,)))
