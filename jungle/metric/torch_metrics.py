import torch

# torch metrics
# This module calculate metrics directly on torch tensors

def exact_match_ratio(outputs, targets):
    labels = (outputs > 0.5).float()
    matched = 0.
    for i in range(labels.size(0)):
        if (labels[i, :].equal(targets[i, :])):
            matched += 1.
    return matched / labels.size(0)


def multi_cls_prec(outputs, targets):
    preds = (outputs > 0.5).float()
    TPs = (targets * preds).sum(dim=0) + 1e-10
    FPs = (preds * (1-targets)).sum(dim=0)

    return TPs / (TPs + FPs)

def multi_cls_recall(outputs, targets):
    preds = (outputs > 0.5).float()
    TPs = (targets * preds).sum(dim=0) + 1e-10
    FNs = ((1-preds) * targets).sum(dim=0)

    return TPs / (TPs + FNs)

def multi_binary_cls_acc(outputs, targets):
    preds = (outputs > 0.5).long()
    matched = targets.long().eq(preds).float().sum()

    return matched / preds.numel()

def multi_binary_cls_macro_prec(outputs, targets):
    preds = (outputs > 0.5).float()
    TPs = (targets * preds).sum(dim=0) + 1e-10
    FPs = (preds * (1-targets)).sum(dim=0)

    return (TPs / (TPs + FPs)).mean()

def multi_binary_cls_micro_prec(outputs, targets):
    preds = (outputs > 0.5).float()
    TPs = (targets * preds).sum() + 1e-10
    FPs = (preds * (1-targets)).sum()

    return (TPs / (TPs + FPs)).mean()

def multi_binary_cls_macro_recall(outputs, targets):
    preds = (outputs > 0.5).float()
    TPs = (targets * preds).sum(dim=0) + 1e-10
    FNs = ((1-preds) * targets).sum(dim=0)

    return (TPs / (TPs + FNs)).mean()

def multi_binary_cls_micro_recall(outputs, targets):
    preds = (outputs > 0.5).float()
    TPs = (targets * preds).sum(dim=1) + 1e-10
    FNs = ((1-preds) * targets).sum(dim=1)

    return (TPs / (TPs + FNs)).mean()

def log_metric_with_logits(outputs, targets):
    pred = torch.sigmoid(outputs)
    pred = pred.clamp_(min=1e-15, max=1-1e-15)
    metric = targets * torch.log(pred) + (1 - targets) * torch.log(1-pred)
    return -metric.mean()