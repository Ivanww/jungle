import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision

from torchvision.ops import MultiScaleRoIAlign
from collections import OrderedDict

class ROIHead(nn.Module):
    def __init__(self, in_channels, featmap_names, num_rep=1024, num_class=91):
        super(ROIHead, self).__init__()

        self.roi_pool = MultiScaleRoIAlign(featmap_names, output_size=7, sampling_ratio=2)
        self.box_head = nn.ModuleDict(OrderedDict([
            ('fc6', nn.Linear(in_channels * self.roi_pool.output_size[0] ** 2, num_rep)),
            # ('relu6', nn.ReLU()),
            ('fc7', nn.Linear(num_rep, num_rep)),
            # ('relu7', nn.ReLU())
        ]))
        self.box_predictor = nn.ModuleDict(OrderedDict([
            ('cls_score', nn.Linear(num_rep, num_class)),
            ('bbox_pred', nn.Linear(num_rep, num_class*4))
        ]))

    def forward(self, x, proposals, image_sizes):
        # ROI Pool/ ROI Align Pool
        x = self.roi_pool(x, proposals, image_sizes)

        # box head
        x = x.flatten(1)
        x = F.relu(self.box_head['fc6'](x))
        x = F.relu(self.box_head['fc7'](x))

        # for classification and regression
        cls_logits = self.box_predictor['cls_score'](x)
        bbox_reg = self.box_predictor['bbox_pred'](x)

        return cls_logits, bbox_reg
