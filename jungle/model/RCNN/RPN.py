import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import math
from collections import OrderedDict
# from torchvision.models.detection import RPNHead
from torchvision.models.detection.rpn import RPNHead

# class RPNHead(nn.Module):
#     """
#     Adds a simple RPN Head with classification and regression heads
#     Arguments:
#         in_channels (int): number of channels of the input feature
#         num_anchors (int): number of anchors to be predicted
#     """

#     def __init__(self, in_channels, num_anchors):
#         super(RPNHead, self).__init__()
#         self.conv = nn.Conv2d(
#             in_channels, in_channels, kernel_size=3, stride=1, padding=1
#         )
#         self.cls_logits = nn.Conv2d(in_channels, num_anchors, kernel_size=1, stride=1)
#         self.bbox_pred = nn.Conv2d(
#             in_channels, num_anchors * 4, kernel_size=1, stride=1
#         )

#         for layer in self.children():
#             torch.nn.init.normal_(layer.weight, std=0.01)
#             torch.nn.init.constant_(layer.bias, 0)

#     def forward(self, x):
#         # type: (List[Tensor]) -> Tuple[List[Tensor], List[Tensor]]
#         logits = []
#         bbox_reg = []
#         for feature in x:
#             t = F.relu(self.conv(feature))
#             logits.append(self.cls_logits(t))
#             bbox_reg.append(self.bbox_pred(t))
#         return logits, bbox_reg

class RPN(nn.Module):
    def __init__(self, in_channels, num_anchors, config, cotrain=True):
        super(RPN, self).__init__()
        # TODO: only one scale is supported

        self.head = RPNHead(in_channels, num_anchors)

        # TODO: make sure default parameters
        self._pre_nms_top_n = config.get('pre_nms_top_n', 2000)
        self._post_nms_top_n = config.get('post_nms_top_n', 2000)
        self._pre_nms_top_n_test = config.get('pre_nms_top_n_test', 1000)
        self._post_nms_top_n_test = config.get('post_nms_top_n_test', 1000)
        self.box_min_size = config.get('box_min_size', 1e-3)
        self.nms_thres = config.get('nms_thres', 0.7)


        self.bbox_dwdh_clip = math.log(1000 / 16.)
        self.cotrain = cotrain

    def merge_box_tensors(self, box_cls, box_reg):
        # list[Tensor], list[Tensor] --> Tensor, Tensor
        flatten_cls_tensors = []
        flatten_reg_tensors = []
        for _cls, reg in zip(box_cls, box_reg):
            N, _, H, W = _cls.shape
            # Ax4 = reg.shape[1]
            # A = Ax4 // 4
            # C = AxC // A # assert C == 1 here

            flatten_cls = _cls.view(N, -1, 1, H, W)
            flatten_cls = flatten_cls.permute(0, 3, 4, 1, 2)
            flatten_cls = flatten_cls.reshape(N, -1, 1)
            flatten_cls_tensors.append(flatten_cls)

            flatten_reg = reg.view(N, -1, 4, H, W)
            flatten_reg = flatten_reg.permute(0, 3, 4, 1, 2)
            flatten_reg = flatten_reg.reshape(N, -1, 4)
            flatten_reg_tensors.append(flatten_reg)

        # concat all scale to last dimension
        flatten_cls_tensors = torch.cat(flatten_cls_tensors, dim=1).flatten(1) # [N, num_of_total_anchor]
        flatten_reg_tensors = torch.cat(flatten_reg_tensors, dim=1) # [N, num_of_total_anchor, 4]

        return flatten_cls_tensors, flatten_reg_tensors

    def decode_box(self, box, anchors):
        # XYXY --> CXCY
        widths = anchors[:, 2] - anchors[:, 0]
        heights = anchors[:, 3] - anchors[:, 1]
        ctr_x = anchors[:, 0] + 0.5 * widths
        ctr_y = anchors[:, 1] + 0.5 * heights

        dx = box[:, 0::4]
        dy = box[:, 1::4]
        dw = box[:, 2::4]
        dh = box[:, 3::4]

        # Prevent sending too large values into torch.exp()
        dw = torch.clamp(dw, max=self.bbox_dwdh_clip)
        dh = torch.clamp(dh, max=self.bbox_dwdh_clip)

        # Apply anchors
        pred_ctr_x = dx * widths[:, None] + ctr_x[:, None]
        pred_ctr_y = dy * heights[:, None] + ctr_y[:, None]
        pred_w = torch.exp(dw) * widths[:, None]
        pred_h = torch.exp(dh) * heights[:, None]
        # CXCY --> XYXY
        pred_boxes1 = pred_ctr_x - torch.tensor(0.5, dtype=pred_ctr_x.dtype, device=pred_w.device) * pred_w
        pred_boxes2 = pred_ctr_y - torch.tensor(0.5, dtype=pred_ctr_y.dtype, device=pred_h.device) * pred_h
        pred_boxes3 = pred_ctr_x + torch.tensor(0.5, dtype=pred_ctr_x.dtype, device=pred_w.device) * pred_w
        pred_boxes4 = pred_ctr_y + torch.tensor(0.5, dtype=pred_ctr_y.dtype, device=pred_h.device) * pred_h
        pred_boxes = torch.stack((pred_boxes1, pred_boxes2, pred_boxes3, pred_boxes4), dim=2).flatten(1)

        return pred_boxes

    def filter_proposals(self, proposals, objectness, bound, level_size):
        # fileter proposals for each image
        # _, top_n_idx = objectness.topk(self._pre_nms_top_n)
        top_n_mask = torch.zeros_like(objectness)
        # top_n_mask[top_n_idx] = 1.
        # top_n_idx = [scores.topk(self._pre_nms_top_n, dim=0)[1] for scores in objectness.split(level_size)]

        filtered_boxes = []
        filtered_scores = []
        for boxes, scores, mask in zip(proposals.split(level_size), objectness.split(level_size), top_n_mask.split(level_size)):
            # for each level
            if self.training:
                _, top_n_idx = scores.topk(min(self._pre_nms_top_n, scores.shape[0]))
            else:
                _, top_n_idx = scores.topk(min(self._pre_nms_top_n_test, scores.shape[0]))
            # mask[top_n_idx] = 1.
            top_boxes = boxes[top_n_idx]
            top_scores = scores[top_n_idx]

            clipped = torch.stack([
                top_boxes[..., 0::2].clamp(min=0, max=bound[1]),
                top_boxes[..., 1::2].clamp(min=0, max=bound[0])], dim=-1).view(-1, 4)

            # remove small boxes
            wmask = (clipped[:, 2] - clipped[:, 0]) >= self.box_min_size
            hmask = (clipped[:, 3] - clipped[:, 1]) >= self.box_min_size
            valid_boxes_mask = (wmask & hmask).nonzero().squeeze(1)

            valid_boxes = clipped[valid_boxes_mask]
            valid_scores = top_scores[valid_boxes_mask]

            keep = torchvision.ops.nms(valid_boxes, valid_scores, self.nms_thres)
            filtered_boxes.append(valid_boxes[keep, :])
            filtered_scores.append(valid_scores[keep])

        filtered_boxes = torch.cat(filtered_boxes, dim=0)
        filtered_scores = torch.cat(filtered_scores, dim=0)

        if self.training:
            post_nms_top = min(self._post_nms_top_n, filtered_scores.size(0))
            _, post_top_n_idx = torch.topk(filtered_scores, post_nms_top)
        else:
            post_nms_top = min(self._post_nms_top_n_test, filtered_scores.size(0))
            _, post_top_n_idx = torch.topk(filtered_scores, post_nms_top)
        filtered_boxes = filtered_boxes[post_top_n_idx]
        filtered_scores = filtered_scores[post_top_n_idx]

        return filtered_boxes, filtered_scores

    def forward(self, x, anchors=None, encap_sizes=None):
        if isinstance(x, OrderedDict):
            x = list(x.values())
        # object confidence list, bbox delta list, one for each scale
        objectness, bbox_delta = self.head(x)
        cls_tensor, reg_tensor = self.merge_box_tensors(objectness, bbox_delta)

        # no anchos or targets -> training RPN only
        if anchors is None or encap_sizes is None:
            assert self.training
            return cls_tensor, reg_tensor

        # decode bbox_delta
        num_anchors_per_level = [a.size(0) for a in anchors]
        anchor_tensor = torch.cat(anchors, dim=0)
        detached_reg_tensor = reg_tensor.detach()
        detached_cls_tensor = cls_tensor.detach()
        proposals = [self.decode_box(t, anchor_tensor) for t in detached_reg_tensor]

        final_proposals = [self.filter_proposals(p, o, isize, num_anchors_per_level)
            for p, o, isize in zip(proposals, detached_cls_tensor, encap_sizes)]
        # final_props, final_scores = self.filter_proposals(proposals, cls_tensor, encap_sizes)
        final_prop_boxes = [p[0] for p in final_proposals]
        final_prop_scores = [p[1] for p in final_proposals]

        if self.training:
            if self.cotrain:
                return (cls_tensor, reg_tensor), (final_prop_boxes, final_prop_scores)
            else:
                return final_prop_boxes, final_prop_scores
        else:
            return final_prop_boxes, final_prop_scores

