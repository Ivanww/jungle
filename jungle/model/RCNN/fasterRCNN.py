import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision

from .RPN import RPN
from .ROIHead import ROIHead
# refracor torchvision faster RCNN to adopt this frame work
from torchvision.models.detection.backbone_utils import resnet_fpn_backbone

class FasterRCNN(nn.Module):
    def __init__(self, backbone=None, rpn=None, head=None, backbone_cfg={}, rpn_cfg={}, head_cfg={}, cotrain=True):
        super(FasterRCNN, self).__init__()

        if backbone is None:
            self.backbone = self._build_backbone(**backbone_cfg)
        else:
            self.backbone = backbone

        if rpn is None:
            if 'in_channels' not in rpn_cfg:
                rpn_cfg.update({'in_channels': getattr(self.backbone, 'out_channels', None)})
            if 'num_anchors' not in rpn_cfg:
                rpn_cfg.update({'num_anchors': 3})
            rpn_cfg.update({'cotrain': cotrain})
            self.rpn = self._build_rpn(**rpn_cfg)
        else:
            self.rpn = rpn

        if head is None:
            if 'in_channels' not in head_cfg:
                head_cfg.update({'in_channels': getattr(self.backbone, 'out_channels', None)})
            if 'featmap_names' not in head_cfg:
                head_cfg.update({'featmap_names': ['0', '1', '2', '3']})
            self.roi_heads = self._build_head(**head_cfg)
        else:
            self.roi_heads = head

        self.anchors = None
        self.image_sizes = None
        self.t_boxes = None

        self.cotrain = cotrain
        if not self.cotrain:
            self.rpn.eval()

    def _build_backbone(self, **kwargs):
        return resnet_fpn_backbone('resnet50', False)

    def _build_rpn(self, in_channels, num_anchors, **kwargs):
        return RPN(in_channels, num_anchors, kwargs)

    def _build_head(self, **kwargs):
        return ROIHead(**kwargs)

    def filter_proposals(self, box_delta):
        if self.anchors is None:
            return RuntimeError('filtering proposals requires anchors, but they are None')
        if self.image_sizes is None:
            return RuntimeError('filtering proposals requires image sizes, but they are None')

    def forward(self, x):
        features = self.backbone(x)
        rpn_out = self.rpn(features, self.anchors, self.image_sizes)
        # training: conf_tensor, conf_score_tensor
        # evaluation: proposal boxes, proposal_boxes scores
        # proposals, conf_scores = self.rpn(features, self.anchors, self.image_sizes)
        if self.image_sizes is None:
            # train RPN only
            assert self.training
            return rpn_out
        else:
            # assert self.t_boxes is not None
            # continue with roi head
            if self.training:
                assert self.t_boxes is not None
                if not self.cotrain:
                    # training head only
                    proposals, _ = rpn_out
                    # WHY??
                    proposals = [torch.cat([p, o], dim=0) for p, o in zip(proposals, self.t_boxes)]
                    cls_logits, bbox_reg = self.roi_heads(features, proposals, self.image_sizes)
                    # return only head results
                    return proposals, cls_logits, bbox_reg
                else:
                    # training RPN & head
                    (rpn_cls, rpn_reg), (proposals, _) = rpn_out
                    # WHY??
                    proposals = [torch.cat([p, o], dim=0) for p, o in zip(proposals, self.t_boxes)]
                    cls_logits, bbox_reg = self.roi_heads(features, proposals, self.image_sizes)
                    return proposals, cls_logits, bbox_reg, rpn_cls, rpn_reg
            else:
                # evaluating
                proposals, _ = rpn_out
                cls_logits, bbox_reg = self.roi_heads(features, proposals, self.image_sizes)
                return proposals, cls_logits, bbox_reg

