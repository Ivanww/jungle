import torch
import torch.nn as nn

# from .Darknet import Darknet19
class Darknet19Adapter(nn.Module):
    def __init__(self, model):
        super(Darknet19Adapter, self).__init__()

        self.features = model.features
    
    def forward(self, x):
        out = []
        for i, m in enumerate(self.features):
            x = m(x)
            if i == 16 or i == 22:
                out.append(x)
        return out

class Darknet53Adapter(nn.Module):
    def __init__(self, model):
        super(Darknet53Adapter, self).__init__()
        self.features = model.modulelist
    
    def forward(self, x):
        out = []
        for i, m in enumerate(self.features):
            x = m(x)
            if i in [14, 23, 28]:
                out.append(x)
        return out