import torch
import torch.nn as nn

from collections import OrderedDict

class ResidualBlock(nn.Module):
    def __init__(self, num_channels, activation=True):
        super(ResidualBlock, self).__init__()
        self.conv1x1 = nn.Sequential(OrderedDict([
            ('conv', nn.Conv2d(num_channels, num_channels//2, 1, bias=False)),
            ('batch_norm', nn.BatchNorm2d(num_channels//2)),
            ('leaky', nn.LeakyReLU(0.1, inplace=False))
        ]))
        if activation:
            self.conv3x3 = nn.Sequential(OrderedDict([
                ('conv', nn.Conv2d(num_channels//2, num_channels, 3, padding=1, bias=False)),
                ('batch_norm', nn.BatchNorm2d(num_channels)),
                ('leaky', nn.LeakyReLU(0.1, inplace=False))
            ]))
        else:
            self.conv3x3 = nn.Sequential(OrderedDict([
                ('conv', nn.Conv2d(num_channels//2, num_channels, 3, padding=1, bias=False)),
                ('batch_norm', nn.BatchNorm2d(num_channels))
            ]))

    
    def forward(self, x):
        identity = x

        x = self.conv1x1(x)
        x = self.conv3x3(x)

        x += identity
        return x

class Darknet53(nn.Module):
    def __init__(self, num_classes=1000):
        super(Darknet53, self).__init__()
        self.modulelist = nn.ModuleList()
        self.num_classes = num_classes

        self._build_models()

        self.avg = nn.AdaptiveAvgPool2d((1, 1))
        self.linear = nn.Linear(1024, num_classes)
    
    def forward(self, x):
        for m in self.modulelist:
            x = m(x)
        x = self.avg(x)
        x = self.linear(x.view(x.size(0), -1))
        
        return x
    
    def _build_models(self):
        # 0, 1 conv, 1 conv total
        self.modulelist.append(nn.Sequential(OrderedDict([
            ('conv', nn.Conv2d(3, 32, 3, padding=1, bias=False)), 
            ('batch_norm', nn.BatchNorm2d(32)), 
            ('leaky', nn.LeakyReLU(0.1, inplace=False))])))

        # Downsample 1
        # 1, 1 conv, 2 conv total
        self.modulelist.append(nn.Sequential(OrderedDict([
            ('conv', nn.Conv2d(32, 64, 3, stride=2, padding=1, bias=False)), 
            ('batch_norm', nn.BatchNorm2d(64)), 
            ('leaky', nn.LeakyReLU(0.1, inplace=False))])))

        stage = 1
        # 2, 2 conv, 4 conv total
        for i in range(1):
            self.modulelist.append(ResidualBlock(64))
        
        # Downsample 2
        # 3, 1 conv, 5 conv total
        self.modulelist.append(nn.Sequential(OrderedDict([
            ('conv', nn.Conv2d(64, 128, 3, stride=2, padding=1, bias=False)), 
            ('batch_norm', nn.BatchNorm2d(128)), 
            ('leaky', nn.LeakyReLU(0.1, inplace=False))])))

        stage += 1
        # 4 - 5, 4 conv, 9 conv total
        for i in range(2):
            self.modulelist.append(ResidualBlock(128))

        # Downsample 3
        # 6, 1 conv, 10 conv total
        self.modulelist.append(nn.Sequential(OrderedDict([
            ('conv', nn.Conv2d(128, 256, 3, stride=2, padding=1, bias=False)), 
            ('batch_norm', nn.BatchNorm2d(256)), 
            ('leaky', nn.LeakyReLU(0.1, inplace=False))])))

        stage += 1
        # 7 - 14, 16 conv, 26 conv total
        for i in range(8):
            self.modulelist.append(ResidualBlock(256))

        # Downsample 4 <----
        # 15, 1 conv, 27 conv total
        self.modulelist.append(nn.Sequential(OrderedDict([
            ('conv', nn.Conv2d(256, 512, 3, stride=2, padding=1, bias=False)), 
            ('batch_norm', nn.BatchNorm2d(512)), 
            ('leaky', nn.LeakyReLU(0.1, inplace=False))])))

        stage += 1
        # 16 - 23, 16 conv, 43 conv total
        for i in range(8):
            self.modulelist.append(ResidualBlock(512))

        # Downsample 5 <---
        # 24, 1 conv, 44 conv total
        self.modulelist.append(nn.Sequential(OrderedDict([
            ('conv', nn.Conv2d(512, 1024, 3, stride=2, padding=1, bias=False)), 
            ('batch_norm', nn.BatchNorm2d(1024)), 
            ('leaky', nn.LeakyReLU(0.1, inplace=False))])))

        stage += 1
        # 25 - 28, 8 conv, 52 conv total
        for i in range(4):
            self.modulelist.append(ResidualBlock(1024))
        
class Darknet19(nn.Module):
    def __init__(self, num_classes=1000):
        super(Darknet19, self).__init__()

        self.features = nn.ModuleList()

        self.features.append(self.__build_conv(3, 32)) # 0
        self.features.append(nn.MaxPool2d(2, 2)) # 1
        self.features.append(self.__build_conv(32, 64)) # 2
        self.features.append(nn.MaxPool2d(2, 2)) # 3
        self.features.append(self.__build_conv(64, 128)) # 4
        self.features.append(self.__build_conv(128, 64, kernel_size=1, padding=0)) # 5
        self.features.append(self.__build_conv(64, 128)) # 6
        self.features.append(nn.MaxPool2d(2, 2)) # 7
        self.features.append(self.__build_conv(128, 256)) # 8
        self.features.append(self.__build_conv(256, 128, kernel_size=1, padding=0)) # 9
        self.features.append(self.__build_conv(128, 256)) # 10
        self.features.append(nn.MaxPool2d(2, 2)) # 11
        self.features.append(self.__build_conv(256, 512)) # 12
        self.features.append(self.__build_conv(512, 256, kernel_size=1, padding=0)) # 13
        self.features.append(self.__build_conv(256, 512)) # 14
        self.features.append(self.__build_conv(512, 256, kernel_size=1, padding=0)) # 15
        self.features.append(self.__build_conv(256, 512)) # 16
        self.features.append(nn.MaxPool2d(2, 2)) # 17
        self.features.append(self.__build_conv(512, 1024)) # 18
        self.features.append(self.__build_conv(1024, 512, kernel_size=1, padding=0)) # 19
        self.features.append(self.__build_conv(512, 1024)) # 20
        self.features.append(self.__build_conv(1024, 512, kernel_size=1, padding=0)) # 21
        self.features.append(self.__build_conv(512, 1024)) # 22

        self.classifier = nn.Sequential(*[
            nn.Conv2d(1024, num_classes, 1),
            nn.AdaptiveAvgPool2d((1, 1))
        ])

        self._init_weights()

    def forward(self, x):
        for m in self.features:
            x = m(x)
        
        out = self.classifier(x)
        return out.view(out.size(0), -1)

    def _init_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)

    def __build_conv(self, in_channels, out_channels, kernel_size=3, padding=1):
        return nn.Sequential(*[
            nn.Conv2d(in_channels, out_channels, kernel_size, padding=padding, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.LeakyReLU(0.1, inplace=False)
        ])

if __name__ == "__main__":
    m = Darknet19()
    x = torch.randn(16, 3, 224, 224)
    out = m(x)
    print(out.shape)
    print(m)