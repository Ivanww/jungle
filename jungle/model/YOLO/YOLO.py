import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import logging, sys

from .Darknet import Darknet53, Darknet19, ResidualBlock
from .YOLOLayers import Detector, UpsampleLayer

class YOLOBase(nn.Module):
    def __init__(self, config, num_classes):
        super(YOLOBase, self).__init__()

        self.log = logging.getLogger('model.YOLO')
        self.log.setLevel(logging.DEBUG)
        console = logging.StreamHandler(sys.stdout)
        console.setLevel(logging.DEBUG)
        console.setFormatter(logging.Formatter(
            '[%(asctime)s][%(name)s][%(levelname)s]%(message)s'))
        self.log.addHandler(console)

        self.num_anchors = config.get('num_anchor', 5)
        self.num_classes = num_classes

        self._build_backbone(config['backbone'])
        # self.conf_thres = config['conf_thres']
        # self.nms_thres = config['nms_thres']

        self.use_head = config.get('use_head', True)

    def _build_backbone(self, backbone):
        raise NotImplementedError()

    # This should be the common interface
    def load_weights(self, weights):
        self.load_darknet_weights(weights)

    def _load_block(self, m, weights, ptr):
        if isinstance(m, nn.Sequential):
            # Sequential blocks all have batch norm
            conv = m[0]
            if len(m) > 1:
                bn = m[1]
                num_bn_biases = bn.bias.numel()
                bn_biases = torch.from_numpy(weights[ptr:ptr + num_bn_biases])
                ptr += num_bn_biases

                bn_weights = torch.from_numpy(weights[ptr: ptr + num_bn_biases])
                ptr  += num_bn_biases

                bn_running_mean = torch.from_numpy(weights[ptr: ptr + num_bn_biases])
                ptr  += num_bn_biases

                bn_running_var = torch.from_numpy(weights[ptr: ptr + num_bn_biases])
                ptr  += num_bn_biases

                #Cast the loaded weights into dims of model weights.
                bn_biases = bn_biases.view_as(bn.bias.data)
                bn_weights = bn_weights.view_as(bn.weight.data)
                bn_running_mean = bn_running_mean.view_as(bn.running_mean)
                bn_running_var = bn_running_var.view_as(bn.running_var)

                #Copy the data to model
                bn.bias.data.copy_(bn_biases)
                bn.weight.data.copy_(bn_weights)
                bn.running_mean.copy_(bn_running_mean)
                bn.running_var.copy_(bn_running_var)
            else:
                num_biases = conv.bias.numel()
                conv_biases = torch.from_numpy(weights[ptr: ptr + num_biases])
                ptr = ptr + num_biases
                conv_biases = conv_biases.view_as(conv.bias.data)
                conv.bias.data.copy_(conv_biases)
            #Let us load the weights for the Convolutional layers
            num_weights = conv.weight.numel()

            #Do the same as above for weights
            conv_weights = torch.from_numpy(weights[ptr:ptr+num_weights])
            ptr = ptr + num_weights

            conv_weights = conv_weights.view_as(conv.weight.data)
            conv.weight.data.copy_(conv_weights)
        return ptr

    def load_darknet_weights(self, weights):
        self.log.info('Loading Weights From Darknet Weight File...')
        fp = open(weights, 'rb')
        header = np.fromfile(fp, dtype=np.int32, count=3)
        self.log.info('header: {}'.format(header))
        version = header[0]**100+header[1]*10+header[2]
        if version <= 19:
            seen = np.fromfile(fp, dtype=np.int32, count=1)
        elif version <= 29:
            seen = np.fromfile(fp, dtype=np.int64, count=1)
        else:
            self.log.warn('New weight file syntax! Loading of weights might not work properly.')
            seen = np.fromfile(fp, dtype=np.int64, count=1)
        self.log.info('seen: {}'.format(seen))

        weights = np.fromfile(fp, dtype=np.float32)
        self.log.info('{} weights loaded from files'.format(len(weights)))

        ptr = 0
        backbone = self.backbone
        loaded_layer = 0
        for i, m in enumerate(backbone.features):
            if isinstance(m, ResidualBlock):
                ptr = self._load_block(m.conv1x1, weights, ptr)
                ptr = self._load_block(m.conv3x3, weights, ptr)
                loaded_layer += 2
            else:
                ptr = self._load_block(m, weights, ptr)
                loaded_layer += 1

        self.log.info("Loading weights to backbone net done!")
        self.log.info('{} layers loaded to backbone net'.format(loaded_layer))

        if self.use_head:
            self.log.info('Loading weights to head...')
            for i, m in enumerate(self.header):
                if isinstance(self.header, nn.ModuleDict):
                    m = self.header[m]
                if isinstance(m, Detector):
                    ptr = self._load_block(m.conv_1, weights, ptr)
                    ptr = self._load_block(m.conv_2, weights, ptr)
                elif isinstance(m, UpsampleLayer):
                    ptr = self._load_block(m.conv, weights, ptr)
                else:
                    ptr = self._load_block(m, weights, ptr)
        else:
            self.log.info('Use random weights...')
            for module in self.header:
                if isinstance(module, nn.Sequential):
                    for m in module.modules():
                        if isinstance(m, nn.Conv2d):
                            nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                            if m.bias is not None:
                                nn.init.constant_(m.bias, 0)
                        elif isinstance(m, nn.BatchNorm2d):
                            nn.init.constant_(m.weight, 1)
                            nn.init.constant_(m.bias, 0)
                        elif isinstance(m, nn.Linear):
                            nn.init.normal_(m.weight, 0, 0.01)
                            nn.init.constant_(m.bias, 0)
                        else:
                            pass
                else:
                    if isinstance(m, nn.Conv2d):
                        nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                        if m.bias is not None:
                            nn.init.constant_(m.bias, 0)
                    elif isinstance(m, nn.BatchNorm2d):
                        nn.init.constant_(m.weight, 1)
                        nn.init.constant_(m.bias, 0)
                    elif isinstance(m, nn.Linear):
                        nn.init.normal_(m.weight, 0, 0.01)
                        nn.init.constant_(m.bias, 0)
                    else:
                        pass

        self.log.info("head weight initializing done!")
        self.log.info('{} weights loaded to model'.format(ptr))

    def load_transfered_weights(self, weights):
        self.log.info('Loading Weights From Transfered Model...')
        weights = torch.load(weights)['model']
        weights_ks = list(weights.keys())
        self.log.info('{} items loaded from file.'.format(len(weights)))

        self.log.info('Loading Weights to Backbone...')
        w_cnt = 0
        for name, m in self.named_modules():
            if isinstance(m, nn.Conv2d):
                wk = weights_ks[w_cnt]
                self.log.info('Loading [{}] -> [{}]...'.format(wk, name))
                m.weight.data.copy_(weights[wk])
                w_cnt += 1
                if m.bias is not None:
                    wk = weights_ks[w_cnt]
                    self.log.info('Loading [{}] -> [{}]...'.format(wk, name))
                    m.bias.data.copy_(weights[wk])
                    w_cnt += 1
            elif isinstance(m, nn.BatchNorm2d):
                wk = weights_ks[w_cnt]
                self.log.info('Loading [{}] -> [{}]...'.format(wk, name))
                m.weight.data.copy_(weights[wk])
                w_cnt += 1
                wk = weights_ks[w_cnt]
                self.log.info('Loading [{}] -> [{}]...'.format(wk, name))
                m.bias.data.copy_(weights[wk])
                w_cnt += 1
                wk = weights_ks[w_cnt]
                self.log.info('Loading [{}] -> [{}]...'.format(wk, name))
                m.running_mean.copy_(weights[wk])
                w_cnt += 1
                wk = weights_ks[w_cnt]
                self.log.info('Loading [{}] -> [{}]...'.format(wk, name))
                m.running_var.copy_(weights[wk])
                w_cnt += 2 # skpt num_batches_tracked

        self.log.info('{}/{} items loaded into backbone'.format(w_cnt, len(weights)))
