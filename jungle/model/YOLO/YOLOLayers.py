import torch
import torch.nn as nn
import torch.nn.functional as F
class Detector(nn.Module):
    def __init__(self, num_anchors, in_channels=512, out_channels=1024, in_dim=608, num_classes = 80):
        super(Detector, self).__init__()
        self.in_dim = in_dim
        self.num_classes = num_classes
        self.conv_1 = nn.Sequential(*[
            nn.Conv2d(in_channels, out_channels, 3, padding=1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.LeakyReLU(0.1, inplace=False)
        ])
        self.conv_2 = nn.Sequential(*[
            nn.Conv2d(out_channels, num_anchors*(5+self.num_classes), 1)
        ])


    def forward(self, x):
        x = self.conv_1(x)
        x = self.conv_2(x)
        return x

class UpsampleLayer(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(UpsampleLayer, self).__init__()
        self.conv = nn.Sequential(*[
            nn.Conv2d(in_channels, out_channels, 1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.LeakyReLU(.1, inplace=False)
        ])
        self.up = nn.Upsample(scale_factor=2)

    def forward(self, x):
        x = self.conv(x)
        x = self.up(x)
        # x = F.interpolate(x, scale_factor=2, mode='nearest')
        return x