import torch
import torch.nn as nn

from .YOLO import YOLOBase
from .Darknet import Darknet19
from .BackboneAdapter import Darknet19Adapter

class YOLOv2(YOLOBase):
    def __init__(self, config, num_classes):
        super(YOLOv2, self).__init__(config, num_classes)

        self.out_channels = self.num_anchors * (self.num_classes + 5)
        # YOLO v2 Head
        self.header = nn.ModuleList()
        # transform A
        self.header.append(nn.Sequential(*[
            nn.Conv2d(1024, 1024, 3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(1024),
            nn.LeakyReLU(0.1, inplace=False)]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(1024, 1024, 3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(1024),
            nn.LeakyReLU(0.1, inplace=False)
        ]))

        # transform B
        self.header.append(nn.Sequential(*[
            nn.Conv2d(512, 64, 1, stride=1, padding=0, bias=False),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.1, inplace=False)
        ]))

        # transform C
        self.header.append(nn.Sequential(*[
            nn.Conv2d(1024+256, 1024, 3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(1024),
            nn.LeakyReLU(0.1, inplace=False)]))
        self.header.append(nn.Sequential(*[
            nn.Conv2d(1024, self.out_channels, 1, stride=1),
        ]))

        # if config.get('weights', None) is not None:
        #     self.log.info('Load Pretrained Weights from {}'.format(config['weights']))
        #     self.load_darknet_weight(config['weights'])
        # else:
        #     # TODO: initweights here
        #     self.log.warn('No weight file found! This could be an error!')

    def _build_backbone(self, backbone):
        if callable(backbone):
            self.log.info('Using a callable object as feature extractor')
            self.backbone = backbone
            self.features = self.backbone
            return

        if backbone == 'darknet19':
            self.backbone = Darknet19()
            self.features = Darknet19Adapter(self.backbone)
        else:
            raise ValueError('')

    def forward(self, x):
        fs = self.features(x)
        shortcut, out = fs
        #shortcut, out = self.backbone(x)
        out = self.header[0](out) # 23
        out = self.header[1](out) # 24

        shortcut = self.header[2](shortcut) # 25, 26
        # push W and H to Depth, # 27
        batch_size, num_channel, height, width = shortcut.shape
        shortcut = shortcut.view(batch_size, num_channel, height//2, 2, width//2, 2).transpose(3, 4).contiguous()
        shortcut = shortcut.view(batch_size, num_channel, height//2*width//2, 4).transpose(2, 3).contiguous()
        shortcut = shortcut.view(batch_size, num_channel, 4, height//2, width//2).transpose(1, 2).contiguous()
        shortcut = shortcut.view(batch_size, num_channel*4, height//2, width//2)
        # shortcut = shortcut.view(batch_size, int(num_channel / 4), height, 2, width, 2).contiguous()
        # shortcut = shortcut.permute(0, 3, 5, 1, 2, 4).contiguous()
        # shortcut = shortcut.view(batch_size, -1, int(height / 2), int(width / 2))

        out = torch.cat([shortcut, out], dim=1) #
        out = self.header[3](out)
        out = self.header[4](out)
        return out