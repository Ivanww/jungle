import torch
import torch.nn as nn

# refer to torchvision model zoo
std_cfgs = {
    'A': { 'feature': [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
            'feature_size': (7, 7), 'classifier': [7*7*512, 4096, 4096]},
    'B': { 'feature': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
            'feature_size': (7, 7), 'classifier': [7*7*512, 4096, 4096]},
    'D': { 'feature': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M'],
            'feature_size': (7, 7), 'classifier': [7*7*512, 4096, 4096]},
    'E': { 'feature': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M', 512, 512, 512, 512, 'M'],
            'feature_size': (7, 7), 'classifier': [7*7*512, 4096, 4096]},
}

custom_cfgs = {
    'CIFAR10': { 'feature': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M'],
            'feature_size': (1, 1), 'classifier': [512]}
}
def build_layers(cfgs, batch_norm=False):
    layers = []
    in_channels = 3
    for v in cfgs:
        if v == 'M':
            layers.append(nn.MaxPool2d(2, 2))
        else:
            layers.append(nn.Conv2d(in_channels, v, 3, padding=1))
            if batch_norm:
                layers.append(nn.BatchNorm2d(v))
                # layers.append(nn.ReLU(inplace=True))
                layers.append(nn.LeakyReLU(inplace=True))
            else:
                # layers.append(nn.ReLU(inplace=True))
                layers.append(nn.LeakyReLU(inplace=True))
            in_channels = v

    return nn.Sequential(*layers)

def build_classifier(cfgs, num_classes, dropout=True):
    layers = []
    in_channels = cfgs[0]
    for v in cfgs[1:]:
        layers.append(nn.Linear(in_channels, v))
        if dropout:
            # layers.append(nn.ReLU(v))
            layers.append(nn.LeakyReLU(inplace=True))
            layers.append(nn.Dropout())
        else:
            layers.append(nn.BatchNorm1d(v))
            # layers.append(nn.ReLU())
            layers.append(nn.LeakyReLU(inplace=True))
        in_channels = v
    layers.append(nn.Linear(in_channels, num_classes))
    return nn.Sequential(*layers)

class VGG_TORCH(nn.Module):
    def __init__(self, features_cfgs, feature_size, classifier_cfgs, num_classes=1000):
        super(VGG_TORCH, self).__init__()
        self.features = build_layers(features_cfgs, batch_norm=True)
        self.avg = nn.AdaptiveAvgPool2d(feature_size)
        self.classifier = build_classifier(classifier_cfgs, num_classes)

        self._init_weights()

    def forward(self, x):
        x = self.features(x)
        x = self.avg(x)
        x = self.classifier(x.view(x.size(0), -1))
        return x

    def _init_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)

def VGG19(num_classes=1000):
    return VGG_TORCH(std_cfgs['E']['feature'], std_cfgs['E']['feature_size'], std_cfgs['E']['classifier'], num_classes)

def VGG16(num_classes=1000):
    return VGG_TORCH(std_cfgs['D']['feature'], std_cfgs['D']['feature_size'], std_cfgs['D']['classifier'], num_classes)

def VGG16_S(num_classes=10):
    return VGG_TORCH(custom_cfgs['CIFAR10']['feature'],
        custom_cfgs['CIFAR10']['feature_size'], custom_cfgs['CIFAR10']['classifier'], num_classes)

if __name__ == "__main__":
    vgg = VGG19()
    print(vgg)
    x = torch.randn((1, 3, 224, 224))
    y = vgg(x)
    print(y.size())
