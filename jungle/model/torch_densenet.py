import torch
import torch.nn as nn
import torch.nn.functional as F
from collections import OrderedDict

def build_conv(in_channels, out_channels, kernel=3, strides=1, padding=1, use_bn=True, activation='relu'):
    layers = []

    if use_bn:
        layers.append(nn.BatchNorm2d(in_channels))
    
    if activation is not None:
        if activation == 'relu':
            layers.append(nn.ReLU())
        else:
            layers.append(nn.ReLU())
    layers.append(nn.Conv2d(in_channels, out_channels, kernel, stride=strides, padding=padding, bias=False))

    return nn.Sequential(*layers)

class DenseLayer(nn.Sequential):
    def __init__(self, in_channels, k):
        super(DenseLayer, self).__init__()

        self.add_module('conv1x1', build_conv(in_channels, 4*k, kernel=1, padding=0))
        self.add_module('conv3x3', build_conv(4*k, k))

    def forward(self, *inputs):
        x = torch.cat(inputs, dim=1)
        return self.conv3x3(self.conv1x1(x))

class DenseBlock(nn.Module):
    def __init__(self, in_channels, k, num_convs):
        super(DenseBlock, self).__init__()
        
        for i in range(num_convs):
            self.add_module('conv_{}'.format(i), DenseLayer(in_channels+i*k, k))

    def forward(self, x):
        features = [x]
        for _, layer in self.named_children():
            features.append(layer(*features))
        
        return torch.cat(features, dim=1)

class TransitionLayer(nn.Sequential):
    def __init__(self, in_channels, out_channels):
        super(TransitionLayer, self).__init__()

        self.add_module('conv', build_conv(in_channels, out_channels, kernel=1, padding=0))
        self.add_module('pool', nn.AvgPool2d(kernel_size=2, stride=2))

class DenseNet(nn.Module):
    def __init__(self, k=12, theta=0.5, init_channels=64, block_config=[6, 12, 24, 16], num_classes=1000):
        super(DenseNet, self).__init__()
        self.features = nn.Sequential(OrderedDict([
            ('conv0', nn.Conv2d(3, init_channels, kernel_size=7, stride=2, padding=3, bias=False)),
            ('norm0', nn.BatchNorm2d(init_channels)),
            ('relu', nn.ReLU()),
            ('pool0', nn.MaxPool2d(kernel_size=3, stride=2, padding=1))
        ]))

        num_channels = init_channels
        for i, num_convs in enumerate(block_config):
            self.features.add_module('block_{}'.format(i+1), 
                DenseBlock(num_channels, k, num_convs))
            
            num_channels = num_channels + num_convs * k
            self.features.add_module('transition_{}'.format(i+1),
                TransitionLayer(num_channels, int(num_channels*theta)))

            num_channels = int(num_channels*theta)
        
        self.features.add_module('norm5', nn.BatchNorm2d(num_channels))

        self.classifier = nn.Linear(num_channels, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.constant_(m.bias, 0)

    def forward(self, x):
        features = self.features(x)
        out = F.relu(features, inplace=True)
        out = F.adaptive_avg_pool2d(out, (1, 1))
        out = self.classifier(out.view(out.size(0), -1))

        return out

if __name__ == "__main__":
    model = DenseNet()

    x = torch.randn(1, 3, 224, 224)
    out = model(x)
    print(out.shape)
