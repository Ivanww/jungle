import torch
import torch.nn as nn
import torch.nn.functional as F

from collections import OrderedDict

def build_mobilenet_conv(in_channels, out_channels, k=3, s=1, g=1, linear=False):
    padding = (k-1)//2
    
    if linear:
        return nn.Sequential(*[
            nn.Conv2d(in_channels, out_channels, k, s, padding=padding, groups=g, bias=False),
            nn.BatchNorm2d(out_channels)
        ])
    else:
        return nn.Sequential(*[
            nn.Conv2d(in_channels, out_channels, k, s, padding=padding, groups=g, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU6(inplace=True)
        ])

class InvertedResidual(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1, t=6):
        super(InvertedResidual, self).__init__()
        self.stride = stride
        self.in_channels = in_channels
        self.out_channels = out_channels
        hidden_channels = in_channels * t
        layers = []

        self.conv = nn.Sequential(*[
            build_mobilenet_conv(in_channels, hidden_channels, k=1, s=1, g=1),
            build_mobilenet_conv(hidden_channels, hidden_channels, k=3, s=stride, g=hidden_channels),
            build_mobilenet_conv(hidden_channels, out_channels, k=1, s=1, g=1, linear=True)
        ])
    
    def forward(self, x):
        if self.stride == 1 and self.in_channels == self.out_channels:
            return x + self.conv(x)
        else:
            return self.conv(x)

class MobileNetV2(nn.Module):
    def __init__(self, block_settings=None, num_classes=1000):
        super(MobileNetV2, self).__init__()

        if block_settings is None:
            self.block_settings = [
                [None, 32, 1, 2],
                [1, 16, 1, 1],
                [6, 24, 2, 2],
                [6, 32, 3, 2],
                [6, 64, 4, 2],
                [6, 96, 3, 1],
                [6, 160, 3, 2],
                [6, 320, 1, 1],
                [None, 1280, 1, 1]
            ]
        else:
            self.block_settings = block_settings

        in_channels = 3 
        features = OrderedDict()
        for i, (t, c, n, s) in enumerate(self.block_settings[:-1]):
            if t is None:
                features['conv_{}'.format(i)] = build_mobilenet_conv(in_channels, c, s=s)
                in_channels = c
            else:
                for b in range(n):
                    if b == 0:
                        features['stage_{}_block_{}'.format(i, b)] = InvertedResidual(in_channels, c, s, t)
                    else:
                        features['stage_{}_block_{}'.format(i, b)] = InvertedResidual(in_channels, c, 1, t)
                    in_channels = c
        
        self.features = nn.Sequential(features)

        t, c, n, s = self.block_settings[-1]

        self.conv1x1 = build_mobilenet_conv(in_channels, c, k=1, s=s)
        in_channels = c
        self.pool = nn.AdaptiveAvgPool2d((1, 1))
        self.classifier = nn.Conv2d(in_channels, num_classes, 1)

        # weight initialization
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out')
                if m.bias is not None:
                    nn.init.zeros_(m.bias)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.ones_(m.weight)
                nn.init.zeros_(m.bias)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.zeros_(m.bias)
    
    def forward(self, x):
        x = self.features(x)
        x = self.conv1x1(x)
        x = self.pool(x)
        x = self.classifier(x)
        return x.view(x.size(0), -1)


if __name__ == "__main__":
    model = MobileNetV2()
    x = torch.randn(1, 3, 224, 224)
    out = model(x)
    print(out.shape)