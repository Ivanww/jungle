import torch
import torch.nn as nn

# Two different categories of building block
# 1. Basic Block
class Block(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1):
        super(Block, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, out_channels, 3, stride=stride, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(out_channels, out_channels, 3, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(out_channels)
        if stride != 1:
            self.downsample = nn.Conv2d(in_channels, out_channels, 1, stride=stride, bias=False)
        else:
            self.downsample = None

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            identity = self.downsample(identity)

        out += identity
        out = self.relu(out)

        return out

# 2. Bottleneck Structure
class Bottleneck(nn.Module):
    def __init__(self, in_channels, bottleneck, out_channels, stride=1):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, bottleneck, 1, bias=False)
        self.bn1 = nn.BatchNorm2d(bottleneck)

        self.conv2 = nn.Conv2d(bottleneck, bottleneck, 3, stride=stride, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(bottleneck)

        self.conv3 = nn.Conv2d(bottleneck, out_channels, 1, bias=False)
        self.bn3 = nn.BatchNorm2d(out_channels)

        self.relu = nn.ReLU(inplace=True)

        if stride != 1:
            self.downsample = nn.Conv2d(in_channels, out_channels, 1, stride=stride, bias=False)
        elif in_channels != out_channels:
            self.downsample = nn.Conv2d(in_channels, out_channels, 1, bias=False)
        else:
            self.downsample = None

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            identity = self.downsample(identity)

        out += identity
        out = self.relu(out)

        return out

res18_config = {
    'bottom': [
        # ['conv', in_channels, out_channels, kernel_size, stride]
        ['conv', 3, 64, 7, 2],
        #['pool', 'pool_type', kernel_size, stride]
        ['pool', 'max', 3, 2]
    ],
    'residual': [
        #['basic', repeat, in_channels, out_channels, stride]
        ['basic', 2, 64, 64, 1],
        ['basic', 2, 64, 128, 2],
        ['basic', 2, 128, 256, 2],
        ['basic', 2, 256, 512, 2],
    ],
    'top': [
        ['pool', 'adptavg', 1],
        #['fc', in_features, out_features, bias]
        ['fc', 512, 1000, True]
    ]
}

res50_config = {
    'bottom': [
        ['conv', 3, 64, 7, 2],
        ['pool', 'max', 3, 2]
    ],
    'residual': [
        # ['bottle', repeat, in_channels, intermediate, out_channels, strid]
        ['bottle', 3, 64, 64, 256, 1],
        ['bottle', 4, 256, 128, 512, 2],
        ['bottle', 6, 512, 256, 1024, 2],
        ['bottle', 3, 1024, 512, 2048, 2]
    ],
    'top': [
        ['pool', 'adptavg', 1],
        ['fc', 2048, 1000, True]
    ]
}

class ResnetBase(nn.Module):
    def __init__(self, config:dict, num_classes=None):
        super(ResnetBase, self).__init__()
        self.config = config
        self.num_classes = num_classes

        self.features = nn.Sequential()
        bottom_conf = self.config.get('bottom', None)
        if bottom_conf is not None:
            bottom = nn.Sequential()
            for i, c in enumerate(bottom_conf):
                bottom.add_module('bottom_{}'.format(i+1), self._build_layer(c))
            self.features.add_module('conv_0', bottom)

        residual_conf = self.config['residual']
        for i, c in enumerate(residual_conf):
            self.features.add_module('conv_{}'.format(i+1), self._build_layer(c))

        self.classifier = nn.ModuleList()
        top_conf = self.config['top']
        assert(top_conf[-1][0] == 'fc')
        if self.num_classes is not None:
            top_conf[-1][2] = self.num_classes
        for c in top_conf:
            self.classifier.append( self._build_layer(c))

    def forward(self, x):
        feas = self.features(x)
        for m in self.classifier[:-1]:
            out = m(feas)

        out = self.classifier[-1](out.view(out.size(0), -1))

        return out

    def _build_layer(self, config):
        if config[0] == 'conv':
            _, in_channels, out_channels, kernel, stride = config
            padding = (kernel-1)//2
            return nn.Conv2d(in_channels, out_channels, kernel, stride=stride, padding=padding, bias=False)
        elif config[0] == 'pool':
            p = config[1]
            if p == 'max':
                _, _, k, s = config
                return nn.MaxPool2d(k, s)
            elif p == 'avg':
                _, _, k, s = config
                return nn.AvgPool2d(k, s)
            elif p == 'adptavg':
                _, _, k = config
                return nn.AdaptiveAvgPool2d((k, k))
            elif p == 'adptmax':
                _, _, k = config
                return nn.AdaptiveMaxPool2d((k, k))
            else:
                raise ValueError('Unknown Pool Type {}'.format(p))
        elif config[0] == 'basic':
            _, n, in_channels, out_channels, stride = config
            blocks = nn.Sequential()
            for i in range(n):
                if i == 0:
                    blocks.add_module('block_{}'.format(i+1),
                        Block(in_channels, out_channels, stride=stride))
                else:
                    blocks.add_module('block_{}'.format(i+1),
                        Block(out_channels, out_channels))
            return blocks
        elif config[0] == 'bottle':
            _, n, in_channels, neck, out_channels, stride = config
            blocks = nn.Sequential()
            for i in range(n):
                if i == 0:
                    blocks.add_module('block_{}'.format(i+1),
                        Bottleneck(in_channels, neck, out_channels, stride=stride))
                else:
                    blocks.add_module('block_{}'.format(i+1),
                        Bottleneck(out_channels, neck, out_channels))
            return blocks
        elif config[0] == 'fc':
            _, in_channels, out_channels, bias = config
            return nn.Linear(in_channels, out_channels, bias)
        else:
            raise ValueError('Unknown Block Type {}'.format(config[0]))

def Resnet18(num_classes=None):
    return ResnetBase(res18_config, num_classes)

def Resnet34(num_classes=None):
    resnet34_conf = {
        'bottom': [
            ['conv', 3, 64, 7, 2],
            ['pool', 'max', 3, 2]
        ],
        'residual': [
            ['basic', 3, 64, 64, 1],
            ['basic', 4, 64, 128, 2],
            ['basic', 6, 128, 256, 2],
            ['basic', 3, 256, 512, 2],
        ],
        'top': [
            ['pool', 'adptavg', 1],
            ['fc', 2048, 1000, True]
        ]
    }
    return ResnetBase(resnet34_conf, num_classes)

def Resnet50(num_classes=None):
    return ResnetBase(res50_config, num_classes)

def Resnet101(num_classes=None):
    resnet101_conf = {
        'bottom': [
            ['conv', 3, 64, 7, 2],
            ['pool', 'max', 3, 2]
        ],
        'residual': [
            ['bottle', 3, 64, 64, 256, 1],
            ['bottle', 4, 256, 128, 512, 2],
            ['bottle', 23, 512, 256, 1024, 2],
            ['bottle', 3, 1024, 512, 2048, 2]
        ],
        'top': [
            ['pool', 'adptavg', 1],
            ['fc', 2048, 1000, True]
        ]
    }
    return ResnetBase(resnet101_conf, num_classes)

def Resnet152(num_classes=None):
    resnet152_conf = {
        'bottom': [
            ['conv', 3, 64, 7, 2],
            ['pool', 'max', 3, 2]
        ],
        'residual': [
            ['bottle', 3, 64, 64, 256, 1],
            ['bottle', 8, 256, 128, 512, 2],
            ['bottle', 36, 512, 256, 1024, 2],
            ['bottle', 3, 1024, 512, 2048, 2]
        ],
        'top': [
            ['pool', 'adptavg', 1],
            ['fc', 2048, 1000, True]
        ]
    }
    return ResnetBase(resnet152_conf, num_classes)

if __name__ == "__main__":
    from torchstat import stat
    model = Resnet50()
    print(model)
    # print(stat(model, (3, 512, 512)))
    x = torch.randn(16, 3, 224, 224)
    out = model(x)
    print(out.shape)
