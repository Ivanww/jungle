import torch
import torch.nn as nn

# This is a wrapper of torch hub models
# all models will load pretrained weights

class TorchHubModel(nn.Module):
    def __init__(self, url, name, bottom=None, top=None):
        super(TorchHubModel, self).__init__()

        self.backbone = torch.hub.load(url, name)

        if bottom is not None:
            raise NotImplementedError('Not Implemented Bottom Building Method for URL {}'.format(url))

        if top is not None:
            if url == 'facebookresearch/WSL-Images':
                # hint: (2048, n_classes) if resnext101_32_8d_wsl
                self.backbone.fc = self.__build_top('fc', top)
            else:
                raise NotImplementedError('Not Implemented Top Building Method for URL {}'.format(url))
    
    def forward(self, x):
        return self.backbone(x)

    def __build_bottom(self, bottom):
        in_channels, out_channels, kernel, stride, padding = bottom
        layer = nn.Conv2d(in_channels, out_channels, kernel_size=kernel,
            stride=stride, padding=padding, bias=False)
        nn.init.kaiming_normal_(layer.weight, mode='fan_out')
        return layer

    def __build_top(self, top_type, top):
        if top_type == 'conv':
            in_channels, out_channels, kernel, stride, padding = top
            layer = nn.Conv2d(in_channels, out_channels, kernel_size=kernel,
                stride=stride, padding=padding, bias=True)
            nn.init.kaiming_normal_(layer.weight, mode='fan_out')
        elif top_type == 'fc':
            in_channels, out_channels = top[:2]
            layer = nn.Linear(in_channels, out_channels, bias=True)
            nn.init.normal_(layer.weight, 0, 0.01)
        else:
            raise ValueError('Unknown Top Type {}'.format(top[0]))

        return layer
