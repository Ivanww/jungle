import torch.nn as nn
import torchvision

class TorchVisionModel(nn.Module):
    BACKBONE = {
        'resnet18': torchvision.models.resnet18,
        'resnet34': torchvision.models.resnet34,
        'resnet50': torchvision.models.resnet50,
        'resnet101': torchvision.models.resnet101,
        'resnet152': torchvision.models.resnet152,
        'densenet121': torchvision.models.densenet121,
        'vgg19': torchvision.models.vgg19_bn,
        'vgg16': torchvision.models.vgg16_bn,
        'mobilenet': torchvision.models.mobilenet_v2,
        'resnext50_32x4d': torchvision.models.resnext50_32x4d,
        'resnext101_32x8d': torchvision.models.resnext101_32x8d,
        'wide_resnet50_2': torchvision.models.wide_resnet50_2,
        'wide_resnet101_2': torchvision.models.wide_resnet101_2
    }
    BOTTOM = {
        'resnet_family': [3, 64, 7, 2, 3],
        'densenet121': [3, 64, 7, 2, 3],
        'vgg19': [3, 64, 3, 1, 1],
        'vgg16': [3, 64, 3, 1, 1],
        'mobilenet': [3, 32, 3, 1, 1]
    }
    TOP = {
        'resnet18': [512, 1000],
        'resnet34': [512, 1000],
        'resnet50': [2048, 1000],
        'resnet101': [2048, 1000],
        'densenet121': [1024, 1000],
        'vgg19': [4096, 1000],
        'vgg16': [4096, 1000],
        'mobilenet': [1280, 1000]
    }
    def __init__(self, backbone, pretrain=True, bottom=None, top=None):
        super(TorchVisionModel, self).__init__()
        self.backbone = TorchVisionModel.BACKBONE[backbone](pretrain)

        if bottom is not None:
            # config bottom
            if backbone.startswith('resnet') or backbone.startswith('resnext') or backbone.startswith('wide_resnet'):
                self.backbone.conv1 = bottom if callable(bottom) else self.__build_bottom(bottom)
            elif backbone.startswith('densenet'):
                self.backbone.features.conv0 = bottom if callable(bottom) else self.__build_bottom(bottom)
            elif backbone.startswith('vgg'):
                self.backbone.features[0] = bottom if callable(bottom) else self.__build_bottom(bottom)
            elif backbone.startswith('mobilenet'):
                self.features[0][0] = bottom if callable(bottom) else self.__build_bottom(bottom)
            else:
                pass

        if top is not None:
            if backbone.startswith('resnet') or backbone.startswith('resnext') or backbone.startswith('wide_resnet'):
                self.backbone.fc = top if callable(top) else self.__build_top('fc', top)
            elif backbone.startswith('densenet'):
                self.backbone.classifier = top if callable(top) else self.__build_top('fc', top)
            elif backbone.startswith('vgg'):
                self.backbone.classifier[6] = top if callable(top) else self.__build_top('fc', top)
            elif backbone.startswith('mobilenet'):
                self.backbone.classifier[1] = top if callable(top) else self.__build_top('fc', top)
            else:
                pass

    def __build_bottom(self, bottom):
        in_channels, out_channels, kernel, stride, padding = bottom
        layer = nn.Conv2d(in_channels, out_channels, kernel_size=kernel,
            stride=stride, padding=padding, bias=False)
        nn.init.kaiming_normal_(layer.weight, mode='fan_out')
        return layer

    def __build_top(self, top_type, top):
        if top_type == 'conv':
            in_channels, out_channels, kernel, stride, padding = top
            layer = nn.Conv2d(in_channels, out_channels, kernel_size=kernel,
                stride=stride, padding=padding, bias=True)
            nn.init.kaiming_normal_(layer.weight, mode='fan_out')
        elif top_type == 'fc':
            in_channels, out_channels = top[:2]
            layer = nn.Linear(in_channels, out_channels, bias=True)
            nn.init.normal_(layer.weight, 0, 0.01)
        else:
            raise ValueError('Unknown Top Type {}'.format(top[0]))

        return layer

    def forward(self, x):
        return self.backbone(x)
