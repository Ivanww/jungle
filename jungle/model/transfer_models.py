import torch.nn as nn

# These is for tranfer learning. All models will load pretrained weights
# https://github.com/lukemelas/EfficientNet-PyTorch
from efficientnet_pytorch import EfficientNet
# https://github.com/Cadene/pretrained-models.pytorch
import pretrainedmodels

class TransferModels(nn.Module):
    BACKBONE = {
        'efficient_net': EfficientNet.from_pretrained,
        'se_resnet50': pretrainedmodels.__dict__['se_resnet50'],
        'se_resnet101': pretrainedmodels.__dict__['se_resnet101'],
        'se_resnet152': pretrainedmodels.__dict__['se_resnet152'],
        'se_resnext50_32x4d': pretrainedmodels.__dict__['se_resnext50_32x4d'],
        'se_resnext101_32x4d': pretrainedmodels.__dict__['se_resnext101_32x4d']
    }
    def __init__(self, backbone, top=None, bottom=None, **kwargs):
        super(TransferModels, self).__init__()

        # **kwargs hints
        # efficient_net: model_name: 'efficientnet-b0', num_classes: 1000
        # se_*: model_name: 'se_*', num_classes: 1000, pretrained: 'imagenet'
        self.backbone = TransferModels.BACKBONE[backbone](**kwargs)

        if bottom is not None:
            if backbone.startswith('efficient_net'):
                if callable(bottom):
                    self.backbone._conv_stem = bottom
                else:
                    self.backbone._conv_stem = self.__build_bottom(bottom)
            elif backbone.startswith('se'):
                if callable(bottom):
                    self.backbone.layer0.conv1 = bottom
                else:
                    self.backbone.layer0.conv1 = self.__build_bottom(bottom)
            else:
                raise NotImplementedError('Changing Bottom of {} Not Implemented!'.format(backbone))

        if top is not None:
            if backbone.startswith('efficient_net'):
                if callable(top):
                    self.backbone._fc = top
                elif isinstance(top[0], list):
                    fc_layers = [self.__build_top('fc', top[0])]
                    for i in range(1, len(top)):
                        fc_layers.append(nn.LeakyReLU(0.01))
                    self.backbone.last_linear = nn.Sequential(*fc_layers)
                else:
                    # Hint ['fc', 1280, 1000]
                    self.backbone._fc = self.__build_top('fc', top)
            elif backbone.startswith('se'):
                self.backbone.avg_pool = nn.AdaptiveAvgPool2d((1, 1))
                if callable(top):
                    self.backbone.last_linear = top
                elif isinstance(top[0], list):
                    # batch layers here?
                    fc_layers = [self.__build_top('fc', top[0])]
                    for i in range(1, len(top)):
                        fc_layers.append(nn.LeakyReLU(0.01))
                        fc_layers.append(self.__build_top('fc', top[i]))
                    self.backbone.last_linear = nn.Sequential(*fc_layers)
                else:
                    self.backbone.last_linear = self.__build_top('fc', top)
            else:
                raise NotImplementedError('Changing Top of {} Not Implemented!'.format(backbone))

    def __build_bottom(self, bottom):
        in_channels, out_channels, kernel, stride, padding = bottom
        layer = nn.Conv2d(in_channels, out_channels, kernel_size=kernel,
            stride=stride, padding=padding, bias=False)
        nn.init.kaiming_normal_(layer.weight, mode='fan_out')
        return layer

    def __build_top(self, top_type, top):
        if top_type == 'conv':
            in_channels, out_channels, kernel, stride, padding = top
            layer = nn.Conv2d(in_channels, out_channels, kernel_size=kernel,
                stride=stride, padding=padding, bias=True)
            nn.init.kaiming_normal_(layer.weight, mode='fan_out')
        elif top_type == 'fc':
            in_channels, out_channels = top[:2]
            layer = nn.Linear(in_channels, out_channels, bias=True)
            nn.init.normal_(layer.weight, 0, 0.01)
        else:
            raise ValueError('Unknown Top Type {}'.format(top[0]))

        return layer

    def forward(self, x):
        return self.backbone(x)
