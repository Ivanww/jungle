import torch
import torch.nn as nn
from collections import OrderedDict
from jungle import engine
from jungle import losses
from jungle import model
from jungle import callback
from jungle import metric

eng = engine.TrainingEngine()

WORKSPACE=''
eng.set_workspace('')

# load dataset here
# train, val = build_dataset()
# eng.set_dataset([train, val])

default_backbone_config = {
    'backbone_name': 'resnet50',
    'pretraned': True
}

default_rpn_config = {
    # output channel of backbone
    # 'in_channels': inference in_channels during runtime
    # number of anchor per location (for one level)
    'num_anchors': 3,
    # parameters for filtering proposals
    'pre_nms_top_n': 2000,
    'post_nms_top_n': 2000,
    'pre_nms_top_n_test': 1000,
    'post_nms_top_n_test': 1000,
    'min_box_size': 1, # min length of proposal
    'nms_thres': 0.7 # threshold for proposals
}

default_head_config = {
    # output channel of backbone
    # 'in_channels': inference in_channels during runtime
    # names of feature maps of different level
    # see default backbone output
    'featmap_names': ['0', '1', '2', '3'],
    'num_rep': 1024, # number of hidden neurons
    # 91 is for pretrained weights
    'num_class': 91, # should be true number of classes + 1 (BG)
}

faster_rcnn_kwargs = {
  'backbone': None, # should be a nn.Module
  'rpn': None,
  'head': None,
  'backbone_cfg': default_backbone_config,
  'rpn_cfg': default_rpn_config,
  'head_cfg': default_head_config,
  'cotrain': True
}

PRETRAIN_WEIGHT = '/home/ivan/.cache/torch/checkpoints/fasterrcnn_resnet50_fpn_coco-258fb6c6.pth'
model = model.FasterRCNN(**faster_rcnn_kwargs)
ckpt = torch.load(PRETRAIN_WEIGHT)
model.load_state_dict(ckpt)

num_rep = 1024
num_class = 2
model.roi_heads.box_predictor = nn.ModuleDict(OrderedDict([
            ('cls_score', nn.Linear(num_rep, num_class)),
            ('bbox_pred', nn.Linear(num_rep, num_class*4))
        ]))

model = model.cuda()
eng.set_model(model)

optim_config = {
    'lr': 5e-3,
    'momentum': 0.9,
    'weight_decay': 5e-5
}
optim = torch.optim.SGD(params=model.parameters(), **optim_config)
# add lr scheduler
# sch_lr = torch.optim.lr_scheduler.CosineAnnealingLR(optim, T_max=10)

eng.set_optimizer(optim)
# eng.set_lr_scheduler(sch_lr)

default_rpn_loss_config = {
  # FG and BG thresholds
  'conf_lb': 0.3,
  'conf_ub': 0.7,
  # control the batch
  'num_prop': 256,
  'pos_ratio': 0.5
}
default_rcnn_loss_config = {
    # FG and BG thresholds
    'conf_lb': 0.5,
    'conf_ub': 0.5,
    # control the batch
    'num_prop': 512, # default 512
    'pos_ratio': 0.25,
    'coord_weights': [10., 10., 5., 5.]
}
default_rcnn_cotrain_loss_config = {
    'rpn_loss_cfg': default_rpn_loss_config,
    'head_loss_cfg': default_rcnn_loss_config
}
loss_fn = losses.FasterRCNNCotrainLoss(**default_rcnn_cotrain_loss_config)
eng.set_loss(loss_fn)

default_image_wrapper_config = {
    # for pack
    'min_size': 800,
    'max_size': 1333,
    # for unpack
    'conf_thres': 0.4, # prob threshold
    'nms_thres': 0.5, # nms threshold to filter overlapped boxes
    'coord_weights': [10., 10., 5., 5.],
    'num_class': 1
}
eng.add_callback(callback.CBImageWrapper(**default_image_wrapper_config), 5)

default_rcnn_anchor_handler_config = {
    # defines the cells of output feature map
    'strides': (4, 8, 16, 32, 64),
    # scales the unit anchors, get base anchor
    'anchor_scales': (32, 64, 128, 256, 512),
    # define the unit anchors
    'aspect_ratios': (0.5, 1., 2.)
}
default_anchor_handler_config = {
    't_anchor': 'rcnn',
    'config': default_rcnn_anchor_handler_config
}
eng.add_callback(callback.CBAnchorHandler(**default_anchor_handler_config), 5)
default_map_metric_config = {
    'num_classes': 2,
    'iou_thres': 0.75,
    'predict_bg': True,
    'as_criterion': False,
}
eng.add_callback(callback.CBLossMeter(batch=100, as_criterion=False), 5)

# import other meters here
default_rcnn_metric_config = {
    'metric_meters': [metric.AveragePrecisionMeter(**default_map_metric_config),
        # other meters
        ],
    'save_hist': True
}
eng.add_callback(callback.CBMetricManager(**default_rcnn_metric_config), 6)

eng.add_callback(callback.CBStateDictWritter(milestones=[],
    min_best=False), priority=7)

# other configs
eng.update_memo({
    'use_cuda': True
})

eng.initialize()
eng.run(10)
